// package com.cloud.bandott.domain.Order;
//
// import com.cloud.bandott.Core.Member.AuthCenterRepository;
// import com.cloud.bandott.Core.Member.MemberCenter;
// import com.cloud.bandott.Core.Member.MemberCenterRepository;
// import com.cloud.bandott.Core.OrderMenu.OrderCenterService;
// import com.cloud.bandott.Core.Store.Menu.StoreMenu;
// import com.cloud.bandott.Core.Store.Menu.StoreMenuRepository;
// import com.cloud.bandott.Core.Store.StoreInfoRepository;
// import org.junit.jupiter.api.*;
// import org.junit.jupiter.params.ParameterizedTest;
// import org.junit.jupiter.params.provider.ValueSource;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.data.domain.Sort;
// import org.springframework.http.HttpHeaders;
// import org.springframework.http.MediaType;
// import org.springframework.security.test.context.support.WithMockUser;
// import org.springframework.test.annotation.Rollback;
// import org.springframework.test.web.servlet.MockMvc;
// import org.springframework.test.web.servlet.MvcResult;
// import org.springframework.transaction.annotation.Transactional;
//
// import java.util.ArrayList;
// import java.util.List;
// import java.util.Random;
// import java.util.Set;
//
// import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
// import static org.junit.jupiter.api.Assertions.assertEquals;
// import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
// @SpringBootTest
// @AutoConfigureMockMvc
// @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
// @WithMockUser
// class OrderCenterFlowTest {
//     private static final Random random = new Random();
//     private static String UserId;
//     private static Long storeId;
//     private static HttpHeaders headers;
//     private static Long orderId = 0L;
//     private final MockMvc mockMvc;
//     private final MemberCenterRepository memberCenterRepository;
//     private final AuthCenterRepository authCenterRepository;
//     private final StoreInfoRepository storeInfoRepository;
//     private final StoreMenuRepository storeMenuRepository;
//     private final OrderCenterService orderCenterService;
//     private final String LocalDateTimePattern = "yyyy-MM-dd HH:mm:ss";
//     private final String LocalTimePattern = "HH:mm:ss";
//
//     @Autowired
//     OrderCenterFlowTest(MockMvc mockMvc,
//                         MemberCenterRepository memberCenterRepository,
//                         AuthCenterRepository authCenterRepository,
//                         StoreInfoRepository storeInfoRepository, StoreMenuRepository storeMenuRepository, OrderCenterService orderCenterService) {
//         this.mockMvc = mockMvc;
//         this.memberCenterRepository = memberCenterRepository;
//         this.authCenterRepository = authCenterRepository;
//         this.storeInfoRepository = storeInfoRepository;
//         this.storeMenuRepository = storeMenuRepository;
//         this.orderCenterService = orderCenterService;
//     }
//
//     @BeforeEach
//     void beforeAll() {
//
//     }
//
//     @Test
//     @Order(-1)
//     @DisplayName("(ﾟ∀ﾟ) - 初始化必要資訊")
//     void initData() throws Exception {
//         // 測試之前必須先有成員與店家
//         // 店家內容必須包含菜單
//         // 取得一筆memberId;
//         MemberCenter memberCenter = memberCenterRepository.findAll().stream().findAny().orElseThrow(() -> new Exception("初始化失敗：找不到人"));
//         UserId = memberCenter.getId().toString();
//         String userAccount = memberCenter.getEmail();
//         String userPassword = memberCenter.getPassword();
//         // 取得一筆token;
//         mockMvc.perform(post("/api/login")
//                 .with(csrf())
//                 .contentType(MediaType.MULTIPART_FORM_DATA)
//                 .param("email", userAccount)
//                 .param("password", userPassword)
//         ).andExpect(status().is2xxSuccessful());
//         String userToken = authCenterRepository.findFirstByMemberCenter_IdAndUsingTokenTrueOrderByIdDesc(Long.valueOf(UserId))
//                 .orElseThrow(() -> new Exception("初始化失敗：找不到令牌"))
//                 .getToken();
//         // 取得一筆storeId;
//         storeId = storeInfoRepository.findAll().stream().findAny().orElseThrow(() -> new Exception("初始化失敗：找不到店家")).getId();
//
//         int menuCount = 0//storeMenuRepository.findByStoreInfo_Id(storeId, Sort.by(Sort.Direction.ASC, "sort")).size();
//         if (menuCount == 0) {
//             throw new Exception("找不到店家菜單");
//         }
//         headers = new HttpHeaders();
//         headers.add("UserId", UserId);
//         headers.add("UserToken", userToken);
//         // base info
//         System.out.println("UserToken = " + userToken);
//         System.out.println("UserId = " + UserId);
//         System.out.println("UserAccount = " + userAccount);
//         System.out.println("UserPassword = " + userPassword);
//         System.out.println("StoreId = " + storeId);
//         System.out.println("menuCount = " + menuCount);
//     }
//
//     @Test
//     @Order(1)
//     @DisplayName("(ﾟ∀ﾟ) - 開桌(新增訂單)")
//     void addAnOrder() throws Exception {
//         //
//         // String title = "商業班";
//         // LocalDateTime endRequestAt = LocalDateTime.parse(
//         //         LocalDate.now() + " " + LocalTime.of(9, 0, 0).format(DateTimeFormatter.ofPattern(LocalTimePattern))
//         //         , DateTimeFormatter.ofPattern(LocalDateTimePattern));
//         // String orderStatus = "Setting";
//         // try {
//         //     mockMvc.perform(post("/api/orders")
//         //             .with(csrf())
//         //             .headers(headers)
//         //             .contentType(MediaType.MULTIPART_FORM_DATA)
//         //             .param("title", title)
//         //             .param("EndRequestAt", endRequestAt.toString())
//         //             .param("storeInfoId", storeId.toString())
//         //             .param("orderStatus", orderStatus)
//         //
//         //     ).andExpect(status().is2xxSuccessful());
//         // } catch (Exception e) {
//         //     throw new RuntimeException(e);
//         // }
//         // OrderListOut orderListOut = orderCenterService.getMyPublishOrders(Long.valueOf(UserId), 0).stream().findFirst().orElseThrow(() -> new Exception("找不到訂單"));
//         // System.out.println("id = " + orderListOut.getId());
//         // assertEquals(title, orderListOut.getTitle());
//         // assertThat(endRequestAt).isEqualTo(orderListOut.getEndRequestAt());
//         // assertEquals(orderStatus, orderListOut.getOrderStatusEnum().name());
//         // orderId = orderListOut.getId();
//     }
//
//     @Test
//     @Order(2)
//     @DisplayName("(ﾟ∀ﾟ) - 開桌(調整訂單)")
//     void editTheOrder() throws Exception {
//         // String orderStatus = "PublicOrder";
//         // LocalDateTime endRequestAt = LocalDateTime.parse(
//         //         LocalDate.now() + " " + LocalTime.of(22, 0, 0).format(DateTimeFormatter.ofPattern(LocalTimePattern))
//         //         , DateTimeFormatter.ofPattern(LocalDateTimePattern));
//         // LocalDateTime takeMealAt = LocalDateTime.parse(
//         //         LocalDate.now() + " " + LocalTime.of(23, 0, 0).format(DateTimeFormatter.ofPattern(LocalTimePattern))
//         //         , DateTimeFormatter.ofPattern(LocalDateTimePattern));
//         // try {
//         //     mockMvc.perform(patch("/api/orders/" + orderId)
//         //             .with(csrf())
//         //             .headers(headers)
//         //             .contentType(MediaType.MULTIPART_FORM_DATA)
//         //             .param("takeMealAt", takeMealAt.toString())
//         //             .param("EndRequestAt", endRequestAt.toString())
//         //             .param("orderStatus", orderStatus)
//         //     ).andExpect(status().is2xxSuccessful());
//         // } catch (Exception e) {
//         //     throw new RuntimeException(e);
//         // }
//         // OrderListOut orderListOut = orderCenterService.getMyPublishOrders(Long.valueOf(UserId), 0).stream().findFirst().orElseThrow(() -> new Exception("找不到訂單"));
//         // assertThat(endRequestAt).isEqualTo(orderListOut.getEndRequestAt());
//         // assertEquals(orderStatus, orderListOut.getOrderStatusEnum().name());
//     }
//
//     // @Test
//     @DisplayName("(ﾟ∀ﾟ) - 開桌(訂餐)")
//     @ParameterizedTest(name = "人數測試，人數：{0}")
//     @ValueSource(ints = {5, 10})
//     void addOrderDetailTest(int orderJoiner) {
//         List<HttpHeaders> userHeaders = new ArrayList<>();
//         // memberCenterRepository.findByDeletedFalseAndEnabledTrue(Pageable.ofSize(orderJoiner)).forEach(memberCenter -> {
//         //     HttpHeaders header = new HttpHeaders();
//         //     try {
//         //         mockMvc.perform(post("/api/login")
//         //                 .with(csrf())
//         //                 .contentType(MediaType.MULTIPART_FORM_DATA)
//         //                 .param("email", memberCenter.getEmail())
//         //                 .param("password", memberCenter.getPassword())
//         //         ).andExpect(status().is2xxSuccessful());
//         //
//         //         header.add("UserId", memberCenter.getId() + "");
//         //         header.add("UserToken", authCenterRepository.findFirstByMemberCenter_IdAndUsingTokenTrueOrderByIdDesc(memberCenter.getId())
//         //                 .orElseThrow(() -> new Exception("初始化失敗：找不到令牌"))
//         //                 .getToken());
//         //     } catch (Exception e) {
//         //         throw new RuntimeException(e);
//         //     }
//         //     userHeaders.add(header);
//         // });
//         userHeaders.forEach(header -> System.out.println("header = " + header));
//
//         // if num is 0 should be to deny.
//         Set<StoreMenu> storeMenus = 0//storeMenuRepository.findByStoreInfo_Id(storeId, Sort.by(Sort.Direction.ASC, "sort"));
//         userHeaders.forEach(userHeader -> {
//             try {
//                 MvcResult mvcResult = mockMvc.perform(post("/api/order/" + orderId + "/menu/" + storeMenus.get(random.nextInt(0, storeMenus.size())).getId())
//                         .with(csrf())
//                         .headers(userHeader)
//                         .contentType(MediaType.MULTIPART_FORM_DATA)
//                         .param("dishCount", String.valueOf(random.nextInt(1, 3)))
//                 ).andExpect(status().is2xxSuccessful()).andReturn();
//                 System.out.println(mvcResult.getResponse().getContentAsString());
//             } catch (Exception e) {
//                 throw new RuntimeException(e);
//             }
//         });
//     }
//
//     @DisplayName("(ﾟ∀ﾟ) - 開桌(一般使用者修改訂餐)")
//     @Test
//     @Transactional(readOnly = true)
//
//     @Rollback(false)
//     void editOrderDetailByUserTest() throws Exception {
//         // HttpHeaders header = new HttpHeaders();
//         // orderId = orderId.equals(0L) ? 803L : orderId;
//         // OrderCenter orderCenter = orderCenterService.getOrderCenter(orderId);
//         // OrderDetail orderDetail = orderCenter.getOrderDetail().stream().findFirst().orElseThrow(() -> new UserInterfaceMessageException("", "找不到訂單明細"));
//         // header.add("UserId", orderDetail.getMemberCenter().getId().toString());
//         // header.add("UserToken", authCenterRepository.findFirstByMemberCenter_IdAndUsingTokenTrueOrderByIdDesc(orderDetail.getMemberCenter().getId())
//         //         .orElseThrow(() -> new Exception("初始化失敗：找不到令牌"))
//         //         .getToken());
//         // mockMvc.perform(patch("/api/order/" + orderId + "/list/" + orderDetail.getId())
//         //         .with(csrf())
//         //         .headers(header)
//         //         .contentType(MediaType.MULTIPART_FORM_DATA)
//         //         .param("dishCount", String.valueOf(random.nextInt(4, 6)))
//         // ).andExpect(status().is4xxClientError());
//        // System.out.println("mvcResult :" + mvcResult.ge);
//     }
//
//     @DisplayName("(ﾟ∀ﾟ) - 開桌(主辦人修改訂餐內容)")
//     void editOrderDetailByOrderMasterTest() {
//
//     }
//
// }
