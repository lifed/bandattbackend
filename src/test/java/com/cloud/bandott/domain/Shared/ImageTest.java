package com.cloud.bandott.domain.Shared;

import com.cloud.bandott.Core.Member.Images.MemberImagesRepository;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.google.common.io.Files;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.Random;

@SpringBootTest
public class ImageTest {
    private static Random random = new Random();
    @Autowired
    private ImageService imageService;
    @Autowired
    private MemberImagesRepository memberImagesRepository;
    @Autowired
    private ResourceLoader resourceLoader;

    @Test
    public void addImageTest() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png");
        MockMultipartFile mFile = new MockMultipartFile("userPicture", "Emoji-Psck (" + random.nextInt(1, 295) + ").png", MediaType.IMAGE_PNG_VALUE, Files.toByteArray(resource.getFile()));
    }

}
