package com.cloud.bandott.domain.Store;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.Store.Category.StoreCategoryMapper;
import com.cloud.bandott.Core.Store.Category.StoreCategoryRepository;
import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategoryRepo;
import com.cloud.bandott.Core.Store.Menu.StoreMenuService;
import com.cloud.bandott.Core.Store.StoreInfo;
import com.cloud.bandott.Core.Store.StoreInfoRepository;
import com.cloud.bandott.Core.Store.StoreInfoService;
import com.cloud.bandott.Presentation.Component.Store.StoreImagesController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Random;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class StoreInfoTest {
    private static Random random = new Random();
    private MockMvc mockMvc;
    private StoreInfoService storeInfoService;
    private StoreMenuService storeMenuService;
    private StoreCategoryRepository storeCategoryRepository;
    private StoreInfoRepository storeInfoRepository;
    private ResourceLoader resourceLoader;
    private StoreCategoryMapper storeSharedMapper;
    private ObjectMapper objectMapper;
    private StoreMenuCategoryRepo storeMenuCategoryRepo;

    @Autowired
    public StoreInfoTest(MockMvc mockMvc, StoreInfoService storeInfoService, StoreMenuService storeMenuService, StoreCategoryRepository storeCategoryRepository, StoreInfoRepository storeInfoRepository, ResourceLoader resourceLoader, StoreCategoryMapper storeSharedMapper, StoreMenuCategoryRepo storeMenuCategoryRepo) {
        this.mockMvc = mockMvc;
        this.storeInfoService = storeInfoService;
        this.storeMenuService = storeMenuService;
        this.storeCategoryRepository = storeCategoryRepository;
        this.storeInfoRepository = storeInfoRepository;
        this.resourceLoader = resourceLoader;
        this.storeSharedMapper = storeSharedMapper;
        this.storeMenuCategoryRepo = storeMenuCategoryRepo;
    }

    @BeforeEach
    void BeforeEachInit() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void Stage1_CreateStoreCategory() throws Exception {
        System.out.println("Stage1...Start");
        String[] storeCategory = {"中式美食", "韓式美食", "日式美食", "美式美食", "越式美食", "印式美食", "其他美食"};
        String[] ChineseLayer = {"壹", "貳", "參", "肆", "伍", "陸", "柒", "捌", "玖"};
        int LayerDeep;
        for (String sc : storeCategory) {
            mockMvc.perform(post("/api/store/category")
                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    .param("name", sc)
            );
            long storeCategoryId = storeCategoryRepository.findCategoryByModuleIdAndParentIdAndName(0, 0, sc).get().getId();
            for (int j = 0; j <= random.nextInt(1, 10); j++) {
                mockMvc.perform(post("/api/store/category")
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .param("storeCategoryId", storeCategoryId + "")
                        .param("name", "第二層類別" + random.nextInt(201, 400))
                );
            }
        }
        System.out.println("Stage1...End");
    }

    @Test
    void Stage2_CreateStores() throws Exception {
        System.out.println("Stage2...Start");
        long[] storeCategoriesId = storeCategoryRepository.findByParentIdNullOrderByCategorySortAsc().stream().mapToLong(BaseEntity::getId).toArray();
        String[] storeNames = {"龍德", "八方雲集-台中中工三店", "小老闆泰式料理店", "茶之軒飯舖", "亞洲雞腿王", "麥味登-台中工業店", "麥當勞-台中中港六餐廳", "PokéPoké 波奇波奇 中港店", "陳師傅牛肉麵大王", "御品樓北方麵食館", "精食巧海苔飯捲-中工店"};
        String[] storeDescriptions = {"日式便當", "煎餃鍋貼", "泰式料理", "便當、麵食", "便當", "早午餐", "速食", "生菜沙拉", "牛肉麵", "麵食", "飯卷"};
        for (int i = 0; i < storeNames.length; i++) {
            String picMain = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
            String picBanner = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
            MockMultipartFile mainImageFile = new MockMultipartFile("mainImage", resourceLoader.getResource(picMain).getFilename(), "text/plain", Files.toByteArray(resourceLoader.getResource(picMain).getFile()));
            MockMultipartFile bannerImageFile = new MockMultipartFile("bannerImage", resourceLoader.getResource(picBanner).getFilename(), "text/plain", Files.toByteArray(resourceLoader.getResource(picBanner).getFile()));
            mockMvc.perform(multipart("/api/store")
                    .file(mainImageFile)
                    .file(bannerImageFile)
                    .param("name", storeNames[i] + random.nextInt(1, 295))
                    .param("description", storeDescriptions[i])
                    .param("storeStatesEnum", "Hidden")
                    .param("storeCategoryId", storeCategoriesId[random.nextInt(0, storeCategoriesId.length - 1)] + "")
            );
        }
        System.out.println("Stage2...End");
    }

    @Test
    void Stage3_CreateStoreMenuCategory() throws Exception {
        System.out.println("Stage3...Start");
        List<StoreInfo> all = storeInfoRepository.findAll();
        Assert.notEmpty(all, "請確認是否有店家資訊");
        long storeId = all.get(0).getId();
        String[] StoreMenuCategory = {"飯類", "麵類", "湯類", "飲品"};
        for (String sc : StoreMenuCategory) {
            mockMvc.perform(post("/api/store/" + storeId + "/menucategory")
                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    .param("name", sc)
            );
        }

        // int[] modifyCategoryIds = {131, 134, 137, 140};
        // String[] StoreMenuCategoryModify = {"米飯類", "麵食類", "熱湯類", "暢飲品"};
        // for (int i = 0; i < modifyCategoryIds.length; i++) {
        //     mockMvc.perform(patch("/api/store/" + storeId + "/menucategory/" + modifyCategoryIds[i])
        //             .contentType(MediaType.MULTIPART_FORM_DATA)
        //             .param("name", StoreMenuCategoryModify[i])
        //     );
        // }
        //
        // int[] deleteCategoryids = {147, 150, 153, 156};
        // for (int id : deleteCategoryids) {
        //     mockMvc.perform(
        //             delete("/api/store/" + storeId + "/menucategory/" + id)
        //     );
        // }

        System.out.println("Stage3...End");
    }

    @Test
    void Stage4_CreateStoreMenu() {
        System.out.println("Stage4...Start");
        List<StoreInfo> all = storeInfoRepository.findAll();
        Assert.notEmpty(all, "請確認是否有店家資訊");
        Long storeId = all.get(0).getId();
        Long[] menuCategories = storeMenuCategoryRepo.findCategoriesByModuleIdAndParentIdIsNull(storeId).stream().map(BaseEntity::getId).toList().toArray(new Long[0]);
//        storeMenuCategoryRepo.findCategoriesByModuleIdAndParentIdIsNull(storeId).stream().map(BaseEntity::getId).toList().toArray(new Long[0]);
        Assert.notEmpty(menuCategories, "請確認是否有菜單類別");
        String[] rices = {"滷肉飯", "控肉販", "雞肉飯"};
        int[] ricesprice = {30, 45, 50};
        String[] noodles = {"陽春麵", "湯麵", "牛肉麵"};
        int[] noodlesprice = {50, 30, 90};
        String[] soups = {"貢丸湯", "豆腐湯", "四神湯"};
        int[] soupsprice = {20, 20, 30};
        String[] drinks = {"可樂", "紅茶", "綠茶"};
        int[] drinksprice = {35, 20, 20};
        int i = 0;
        addMenus(storeId, menuCategories[i++], rices, ricesprice);
        addMenus(storeId, menuCategories[i++], noodles, noodlesprice);
        addMenus(storeId, menuCategories[i++], soups, soupsprice);
        addMenus(storeId, menuCategories[i], drinks, drinksprice);

        // int[] deleteMenus = {160, 166, 172};
        // IntStream.range(0, deleteMenus.length).forEach(i -> {
        //     try {
        //         mockMvc.perform(delete("/api/store/" + storeId + "/menu/" + deleteMenus[i]));
        //     } catch (Exception e) {
        //         throw new RuntimeException(e);
        //     }
        // });
        System.out.println("Stage4...End");
    }

//    @Test
//    void modifyStores() throws Exception {
//        String storePic = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
//        MockMultipartFile menuDishPicFile = new MockMultipartFile("imageFile", resourceLoader.getResource(menuDishPic).getFilename(), "text/plain", Files.toByteArray(resourceLoader.getResource(menuDishPic).getFile()));
//        int[] storeCategoryid = {1,4,12,17,23,28,34};
//
//        String picMain = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
//        String picBanner = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
//        MockMultipartFile mainImageFile = new MockMultipartFile("mainImage", resourceLoader.getResource(picMain).getFilename(), "text/plain", Files.toByteArray(resourceLoader.getResource(picMain).getFile()));
//        MockMultipartFile bannerImageFile = new MockMultipartFile("bannerImage", resourceLoader.getResource(picBanner).getFilename(), "text/plain", Files.toByteArray(resourceLoader.getResource(picBanner).getFile()));
//        String[] storename = {"龍德", "八方雲集-台中中工三店", "小老闆泰式料理店","茶之軒飯舖","亞洲雞腿王","麥味登-台中工業店","麥當勞-台中中港六餐廳","PokéPoké 波奇波奇 中港店", "陳師傅牛肉麵大王", "御品樓北方麵食館", "精食巧海苔飯捲-中工店"};
//        String[] storedescription = {"日式便當", "煎餃鍋貼", "泰式料理", "便當、麵食","便當","早午餐","速食","生菜沙拉","牛肉麵","麵食","飯卷"};
//        for (int i = 0; i < storename.length; i++) {
//            mockMvc.perform(multipart("/api/store")
//                    .file(mainImageFile)
//                    .file(bannerImageFile)
//                    .param("name", storename[i])
//                    .param("description", storedescription[i])
//                    .param("storeStatesEnum", "Hidden")
//                    .param("storeCategoryId", storeCategoryid[2] + "")
//            );
//        }
//        System.out.println("Stage2...End");
//    }

    void addMenus(Long storeId, Long menuCategory, String[] names, int[] prices) {
        for (int i = 0; i < names.length; i++) {
            String menuDishPic = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
            try {
                MockMultipartFile menuDishPicFile = new MockMultipartFile("imageFile", resourceLoader.getResource(menuDishPic).getFilename(), "text/plain", Files.toByteArray(resourceLoader.getResource(menuDishPic).getFile()));
                mockMvc.perform(multipart("/api/store/" + storeId + "/menu")
                        .file(menuDishPicFile)
                        .param("name", names[i])
                        .param("description", "")
                        .param("price", prices[i] + "")
                        .param("storeMenuCategoryId", menuCategory + "")
                );
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    @Test
    void queryTest() throws Exception {
        // StoreCategory storeCategorytest = storeCategoryRepository.getReferenceById(1L);
        // System.out.println(storeCategorytest.getModuleCategories());
        // // 所有類別
        // List<StoreCategory> storeCategories = storeCategoryRepository.findRootCategoriesByModuleId(0);
        // List<StoreCategoryResponse> storeCategoryResponses = new ArrayList<>();
        // for (StoreCategory storeCategory : storeCategories) {
        //     storeCategoryResponses.add(storeSharedMapper.storeCategoryToStoreCategoryResponse(storeCategory));
        // }
        // try {
        //     System.out.println(objectMapper.writeValueAsString(storeCategoryResponses));
        // } catch (JsonProcessingException e) {
        //     throw new RuntimeException(e);
        // }
        // mockMvc.perform(get("/api/storeCategories"));
        // // 類別下的商店
        // // 商店詳細列表

    }

    @Test
    public void addPicMain() throws Exception {
        String imageResourcePath = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
        MockMultipartFile multipartFile = new MockMultipartFile("imageFile",
                resourceLoader.getResource(imageResourcePath).getFilename(),
                "text/plain",
                Files.toByteArray(resourceLoader.getResource(imageResourcePath).getFile()));
        Long storeId = 58L;
        mockMvc.perform(multipart("/api/store/p/file/" + storeId
                        + "/" + StoreImagesController.ImageType.main.name())
                        .file(multipartFile)
                ).andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void addPicMenu() throws Exception {
        String imageResourcePath = "classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png";
        MockMultipartFile multipartFile = new MockMultipartFile("imageFile",
                resourceLoader.getResource(imageResourcePath).getFilename(),
                "text/plain",
                Files.toByteArray(resourceLoader.getResource(imageResourcePath).getFile()));
        Long storeId = 58L;
        Long menuId = 102L;
        mockMvc.perform(multipart("/api/store/p/file/" + storeId
                        + "/" + StoreImagesController.ImageType.menu.name() + "/" + menuId)
                        .file(multipartFile)
                ).andExpect(status().isCreated())
                .andReturn();
    }

}

