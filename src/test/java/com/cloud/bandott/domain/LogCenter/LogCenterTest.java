package com.cloud.bandott.domain.LogCenter;

import com.cloud.bandott.Core.SharedKernel.LogCenter.LogCenter;
import com.cloud.bandott.Core.SharedKernel.LogCenter.LogCenterRepo;
import com.cloud.bandott.Core.SharedKernel.LogCenter.LogCenterService;
import com.cloud.bandott.Core.SharedKernel.LogCenter.ModuleAction;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@SpringBootTest
public class LogCenterTest {
    LogCenterRepo logCenterRepo;
    LogCenterService logCenterService;

    @Autowired
    public LogCenterTest(LogCenterRepo logCenterRepo, LogCenterService logCenterService) {
        this.logCenterRepo = logCenterRepo;
        this.logCenterService = logCenterService;
    }

    @Test
    void insertLogCenter() {
        for (int i = 0; i < 10; i++) {
            LogCenter logCenter = new LogCenter();
            logCenter.setModuleName(this.getClass().getSimpleName());
            logCenter.setUserId(1L);
            logCenter.setActionReason("InTestUpdateData" + i);
            logCenter.setModuleAction(ModuleAction.SQLUpdate.value());
            logCenterRepo.save(logCenter);
        }
        // Assert.assertEquals(, logCenter.getId());
    }

    @Test
    void getLogAsyncTest() throws ExecutionException, InterruptedException {
        Instant stime, etime;
        stime = Instant.now();
        List<CompletableFuture> completableFutures = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            completableFutures.add(logCenterService.findUserByModuleActionAsync(1L, ModuleAction.SQLUpdate));
        }
        CompletableFuture allOf = CompletableFuture.allOf(completableFutures.toArray(new CompletableFuture[completableFutures.size()]));
        allOf.join();

        etime = Instant.now();
        System.out.println("Async開始時間: " + stime + ", 結束時間: " + etime + ", 相差時間: " + Duration.between(stime, etime));
    }


    @Test
    void getLogTest() {
        Instant stime, etime;
        stime = Instant.now();
        for (int i = 0; i < 10; i++) {
            logCenterService.findUserByModuleAction(1L, ModuleAction.SQLUpdate);
        }
        Optional<LogCenter> logCenter = logCenterService.findUserByModuleAction(1L, ModuleAction.SQLUpdate);
        if (logCenter.isPresent()) {
            System.out.println(logCenter.get().toString());
        } else {
            System.out.println("is empty");
        }
        etime = Instant.now();
        System.out.println("Normal開始時間: " + stime + ", 結束時間: " + etime + ", 相差時間: " + Duration.between(stime, etime));
    }

    @Test
    void getLogTest2() {
        Instant stime, etime;
        stime = Instant.now();
        for (long i = 0; i < 10; i++) {
            logCenterRepo.findFirstByIdOrderByIdDesc(90000L);
        }
        etime = Instant.now();
        System.out.println("Normal開始時間: " + stime + ", 結束時間: " + etime + ", 相差時間: " + Duration.between(stime, etime));
    }

    @Test
    public void queryTestFromPostgresqlWithEM() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("postgresql");
        EntityManager em = factory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        for (long i = 0; i < 10; i++) {
            em.createQuery("from LogCenter lc where lc.id = :i order by lc.id desc");
        }

        tx.commit();
        em.close();
        factory.close();
    }

    @Test
    public void addLogTestFromPostgresqlWithEM() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("postgresql");
        EntityManager em = factory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(new LogCenter());
        tx.commit();
        em.close();
        factory.close();
    }
}
