package com.cloud.bandott.domain.MemberCenter;

import com.cloud.bandott.Core.Member.MemberCenterRepository;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.cloud.bandott.Utils.RandomUserInfoUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.google.common.io.Files;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MemberCenterTest {
    private static RandomUserInfoUtil randomUserInfoUtil;
    private static Faker faker;
    private static Random random;
    private final MemberCenterRepository memberCenterRepository;
    private final MemberCenterService memberCenterService;
    private final ResourceLoader resourceLoader;
    private Environment env;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Autowired
    public MemberCenterTest(MemberCenterRepository memberCenterRepository, MemberCenterService memberCenterService, ResourceLoader resourceLoader, Environment env, MockMvc mockMvc) {
        this.memberCenterRepository = memberCenterRepository;
        this.memberCenterService = memberCenterService;
        this.resourceLoader = resourceLoader;
        this.env = env;
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    public void before() {
        faker = new Faker();
        random = new Random();
        objectMapper = new ObjectMapper();
    }

    @Async
    public CompletableFuture<String> addMemberFromController() {
        String password = faker.internet().password(8, 16, true, false, true);
        MultiValueMap<String, String> formMap = new LinkedMultiValueMap<>();
        formMap.add("email", faker.internet().emailAddress());
        formMap.add("password", password);
        formMap.add("repassword", password);
        formMap.add("name", RandomUserInfoUtil.generateChineseName(random.nextInt(1)));
        formMap.add("nickname", faker.name().name().trim());

        MockMultipartFile file1;
        try {
            Resource resource = resourceLoader.getResource("classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png");
            file1 = new MockMultipartFile("userPicture", "Emoji-Psck (" + random.nextInt(1, 295) + ").png", MediaType.IMAGE_PNG_VALUE, Files.toByteArray(resource.getFile()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            mockMvc.perform(MockMvcRequestBuilders
                            .multipart("/api/signUp")
                            .file(file1)
                            .params(formMap))
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return CompletableFuture.completedFuture("新增成功");
    }

    // 10000 in async mode within 48s
    // 1000 in not async mode within 60s
    // FIXME: the process will be interrupted if the email has been used
    @Test
    public void generateMemberWithThread() {
        List<CompletableFuture<String>> futures = new ArrayList<>();
        for (int i = 0; i <= 10000; i++) {
            futures.add(addMemberFromController());
        }
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).join();
    }

}
