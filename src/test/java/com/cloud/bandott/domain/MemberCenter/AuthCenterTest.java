package com.cloud.bandott.domain.MemberCenter;

import com.cloud.bandott.Core.Member.AuthCenterService;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthCenterTest {
    private final AuthCenterService authCenterService;
    private final MemberCenterService memberCenterService;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Autowired
    public AuthCenterTest(AuthCenterService authCenterService, MemberCenterService memberCenterService, MockMvc mockMvc) {
        this.authCenterService = authCenterService;
        this.memberCenterService = memberCenterService;
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    public void before() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void loginGenerateTokenFromController() throws Exception {
        Map<String, String> formMap = new HashMap<>();
        // formMap.put("email", "kelvin.zboncak@yahoo.com");
        // formMap.put("password", "ecw2v9jl");
        formMap.put("email", "madeleine.bradtke@gmail.com");
        formMap.put("password", "58pwiupphm");
        mockMvc.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(formMap))
        ).andExpect(status().isOk());
    }

}
