package com.cloud.bandott.Controller;

import com.cloud.bandott.Utils.RandomUserInfoUtil;
import com.github.javafaker.Faker;
import com.google.common.io.Files;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MemberControllerTest {
    private MockMvc mockMvc;

    private static Faker faker;
    private static Random random;
    private final ResourceLoader resourceLoader;

    @Autowired
    public MemberControllerTest(MockMvc mockMvc, ResourceLoader resourceLoader) {
        this.mockMvc = mockMvc;
        this.resourceLoader = resourceLoader;
    }

    @BeforeEach
    public void before() {
        faker = new Faker();
        random = new Random();
//        objectMapper = new ObjectMapper();
    }

    /*
     * In controller, login(@RequestBody LoginMemberRequest loginMemberRequest)
     */
    @Test
    public void LoginWithRequestBody() throws Exception {
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("email", "domain1@ptt.cc");
        paramMap.put("password", "Aa12345678");

        HttpHeaders headers = new HttpHeaders();
        mockMvc.perform(post("/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .headers(headers)
                        .content(JSONObject.toJSONString(paramMap))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.token").exists());
    }

    /*
     * In controller, login(LoginMemberRequest loginMemberRequest)
     */
    @Test
    public void LoginWithRequestParam() throws Exception {
//        UrlEncodedFormEntity form = new UrlEncodedFormEntity(Arrays.as        // paramMap.put("email", "email");
        // paramMap.put("password", "password");List(
//                new BasicNameValuePair("email", "email"),
//                new BasicNameValuePair("password", "pwdpasswordpasswordpasswordpasswordpwdpasswordpasswordpas")
//        ), "utf-8");
//
//        mockMvc.perform(post("/login")
//                                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
//                                .content(EntityUtils.toString(form))
//                )
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.token").exists());
    }

    @Test
    public void RegisterWithRequestBody() throws Exception {
        String password = faker.internet().password(8, 16, true, false, true);
        MultiValueMap<String, String> formMap = new LinkedMultiValueMap<>();
        formMap.add("email", faker.internet().emailAddress());
        formMap.add("password", password);
        formMap.add("repassword", password);
        formMap.add("name", RandomUserInfoUtil.generateChineseName(random.nextInt(1)));
        formMap.add("nickname", faker.name().name().trim());

        MockMultipartFile file1;
        try {
            Resource resource = resourceLoader.getResource("classpath:FakerUploadPic/Emoji-Psck (" + random.nextInt(1, 295) + ").png");
            file1 = new MockMultipartFile("userPicture", "Emoji-Psck (" + random.nextInt(1, 295) + ").png", MediaType.IMAGE_PNG_VALUE, Files.toByteArray(resource.getFile()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            mockMvc.perform(MockMvcRequestBuilders
                            .multipart("/api/signUp")
                            .file(file1)
                            .params(formMap))
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
