package com.cloud.bandott.Core.Filters.ControllerAdvice;

// MediaType.APPLICATION_JSON
public enum UserInterfaceMessageEnum {
    MemberAccountAndPasswordNotMatchException("電子信箱或密碼錯誤"),
    MemberPasswordAndRepasswordNotMatchException("密碼不一致"),
    MemberAccountIsNotEnable("帳號未啟用"),
    MemberAccountHasBeenDelete("帳號已刪除"),
    MemberAccountIsExist("信箱已存在"),
    MemberAccountIsNotExist("帳號不存在"),
    StoreIsNotExist("店家不存在"),
    ;

    private String message;

    UserInterfaceMessageEnum(String message) {
        this.message = message;
    }

    public String value() {
        return message;
    }
}
