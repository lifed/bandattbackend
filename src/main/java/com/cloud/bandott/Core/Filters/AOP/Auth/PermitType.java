package com.cloud.bandott.Core.Filters.AOP.Auth;

public enum PermitType {
    /**
     * All user.
     */
    anonymous,
    /**
     * Registered User.
     */
    authentication,
    /**
     * WebAdmin.
     */
    admin,
    /**
     * Programmer.
     */
    root

}
