package com.cloud.bandott.Core.Filters.ControllerAdvice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolationException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
@Slf4j
@AllArgsConstructor
@Order(999)
public class GlobalExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> constraintViolationException(ConstraintViolationException ex) throws JsonProcessingException {
        Map<String, String> anError = new HashMap<>();

        anError.put("ExceptionName", "");
        anError.put("ExceptionMessage", ex.getMessage());
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("content-type", "application/json; charset=utf-8 ");
        ObjectMapper objectMapper = new ObjectMapper();
        return new ResponseEntity<>(
                objectMapper.writeValueAsString(anError),
                responseHeaders,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) throws JsonProcessingException {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        Map<String, String> anError = new HashMap<>();
        if (!errors.isEmpty()) {
            Map.Entry<String, String> firstEntry = errors.entrySet().iterator().next();
            anError.put("ExceptionName", firstEntry.getKey());
            anError.put("ExceptionMessage", firstEntry.getValue());
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("content-type", MediaType.APPLICATION_JSON_VALUE);
        ObjectMapper objectMapper = new ObjectMapper();
        return new ResponseEntity<>(
                objectMapper.writeValueAsString(anError),
                responseHeaders,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> globalExceptionHandler(Exception ex) {
        log.error("ex = {}", ex.toString());
        log.error("ex.getLocalizedMessage = {}", ex.getLocalizedMessage());
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("content-type", "text/html; charset=utf-8");
        return new ResponseEntity<>(
                "伺服器異常，請聯絡管理者或稍後再試。",
                responseHeaders,
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
}
