package com.cloud.bandott.Core.Filters.AOP.Auth;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.AuthCenterService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDateTime;
import java.util.Objects;

@Aspect
@Component
@AllArgsConstructor
@Slf4j
public class AuthAdvisor {
    private final AuthCenterService authCenterService;

    @Pointcut("@annotation(com.cloud.bandott.Core.Filters.AOP.Auth.VerifyPermit)")
    public void verifyPermit() {
    }

    // @Around("verifyPermit()")
    // public void around() {
    //     System.out.println("aop: around");
    // }

    @Before("verifyPermit()")
    public void before() {
        System.out.println("aop: before");
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        try {
            if (Long.parseLong(request.getHeader("UserId")) <= 0
                    || request.getHeader("UserToken") == null
                    || request.getHeader("UserToken").equals("")) {
                throw new Exception();
            }
        } catch (Exception e) {
            log.error("UserId: {}, UserToken: {}, DateTime: {}, Reason: {}",
                    request.getHeader("UserId"),
                    request.getHeader("UserToken"),
                    LocalDateTime.now(),
                    e.getMessage());
            throw new UserInterfaceMessageException("User Data error", "使用者資料錯誤");
        }
        authCenterService.verifyGoodAuth(Long.parseLong(request.getHeader("UserId")), request.getHeader("UserToken"));
    }

    // @After("verifyPermit()")
    // public void after() {
    //     System.out.println("aop: after");
    // }
    //
    // @AfterReturning("verifyPermit()")
    // public void afterReturning() {
    //     System.out.println("aop: afterReturning");
    // }
    //
    // @AfterThrowing(pointcut = "verifyPermit()", throwing = "e")
    // public void afterThrowing(Throwable e) throws Throwable {
    //     System.out.println("aop: afterThrowing");
    //     throw e;
    // }

}
