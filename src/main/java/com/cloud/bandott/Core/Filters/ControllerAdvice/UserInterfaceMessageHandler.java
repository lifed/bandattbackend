package com.cloud.bandott.Core.Filters.ControllerAdvice;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
@Order(1)
public class UserInterfaceMessageHandler {
    @ExceptionHandler(UserInterfaceMessageException.class)
    public ResponseEntity<Map<String, String>> handleUserInterfaceMessageException(UserInterfaceMessageException ex) {
        HttpStatus httpStatus = ex.getHttpStatus();
        Map<String, String> errors = new HashMap<>();
        errors.put("ExceptionName", ex.getExceptionName());
        errors.put("ExceptionMessage", ex.getExceptionMessage());
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(
                errors,
                responseHeaders,
                httpStatus
        );
    }
}
