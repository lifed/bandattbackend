package com.cloud.bandott.Core.Filters.ControllerAdvice;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@NoArgsConstructor
public class UserInterfaceMessageException extends RuntimeException {
    private String exceptionName;
    private String exceptionMessage;
    private HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

    public UserInterfaceMessageException(UserInterfaceMessageEnum userInterfaceMessageEnum, HttpStatus httpStatus) {
        this.exceptionName = userInterfaceMessageEnum.name();
        this.exceptionMessage = userInterfaceMessageEnum.value();
        this.httpStatus = httpStatus;
    }

    public UserInterfaceMessageException(UserInterfaceMessageEnum userInterfaceMessageEnum) {
        this.exceptionName = userInterfaceMessageEnum.name();
        this.exceptionMessage = userInterfaceMessageEnum.value();
    }

    public UserInterfaceMessageException(String exceptionName, String exceptionMessage) {
        this.exceptionName = exceptionName;
        this.exceptionMessage = exceptionMessage;
    }
}
