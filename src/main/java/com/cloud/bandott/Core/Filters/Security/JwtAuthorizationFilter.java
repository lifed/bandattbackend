package com.cloud.bandott.Core.Filters.Security;

import com.cloud.bandott.Core.Member.SecurityUserDetailsService;
import com.cloud.bandott.Core.SharedKernel.Security.JwtTokenBlacklist;
import com.cloud.bandott.Core.SharedKernel.Security.JwtUtil;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.regex.Pattern;

@Component
@AllArgsConstructor
@Slf4j
public class JwtAuthorizationFilter extends OncePerRequestFilter {
    private final SecurityUserDetailsService securityUserDetailsService;
    private final JwtUtil jwtUtil;
    private final JwtTokenBlacklist jwtTokenBlacklist;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        String publicURIPattern = "^/api/.*?/p/.*$";
        if (header == null || !header.startsWith("Bearer ")
                || Pattern.compile(publicURIPattern).matcher(request.getRequestURI()).find()) {
            filterChain.doFilter(request, response);
            return;
        }
        try {
            String token = header.substring(7);
            if (jwtTokenBlacklist.contains(token)) {
                throw new JwtException("token已失效");
            }
            UserDetails userDetails = securityUserDetailsService.loadUserByUsername(jwtUtil.extractUsername(token));
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            filterChain.doFilter(request, response);
        } catch (JwtException e) {
            log.error("jwt filter exception: {}", e.toString());
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
