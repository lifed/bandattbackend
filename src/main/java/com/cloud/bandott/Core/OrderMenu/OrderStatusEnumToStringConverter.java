package com.cloud.bandott.Core.OrderMenu;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class OrderStatusEnumToStringConverter implements Converter<OrderStatusEnum, String> {
    @Override
    public String convert(OrderStatusEnum orderStatusEnum) {
        return orderStatusEnum.value();
    }
}
