package com.cloud.bandott.Core.OrderMenu.Detail.Projections;

import org.springframework.beans.factory.annotation.Value;

public interface OrderDetailPickUpProjection {
    @Value("#{target.name}")
    String getName();

    @Value("#{target.price}")
    Double getPrice();

    @Value("#{target.quantity}")
    Double getQuantity();

    @Value("#{target.total}")
    Double getTotal();

    @Value("#{target.typeName}")
    String getTypeName();
}
