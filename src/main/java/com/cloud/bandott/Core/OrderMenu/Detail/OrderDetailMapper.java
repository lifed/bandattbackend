package com.cloud.bandott.Core.OrderMenu.Detail;

import com.cloud.bandott.Core.Store.Menu.StoreMenu;
import com.cloud.bandott.Presentation.Component.Order.OrderDetail.OrderDetailVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {MealStatusEnumToStringConverter.class})
public interface OrderDetailMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "dishName", source = "name")
    @Mapping(target = "dishPrice", source = "price")
    OrderDetail storeMenuToOrderDetail(StoreMenu storeMenu);

    @Mapping(target = "dishName", source = "dishName")
    @Mapping(target = "dishPrice", source = "dishPrice")
    @Mapping(target = "dishQuantity", source = "dishQuantity")
    // @Mapping(target = "mealStatus", source = "mealStatusEnum", qualifiedByName = "mealStatusEnumMapping")
    @Mapping(target = "mealStatus", source = "mealStatusEnum")
    @Mapping(target = "mealStatusReason", source = "mealStatusReason")
    @Mapping(target = "userName", source = "memberCenter.name")
    @Mapping(target = "userId", source = "memberCenter.id")
    @Mapping(target = "mealStatusEnum", source = "mealStatusEnum")
    OrderDetailVO orderDetailToOrderDetailVO(OrderDetail orderDetail);

    // @Named("mealStatusEnumMapping")
    // @ValueMappings({
    //         @ValueMapping(source = "Ordered", target = "已下單"),
    //         @ValueMapping(source = "PreOrderOverdue", target = "逾時預定"),
    //         @ValueMapping(source = "Reserved", target = "已預定"),
    //         @ValueMapping(source = "Paid", target = "已付款"),
    //         @ValueMapping(source = "SoldOut", target = "已完售"),
    //         @ValueMapping(source = "CancelledBySelf", target = "已取消"),
    //         @ValueMapping(source = "CancelledByAdmin", target = "已取消(管理員)"),
    //         @ValueMapping(source = "CancelledByOwner", target = "已取消(主辦人)"),
    // })
    // String mapMealStatusEnumToString(MealStatusEnum mealStatusEnum);
}
