package com.cloud.bandott.Core.OrderMenu.Detail;

public enum MealStatusEnum {
    @Deprecated PreOrder("已下單"),
    @Deprecated PreOrderOverdue("逾時下單"),
    @Deprecated Cancelled("已取消"),
    Ordered("已下單"), OverdueOrder("逾時下單"),
    Reserved("已預定"), Paid("已付款"), SoldOut("已完售"),
    CancelledBySelf("已取消"), CancelledByAdmin("已取消(管理員)"), CancelledByOwner("已取消(主辦人)");

    private String status;

    MealStatusEnum(String status) {
        this.status = status;
    }

    public String value() {
        return this.status;
    }
}
