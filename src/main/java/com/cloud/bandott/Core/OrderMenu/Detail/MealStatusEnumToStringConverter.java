package com.cloud.bandott.Core.OrderMenu.Detail;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MealStatusEnumToStringConverter implements Converter<MealStatusEnum, String> {
    @Override
    public String convert(MealStatusEnum mealStatusEnum) {
        return mealStatusEnum.value();
    }
}
