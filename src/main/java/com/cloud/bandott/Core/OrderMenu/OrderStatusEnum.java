package com.cloud.bandott.Core.OrderMenu;

public enum OrderStatusEnum {
    Setting("設定中(未公開)"), PublicOrder("公開"), Abandoned("棄單");

    private String value;

    OrderStatusEnum(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
