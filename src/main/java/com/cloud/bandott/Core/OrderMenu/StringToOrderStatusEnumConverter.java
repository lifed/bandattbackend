package com.cloud.bandott.Core.OrderMenu;

import org.springframework.core.convert.converter.Converter;

public class StringToOrderStatusEnumConverter implements Converter<String, OrderStatusEnum> {
    @Override
    public OrderStatusEnum convert(String source) {
        return OrderStatusEnum.valueOf(source);
    }
}
