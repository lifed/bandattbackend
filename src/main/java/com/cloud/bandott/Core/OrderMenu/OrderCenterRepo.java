package com.cloud.bandott.Core.OrderMenu;

import com.cloud.bandott.Core.Member.MemberCenter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;

@Repository
@RestResource(exported = false)
public interface OrderCenterRepo extends JpaRepository<OrderCenter, Long> {
    Page<OrderCenter> findByEndRequestAtBetweenAndOrderStatusEnumOrderByCreateAtDesc(LocalDateTime endRequestAtStart, LocalDateTime endRequestAtEnd, OrderStatusEnum orderStatusEnum, Pageable pageable);

    long deleteByIdAndMemberCenter(Long id, MemberCenter memberCenter);

    Optional<OrderCenter> findByIdAndMemberCenter_Id(Long id, Long id1);

    Page<OrderCenter> findByCreateAtAfterOrderByIdAsc(Instant createAt, Pageable pageable);

    Page<OrderCenter> findByMemberCenter_IdOrderByIdDesc(Long id, Pageable pageable);


    // @Query("""
    //         select o from OrderCenter o where o.id in
    //         (select od.orderCenter.id from OrderDetail od
    //         where od.memberCenter.id = ?1
    //         group by od.orderCenter.id)
    //         order by o.id desc""")
    @Query("""
            select distinct o from OrderCenter o 
            left join o.orderDetail od 
            where od.memberCenter.id = ?1 
            order by o.id desc""")
    Page<OrderCenter> findOrderCentersByOrderDetail(Long memberId, Pageable pageable);

}
