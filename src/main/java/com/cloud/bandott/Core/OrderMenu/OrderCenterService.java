package com.cloud.bandott.Core.OrderMenu;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.cloud.bandott.Presentation.Component.Order.In.OrderDto;
import com.cloud.bandott.Presentation.Component.Order.Out.OrderListVO;
import com.cloud.bandott.Presentation.Component.Order.Out.OrderVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class OrderCenterService {
    private final OrderCenterRepo orderCenterRepo;
    private final OrderCenterMapper orderCenterMapper;
    private final MemberCenterService memberCenterService;


    public void getOrder(OrderDto orderDto) {
        OrderCenter orderCenter = orderCenterMapper.orderDtoToOrderCenter(orderDto);
        orderCenterRepo.save(orderCenter);
    }

    public void addOrder(OrderDto orderDto) {
        OrderCenter orderCenter = orderCenterMapper.orderDtoToOrderCenter(orderDto);
        orderCenterRepo.save(orderCenter);
    }

    public void editOrder(boolean isAdmin, OrderDto orderDto) {
        OrderCenter orderCenter;
        if (isAdmin) {
            orderCenter = orderCenterRepo.findById(orderDto.getOrderId())
                    .orElseThrow(() -> new UserInterfaceMessageException("Can not find order by admin", "找不到訂單資訊"));
        } else {
            orderCenter = orderCenterRepo.findByIdAndMemberCenter_Id(orderDto.getOrderId()
                            , memberCenterService.getMemberInfoById(orderDto.getMemberCenterId()).getId())
                    .orElseThrow(() -> new UserInterfaceMessageException("Can not find order by user", "找不到訂單資訊"));
        }
        // 不能對訂單修改特定資料
        orderDto.setStoreId(null);
        orderCenterMapper.updateOrderCenterFromOrderDto(orderDto, orderCenter);
        orderCenterRepo.save(orderCenter);
    }

    @Transactional
    public void deleteOrder(boolean isAdmin, long orderId, MemberCenter memberCenter) {
        if (isAdmin) {
            orderCenterRepo.deleteById(orderId);
        } else {
            orderCenterRepo.deleteByIdAndMemberCenter(orderId, memberCenter);
        }
    }

    OrderListVO boxingOrderList(Page<OrderCenter> orderCenters, int page) {
        List<OrderVO> orderVOList = new ArrayList<>();
        for (OrderCenter orderCenter : orderCenters) {
            orderVOList.add(orderCenterMapper.orderCenterToOrderVO(orderCenter));
        }
        return OrderListVO.builder()
                .Page(page)
                .totalPages(orderCenters.getTotalPages())
                .totalElements(orderCenters.getTotalElements())
                .Size(orderCenters.getSize())
                .orderList(orderVOList)
                .build();
    }

    public OrderListVO getTodayTables(int page) {
        // System.out.println(" LocalDateTime.now().withHour(0).withMinute(0).withSecond(0) = " +  LocalDateTime.now().withHour(0).withMinute(0).withSecond(0));
        return boxingOrderList(orderCenterRepo.findByEndRequestAtBetweenAndOrderStatusEnumOrderByCreateAtDesc(
                LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0)
                , LocalDateTime.now().withHour(23).withMinute(59).withSecond(59).withNano(99)
                , OrderStatusEnum.PublicOrder
                , PageRequest.of(page - 1, 10)
        ), page);
    }

    public OrderListVO getPublishedOrders(String email, int page) {
        Page<OrderCenter> publishedOrders = orderCenterRepo.findByMemberCenter_IdOrderByIdDesc(
                memberCenterService.getMemberInfoByEmail(email).getId(),
                PageRequest.of(page - 1, 10));
        return boxingOrderList(publishedOrders, page);
    }

    public OrderListVO getJoinedOrders(String email, int page) {
        return boxingOrderList(orderCenterRepo.findOrderCentersByOrderDetail(
                memberCenterService.getMemberInfoByEmail(email).getId()
                , PageRequest.of(page - 1, 10)), page);
    }

    public OrderListVO getOrdersOut(int page) {
        return boxingOrderList(orderCenterRepo.findByCreateAtAfterOrderByIdAsc(Instant.now(), PageRequest.of(page - 1, 10)), page);
    }

    public OrderDto getOrderInfo(long orderId) {
        OrderCenter orderCenter = orderCenterRepo.findById(orderId)
                .orElseThrow(() -> new UserInterfaceMessageException("Can not find order: get", "找不到訂單資訊"));
        return orderCenterMapper.orderCenterToOrderDto(orderCenter);
    }

    public OrderCenter getOrderCenter(long orderId) {
        return orderCenterRepo.findById(orderId)
                .orElseThrow(() -> new UserInterfaceMessageException("Can not find order: get", "找不到訂單資訊"));
    }

}
