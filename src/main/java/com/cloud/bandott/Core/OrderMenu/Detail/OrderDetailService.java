package com.cloud.bandott.Core.OrderMenu.Detail;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.cloud.bandott.Core.OrderMenu.OrderCenter;
import com.cloud.bandott.Core.OrderMenu.OrderCenterService;
import com.cloud.bandott.Core.OrderMenu.OrderStatusEnum;
import com.cloud.bandott.Core.Store.Menu.StoreMenu;
import com.cloud.bandott.Core.Store.Menu.StoreMenuService;
import com.cloud.bandott.Presentation.Component.Order.In.OrderDetailDto;
import com.cloud.bandott.Presentation.Component.Order.OrderDetail.OrderDetailListVO;
import com.cloud.bandott.Presentation.Component.Order.OrderDetail.OrderDetailPickUpVO;
import com.cloud.bandott.Presentation.Component.Order.OrderDetail.OrderDetailVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OrderDetailService {
    private final OrderDetailRepo orderDetailRepo;
    private final OrderDetailMapper orderDetailMapper;
    private final StoreMenuService storeMenuService;
    private final OrderCenterService orderCenterService;
    private final MemberCenterService memberCenterService;

    @Transactional
    public void addOrderDetail(OrderDetailDto orderDetailDto) {
        /*
            檢查資料
            1. 檢查數量
            2. 檢查訂單狀態
            3. 檢查訂單時間
            4. 檢查menuId是否符合store內的訂單
         */
        List<OrderDetailDto.OrderDetailContentDto> orderContents = orderDetailDto.getItems();
        orderContents.forEach(content -> {
            if (content.getQuantity() <= 0) {
                throw new UserInterfaceMessageException("(´ﾟдﾟ`)", "購物車的商品數量應該大於1");
            }
        });
        OrderCenter orderCenter = orderCenterService.getOrderCenter(orderDetailDto.getOrderId());
        if (!OrderStatusEnum.PublicOrder.equals(orderCenter.getOrderStatusEnum())) {
            throw new UserInterfaceMessageException("((´ﾟдﾟ`))", "目前無法下單喔");
        }

        if (LocalDateTime.now().isAfter(orderCenter.getEndRequestAt().plusMinutes(10))) {
            // TODO: 移除註解
            // throw new UserInterfaceMessageException("(((ﾟдﾟ)))", "訂單已截止");
        }
        Set<StoreMenu> storeMenuSet = storeMenuService.getMenus(orderCenter.getStoreInfo().getId());
        Set<Long> idSet = storeMenuSet
                .stream()
                .map(StoreMenu::getId)
                .collect(Collectors.toSet());
        orderDetailDto.getItems().forEach(content -> {
            if (!idSet.contains(content.getId())) {
                throw new UserInterfaceMessageException("(((ﾟдﾟ)))", "店家菜單與菜單不符合");
            }
        });

        orderContents.forEach(orderDetailContent -> {
            storeMenuSet.forEach(storeMenu -> {
                if (Objects.equals(orderDetailContent.getId(), storeMenu.getId())) {
                    OrderDetail orderDetail = orderDetailMapper.storeMenuToOrderDetail(storeMenu);
                    orderDetail.setOrderCenter(orderCenter);
                    orderDetail.setStoreMenu(storeMenu);
                    orderDetail.setMemberCenter(orderDetailDto.getMemberCenter());
                    orderDetail.setDishQuantity(orderDetailContent.getQuantity());
                    orderDetail.setMealStatusEnum(MealStatusEnum.Ordered);
                    if (LocalDateTime.now().isBefore(orderCenter.getEndRequestAt().plusMinutes(10))
                            && LocalDateTime.now().isAfter(orderCenter.getEndRequestAt())) {
                        orderDetail.setMealStatusEnum(MealStatusEnum.PreOrderOverdue);
                    }
                    orderDetailRepo.save(orderDetail);
                }
            });
        });
    }

    public List<OrderDetailPickUpVO> getOrderDetailPickup(Long orderId) {
        return orderDetailRepo.findOrderDetailPickup(orderId)
                .stream()
                .map(projection -> new OrderDetailPickUpVO(projection))
                .collect(Collectors.toList());

    }

    public OrderDetailListVO getOrderPublished(Long orderId) {
        return collectOrderDetailListVO(orderDetailRepo.findByOrderCenter_IdOrderByCreateAtAsc(orderId));
    }

    public OrderDetailListVO getOrderJoined(Long orderId, Long memberId) {
        return collectOrderDetailListVO(orderDetailRepo.findByMemberCenter_IdAndOrderCenter_IdOrderByCreateAtAsc(memberId, orderId));
    }

    public OrderDetailListVO collectOrderDetailListVO(List<OrderDetail> orderDetails) {
        List<OrderDetailVO> orderDetailVOS = orderDetails.stream()
                .map(orderDetail -> orderDetailMapper.orderDetailToOrderDetailVO(orderDetail))
                .collect(Collectors.toList());
        return OrderDetailListVO.builder().detailVOList(orderDetailVOS).build();
    }

    public Optional<OrderDetail> getOrderDetailByMemberId(Long orderId, Long memberId) {
        return orderDetailRepo.findFirstByOrderCenter_IdAndMemberCenter_IdOrderByIdAsc(orderId, memberId);
    }


    @Transactional
    public void deleteOrderDetail(Long orderId, Long orderDetailId, MemberCenter memberCenter, boolean isAdmin) {
        OrderDetail orderDetail = validateOrder(orderId, orderDetailId);
        boolean isOrderOwner = Objects.equals(orderDetail.getOrderCenter().getMemberCenter().getId(), memberCenter.getId());
        // TODO: 已預定的狀況下預定的使用者不能刪除
        if (Objects.equals(orderDetail.getMemberCenter().getId(), memberCenter.getId())) {
            orderDetail.setMealStatusEnum(MealStatusEnum.CancelledBySelf);
            orderDetailRepo.save(orderDetail);
            // Warning: entity使用SQLDelete機制，若刪除參數修改則entity要連動修改
            // orderDetailRepo.delete(orderDetail);
        } else {
            if (isOrderOwner) {
                orderDetail.setMealStatusEnum(MealStatusEnum.CancelledByOwner);
                orderDetailRepo.save(orderDetail);
            } else if (isAdmin) {
                orderDetail.setMealStatusEnum(MealStatusEnum.CancelledByAdmin);
                orderDetailRepo.save(orderDetail);
            } else {
                throw new UserInterfaceMessageException("Not permission", "權限不足");
            }
        }
    }

    public void modifyDetail(Long orderId, Long orderDetailId, Integer dishQuantity, MealStatusEnum mealStatusEnum, MemberCenter memberCenter, boolean isAdmin) {
        OrderDetail orderDetail = validateOrder(orderId, orderDetailId);
        boolean isOrderOwner = Objects.equals(orderDetail.getOrderCenter().getMemberCenter().getId(), memberCenter.getId());
        if (MealStatusEnum.Reserved == mealStatusEnum
                || MealStatusEnum.Paid == mealStatusEnum) {
            orderDetail.setDishQuantity(dishQuantity);
        }
        if (isOrderOwner && (MealStatusEnum.Reserved == mealStatusEnum
                || MealStatusEnum.SoldOut == mealStatusEnum
                || MealStatusEnum.Paid == mealStatusEnum
                || MealStatusEnum.CancelledByOwner == mealStatusEnum
                || MealStatusEnum.CancelledBySelf == mealStatusEnum)) {
            orderDetail.setMealStatusEnum(mealStatusEnum);
        }
        if (!(MealStatusEnum.CancelledBySelf == orderDetail.getMealStatusEnum()
                || MealStatusEnum.CancelledByOwner == orderDetail.getMealStatusEnum()
                || MealStatusEnum.Cancelled == orderDetail.getMealStatusEnum()
                || MealStatusEnum.CancelledByAdmin == orderDetail.getMealStatusEnum())) {
            throw new UserInterfaceMessageException("Can't modify data: status locked", "目前訂單狀態：" + orderDetail.getMealStatusEnum().value() + "，所以無法修改");
        }

        if (Objects.equals(orderDetail.getMemberCenter().getId(), memberCenter.getId())
                || isOrderOwner || isAdmin) {
            orderDetailRepo.save(orderDetail);
        } else {
            throw new UserInterfaceMessageException("Not permission", "權限不足");
        }
    }

    public void modifyDetailMealStatusByUser(Long orderId, Long UserId, MemberCenter memberCenter, boolean isAdmin) {
        OrderCenter orderCenter = orderCenterService.getOrderCenter(orderId);
        boolean isOrderOwner = Objects.equals(orderCenter.getMemberCenter().getId(), memberCenter.getId());
        MemberCenter memberInfoByUserId = memberCenterService.getMemberInfoById(UserId);
        if (isOrderOwner || isAdmin) {
            orderDetailRepo.updateAllDetailMealStatusByUser(orderCenter, memberInfoByUserId);
        } else {
            throw new UserInterfaceMessageException("Not permission", "權限不足");
        }
    }

    public void modifyDetailMealStatus(Long orderId, MemberCenter memberCenter, boolean isAdmin) {
        OrderCenter orderCenter = orderCenterService.getOrderCenter(orderId);
        boolean isOrderOwner = Objects.equals(orderCenter.getMemberCenter().getId(), memberCenter.getId());
        if (isOrderOwner || isAdmin) {
            orderDetailRepo.updateAllDetailMealStatusToReserved(orderCenter);
        } else {
            throw new UserInterfaceMessageException("Not permission", "權限不足");
        }
    }


    private OrderDetail validateOrder(Long orderId, Long orderDetailId) {
        OrderDetail orderDetail = orderDetailRepo.findById(orderDetailId)
                .orElseThrow(() -> new UserInterfaceMessageException("Not Found", "找不到該訂單明細"));
        if (!Objects.equals(orderDetail.getOrderCenter().getId(), orderId)) {
            throw new UserInterfaceMessageException("Not match", "訂單與明細不符合");
        }
        if (LocalDateTime.now().isAfter(orderDetail.getOrderCenter().getEndRequestAt())) {
            throw new UserInterfaceMessageException("(((ﾟдﾟ)))", "訂單已截止");
        }
        return orderDetail;
    }
}
