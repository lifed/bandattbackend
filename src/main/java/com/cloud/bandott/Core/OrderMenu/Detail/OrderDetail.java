package com.cloud.bandott.Core.OrderMenu.Detail;

import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.OrderMenu.OrderCenter;
import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.SharedKernel.DefaultEntityListener;
import com.cloud.bandott.Core.Store.Menu.StoreMenu;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;

import java.util.Objects;

@Entity
@Table(name = "order_detail", indexes = {
        @Index(name = "idx_order_detail_meal_status", columnList = "meal_status")
        , @Index(name = "idx_order_detail_order_member_id", columnList = "member_id")
        , @Index(name = "idx_order_detail_order_order_center_id", columnList = "order_center_id")
})
@SQLDelete(sql = "UPDATE order_detail SET meal_status = 'Cancelled' WHERE id=?")
@DynamicUpdate
@EntityListeners(DefaultEntityListener.class)
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "訂單細項")
public class OrderDetail extends BaseEntity {
    @Schema(name = "菜餚名稱", required = true)
    @Column(name = "dish_name")
    @NotNull
    private String dishName;

    @Schema(name = "菜餚金額", required = true)
    @Column(name = "dish_money")
    @NotNull
    private Long dishPrice;

    @Schema(name = "菜餚數量", required = true)
    @Column(name = "dish_quantity")
    @NotNull
    private Integer dishQuantity;

    @Schema(name = "餐點狀態", required = true)
    @Column(name = "meal_status")
    @Enumerated(EnumType.STRING)
    @NotNull
    private MealStatusEnum mealStatusEnum;

    @Schema(name = "餐點狀態變更理由")
    @Column(name = "meal_status_reason")
    private String mealStatusReason;

    @Schema(name = "跟單人員Id", required = true)
    @JoinColumn(name = "member_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private MemberCenter memberCenter;

    @Schema(name = "訂單中心Id", required = true)
    @JoinColumn(name = "order_center_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private OrderCenter orderCenter;

    @Schema(name = "菜餚Id")
    @JoinColumn(name = "menu_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private StoreMenu storeMenu;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OrderDetail that = (OrderDetail) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
