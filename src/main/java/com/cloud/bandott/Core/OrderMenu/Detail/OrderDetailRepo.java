package com.cloud.bandott.Core.OrderMenu.Detail;

import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.OrderMenu.Detail.Projections.OrderDetailPickUpProjection;
import com.cloud.bandott.Core.OrderMenu.OrderCenter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@RestResource(exported = false)
public interface OrderDetailRepo extends JpaRepository<OrderDetail, Long> {
    Optional<OrderDetail> findFirstByOrderCenter_IdAndMemberCenter_IdOrderByIdAsc(Long id, Long id1);

    @Transactional
    @Modifying
    @Query("""
            update OrderDetail o 
            set o.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.Reserved
            where (o.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.PreOrder 
            or o.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.Ordered 
            or o.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.OverdueOrder) 
            and o.orderCenter = ?1 
            """)
    int updateAllDetailMealStatusToReserved(OrderCenter orderCenter);

    @Transactional
    @Modifying
    @Query("""
            update OrderDetail o 
            set o.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.Paid
            where o.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.Reserved
            and o.orderCenter = ?1
            and o.memberCenter = ?2
             """)
    int updateAllDetailMealStatusByUser(OrderCenter orderCenter, MemberCenter memberCenter);

    List<OrderDetail> findByOrderCenter_IdOrderByCreateAtAsc(Long id);

    List<OrderDetail> findByMemberCenter_IdAndOrderCenter_IdOrderByCreateAtAsc(Long memberId, Long orderId);

    @Query("""
            select sm.name as name, 
            od.dishPrice as price, 
            sum(od.dishQuantity) as quantity, 
            (sum(od.dishQuantity) * od.dishPrice) as total,
            smc.name as typeName,
            smc.categorySort
            from OrderDetail od
            left join od.storeMenu sm
            left join sm.storeMenuCategory smc
            where (od.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.Paid
                or od.mealStatusEnum = com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum.Reserved)
            and od.orderCenter.id = :orderId
            group by sm.name, od.dishPrice, od.orderCenter.id, smc.name, smc.categorySort
            order by smc.categorySort
            """)
    List<OrderDetailPickUpProjection> findOrderDetailPickup(Long orderId);
}
