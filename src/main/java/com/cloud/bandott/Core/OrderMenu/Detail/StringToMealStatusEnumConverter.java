package com.cloud.bandott.Core.OrderMenu.Detail;

import org.springframework.core.convert.converter.Converter;

public class StringToMealStatusEnumConverter implements Converter<String, MealStatusEnum> {
    @Override
    public MealStatusEnum convert(String source) {
        return MealStatusEnum.valueOf(source);
    }
}
