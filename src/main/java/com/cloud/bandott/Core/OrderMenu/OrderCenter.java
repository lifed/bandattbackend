package com.cloud.bandott.Core.OrderMenu;

import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.OrderMenu.Detail.OrderDetail;
import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.SharedKernel.DefaultEntityListener;
import com.cloud.bandott.Core.Store.StoreInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "order_center", indexes = {
        @Index(name = "idx_order_center_order_status", columnList = "order_status")
        , @Index(name = "idx_order_center_end_request_at", columnList = "end_request_at")
        , @Index(name = "idx_order_center_member_id", columnList = "member_id")
        , @Index(name = "idx_order_center_store_id", columnList = "store_id")
})
@SQLDelete(sql = "UPDATE order_center SET order_status = 'Abandoned' WHERE id=?")
@DynamicUpdate
@EntityListeners(DefaultEntityListener.class)
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "訂單中心")
public class OrderCenter extends BaseEntity {
    @Schema(name = "訂單標題")
    @Column(name = "title")
    @NotNull
    private String title;

    @Schema(name = "訂單截止時間")
    @Column(name = "end_request_at")
    @NotNull
    private LocalDateTime endRequestAt;

    @Schema(name = "預計取餐時間")
    @Column(name = "take_meal_at")
    private LocalDateTime takeMealAt;

    @Schema(name = "訂單狀態")
    @Column(name = "order_status")
    @Enumerated(EnumType.STRING)
    @NotNull
    private OrderStatusEnum orderStatusEnum;

    @Schema(name = "棄單理由")
    @Column(name = "abandon_reason")
    private String abandonReason;

    @Schema(name = "開桌人員Id")
    @JoinColumn(name = "member_id")
    @ManyToOne
    @NotNull
    private MemberCenter memberCenter;

    @ManyToOne
    @JoinColumn(name = "store_id")
    @Schema(name = "店家Id")
    @NotNull
    private StoreInfo storeInfo;

    @Schema(name = "訂單詳情")
    @OneToMany(mappedBy = "orderCenter")
    @ToString.Exclude
    private List<OrderDetail> orderDetail;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OrderCenter that = (OrderCenter) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
