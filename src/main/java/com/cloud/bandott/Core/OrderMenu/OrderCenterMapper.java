package com.cloud.bandott.Core.OrderMenu;

import com.cloud.bandott.Presentation.Component.Order.In.OrderDto;
import com.cloud.bandott.Presentation.Component.Order.Out.OrderVO;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {OrderStatusEnumToStringConverter.class})
public interface OrderCenterMapper {
    @Mapping(target = "storeName", source = "storeInfo.name")
    @Mapping(target = "storeId", source = "storeInfo.id")
    @Mapping(target = "organizerName", source = "memberCenter.name")
    @Mapping(target = "organizerNickname", source = "memberCenter.nickname")
    @Mapping(target = "orderStatus", source = "orderStatusEnum")
    OrderVO orderCenterToOrderVO(OrderCenter orderCenter);

    @Mapping(source = "memberCenterId", target = "memberCenter.id")
    @Mapping(source = "storeId", target = "storeInfo.id")
    @Mapping(source = "orderId", target = "id")
    @Mapping(source = "orderStatus", target = "orderStatusEnum")
    OrderCenter orderDtoToOrderCenter(OrderDto orderDto);

    @InheritInverseConfiguration(name = "orderDtoToOrderCenter")
    OrderDto orderCenterToOrderDto(OrderCenter orderCenter);

    @InheritConfiguration(name = "orderDtoToOrderCenter")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    OrderCenter updateOrderCenterFromOrderDto(OrderDto orderDto, @MappingTarget OrderCenter orderCenter);


    @AfterMapping
    default void linkOrderDetail(@MappingTarget OrderCenter orderCenter) {
        if (orderCenter.getOrderDetail() != null) {
            orderCenter.getOrderDetail().forEach(orderDetail -> orderDetail.setOrderCenter(orderCenter));
        }
    }
}
