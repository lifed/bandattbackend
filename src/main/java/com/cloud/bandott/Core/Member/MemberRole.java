package com.cloud.bandott.Core.Member;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;

import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "member_role")
@DynamicUpdate
@SuperBuilder
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Schema(title = "會員角色權限")
public class MemberRole extends BaseEntity {
    @Column(name = "name")
    private String name;
    @ManyToMany(mappedBy = "memberRoles")
    @ToString.Exclude
    private List<MemberCenter> memberCenters;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MemberRole that = (MemberRole) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
