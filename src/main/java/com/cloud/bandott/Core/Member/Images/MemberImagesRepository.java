package com.cloud.bandott.Core.Member.Images;

import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageCenterRepo;

public interface MemberImagesRepository extends ImageCenterRepo<MemberImages> {


}
