package com.cloud.bandott.Core.Member;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageEnum;
import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.google.common.hash.Hashing;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthCenterService {
    private final AuthCenterRepository authCenterRepository;

    @Transactional
    public AuthCenter generateToken(MemberCenter memberCenter) {
        if (memberCenter == null) {
            throw new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberAccountIsNotExist);
        }
        deactivateToken(memberCenter);
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        AuthCenter authCenter = AuthCenter.builder()
                .memberCenter(memberCenter)
                .usingToken(true)
                .fromIp(request.getRemoteAddr())
                .token(Hashing.sha256().hashString("" + Instant.now() + memberCenter.getId() + memberCenter.hashCode(), StandardCharsets.UTF_8).toString())
                .build();
        authCenterRepository.save(authCenter);
        return authCenter;
    }

    public void deactivateToken(MemberCenter memberCenter) {
        authCenterRepository.updateUsingTokenByMemberCenterAndUsingTokenTrue(memberCenter);
    }

    public void verifyGoodAuth(long memberId, String userToken) {
        Optional<AuthCenter> authCenterOptional = authCenterRepository.findFirstByMemberCenter_IdAndUsingTokenTrueOrderByIdDesc(memberId);
        // 資安考量: 不能回應清楚資訊給用戶端
        if (authCenterOptional.isEmpty()) {
            throw new UserInterfaceMessageException("User Data error", "驗證失敗");
        }
        if (!authCenterOptional.get().getToken().equals(userToken)) {
            throw new UserInterfaceMessageException("User Data error", "驗證失敗");
        }
    }

}
