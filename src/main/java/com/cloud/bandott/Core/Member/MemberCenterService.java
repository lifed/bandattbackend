package com.cloud.bandott.Core.Member;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageEnum;
import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.Images.MemberImages;
import com.cloud.bandott.Core.Member.Images.MemberImagesRepository;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.cloud.bandott.Core.SharedKernel.MailServer.MailService;
import com.cloud.bandott.Core.SharedKernel.Security.JwtUtil;
import com.cloud.bandott.Core.SharedKernel.StoragePath.Paths.MemberPath;
import com.cloud.bandott.Presentation.Component.Member.MemberInfoDto;
import com.cloud.bandott.Presentation.Component.Member.Response.LoginMemberResponse;
import com.google.common.hash.Hashing;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@AllArgsConstructor
public class MemberCenterService {
    private final MemberCenterRepository memberCenterRepository;
    private final AuthCenterService authCenterService;
    private final ImageService imageService;
    private final MemberImagesRepository memberImagesRepository;
    private final PasswordEncoder passwordEncoder;
    private final MemberRoleRepository memberRoleRepository;
    private final MemberCenterMapper memberCenterMapper;
    private final MailService mailService;
    private final JwtUtil jwtUtil;

    /**
     * 1. 驗證資料是否符合邏輯
     * 2. 驗證密碼是否一致
     * 3. 檢查是否有存在的email
     * 4. 新增人員
     */
    @Async
    public CompletableFuture<Void> register(MemberInfoDto memberInfoDto) {
        if (!memberInfoDto.getPassword().equals(memberInfoDto.getRepassword())) {
            return CompletableFuture.failedFuture(new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberPasswordAndRepasswordNotMatchException));
        }
        if (memberCenterRepository.findFirstByEmailAllIgnoreCaseOrderByEmailAsc(memberInfoDto.getEmail()).isPresent()) {
            return CompletableFuture.failedFuture(new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberAccountIsExist));
        }
        List<MemberRole> memberRoles = new ArrayList<>();
        memberRoles.add(memberRoleRepository.findByName(EnumRole.FRONTEND_USER.name()));
        MemberCenter memberCenter = MemberCenter.builder()
                .deleted(false)
                .enabled(true)
                .name(memberInfoDto.getName())
                .nickname(memberInfoDto.getNickname())
                .password(passwordEncoder.encode(memberInfoDto.getPassword()))
                .email(memberInfoDto.getEmail())
                .memberRoles(memberRoles)
                .build();
        memberCenterRepository.save(memberCenter);
        MultipartFile uploadImg = memberInfoDto.getUploadImg();
        if (uploadImg != null && !uploadImg.isEmpty()) {
            imageService.addImage(memberCenter.getId(),
                    new MemberPath(memberCenter.getId()).getRootPath(),
                    uploadImg,
                    memberImagesRepository,
                    MemberImages.class);
        }
        return CompletableFuture.completedFuture(null);
    }

    public LoginMemberResponse loginWithPrincipal(String email, String urlPath) {
        return memberCenterRepository.findFirstByEmail(email)
                .map(memberCenter -> LoginMemberResponse.builder()
                        .Id(memberCenter.getId())
                        .Name(memberCenter.getNickname())
                        .Token(authCenterService.generateToken(memberCenter).getToken())
                        .PicURL(imageService.getPicUrl(memberCenter.getId(), urlPath + "/api/member/p/file/", memberImagesRepository))
                        .Auth(hasBackendAuth(memberCenter))
                        .build())
                .orElse(LoginMemberResponse.builder()
                        .Id(null)
                        .Name(null)
                        .Token(null)
                        .PicURL(null)
                        .build());
    }


    public MemberCenter getMemberInfoByEmail(String email) {
        return memberCenterRepository.findFirstByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("找不到使用者"));
    }

    public MemberCenter getMemberInfoById(Long memberId) {
        return memberCenterRepository.findFirstByIdOrderByIdAsc(memberId)
                .orElseThrow(() -> new UsernameNotFoundException("找不到使用者"));
    }

    public void updateMemberInfo(MemberInfoDto memberInfoDto) {
        MemberCenter memberCenter = memberCenterRepository.findFirstByEmail(memberInfoDto.getEmail())
                .orElseThrow(() -> new UsernameNotFoundException("找不到使用者"));
        if (memberInfoDto.getPassword() != null && memberInfoDto.getPassword().trim().equals("")) {
            memberInfoDto.setPassword(null);
        }
        memberCenterMapper.updateMemberInfoDtoFromToMemberCenter(memberInfoDto, memberCenter);
        if (memberInfoDto.getPassword() != null) {
            memberCenter.setPassword(passwordEncoder.encode(memberInfoDto.getPassword()));
        }
        memberCenterRepository.save(memberCenter);
        MultipartFile uploadImg = memberInfoDto.getUploadImg();
        if (uploadImg != null && !uploadImg.isEmpty()) {
            imageService.addImage(memberCenter.getId(),
                    new MemberPath(memberCenter.getId()).getRootPath(),
                    uploadImg,
                    memberImagesRepository,
                    MemberImages.class);
        }
    }

    public void setEmailCode(String email, String localUrlPath) {
        MemberCenter memberCenter = memberCenterRepository.findFirstByEmail(email)
                .orElseThrow(() -> new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberAccountIsNotExist));
        String changePasswordCode = Hashing.sha512()
                .hashString(email + LocalDateTime.now(), StandardCharsets.UTF_8)
                .toString();
        mailService.sendMessage(email, "重新設定密碼", localUrlPath + "?hashcode=" + changePasswordCode);
        memberCenter.setChangePassword(changePasswordCode);
        memberCenterRepository.save(memberCenter);
    }

    public void resetPassword(String hashCode, String newPassword) {
        MemberCenter memberCenter = memberCenterRepository.findFirstByChangePasswordOrderByIdAsc(hashCode)
                .orElseThrow(() -> new UsernameNotFoundException("找不到使用者"));
        memberCenter.setPassword(passwordEncoder.encode(newPassword));
        memberCenter.setChangePassword(null);
        memberCenterRepository.save(memberCenter);
    }

    public LoginMemberResponse jwtSignIn(String email, String password, String urlPath) {
        MemberCenter memberCenter = memberCenterRepository.findFirstByEmail(email)
                .orElseThrow(() -> new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberAccountAndPasswordNotMatchException));
        if (!passwordEncoder.matches(password, memberCenter.getPassword())) {
            throw new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberAccountAndPasswordNotMatchException);
        }
        return LoginMemberResponse.builder()
                .Id(memberCenter.getId())
                .Name(memberCenter.getNickname())
                .Token(jwtUtil.generateToken(new User(email, password, AuthorityUtils.createAuthorityList(memberCenter.getRoles()))))
                .PicURL(imageService.getPicUrl(memberCenter.getId(), urlPath + "/api/member/p/file/", memberImagesRepository))
                .Auth(hasBackendAuth(memberCenter))
                .build();
    }

    public String hasBackendAuth(MemberCenter memberCenter) {
        List<String> roles = Arrays.asList("BACKEND_USER", "WEB_ADMIN", "ROOT");
        boolean hasAuth = memberCenter.getMemberRoles()
                .stream()
                .map(MemberRole::getName)
                .anyMatch(roles::contains);
        return hasAuth ? "backend" : "";
    }
}
