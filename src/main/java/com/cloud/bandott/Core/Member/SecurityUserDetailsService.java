package com.cloud.bandott.Core.Member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class SecurityUserDetailsService implements UserDetailsService {
    @Autowired
    private MemberCenterRepository memberCenterRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        // INFO: 若使用UserInterfaceException會無法觸發ControllerAdvice
        MemberCenter memberCenter = memberCenterRepository.findFirstByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("找不到使用者: " + username));
        return new User(memberCenter.getEmail(), memberCenter.getPassword(), AuthorityUtils.createAuthorityList(memberCenter.getRoles()));
    }
}
