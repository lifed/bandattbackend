package com.cloud.bandott.Core.Member;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.SharedKernel.DefaultEntityListener;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.*;

import java.util.Objects;

@Table(name = "auth_center", indexes = {
        @Index(name = "idx_auth_using_token", columnList = "using_token")
        , @Index(name = "idx_auth_member_id", columnList = "member_id")
})
@Entity
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@SQLDelete(sql = "UPDATE auth_center SET using_token = false WHERE id=?")
@EntityListeners(DefaultEntityListener.class)
@FilterDef(name = "usingTokenFilter", parameters = @ParamDef(name = "isUsing_token", type = Boolean.class))
@Filter(name = "usingTokenFilter", condition = "using_token = :isUsing_token")
@DynamicUpdate
@Schema(title = "驗證中心")
public class AuthCenter extends BaseEntity {
    @Schema(name = "該token是否在使用中")
    @Column(
            name = "using_token",
            nullable = false
    )
    @NotNull
    private boolean usingToken;

    @Schema(name = "Token")
    @Column(
            name = "token",
            nullable = false,
            length = 300
    )
    @NotNull
    private String token;

    @Schema(name = "請求認證來源IP")
    @Column(
            name = "from_ip",
            nullable = false,
            // IPv6 address Max string Length 45
            // example: ABCD:ABCD:ABCD:ABCD:ABCD:ABCD:192.168.100.101
            length = 45
    )
    @NotNull
    private String fromIp;

    @Schema(name = "使用者Id")
    @ManyToOne
    @JoinColumn(name = "member_id")
    private MemberCenter memberCenter;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AuthCenter that = (AuthCenter) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
