package com.cloud.bandott.Core.Member.Images;

import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageCenter;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DiscriminatorOptions;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "會員圖片庫")
@DiscriminatorOptions(force = true)
public class MemberImages extends ImageCenter {

    public MemberImages(MultipartFile mFile) {
        super(mFile);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MemberImages that = (MemberImages) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
