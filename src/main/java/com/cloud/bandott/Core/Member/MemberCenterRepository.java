package com.cloud.bandott.Core.Member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MemberCenterRepository extends JpaRepository<MemberCenter, Long> {
    Optional<MemberCenter> findFirstByIdOrderByIdAsc(Long id);

    Optional<MemberCenter> findFirstByEmailAllIgnoreCaseOrderByEmailAsc(String email);

    Optional<MemberCenter> findFirstByChangePasswordOrderByIdAsc(String changePassword);

    Optional<MemberCenter> findFirstByEmail(String email);
}
