package com.cloud.bandott.Core.Member;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.SharedKernel.DefaultEntityListener;
import com.cloud.bandott.Presentation.Component.Member.Request.CustomRegexp;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.*;

import java.util.List;
import java.util.Objects;

@Entity
// TODO : test from 1m
@Table(name = "member_center", indexes = {
        @Index(name = "idx_login_cluster", columnList = "email, password")
        , @Index(name = "idx_mem_email", columnList = "email")
        , @Index(name = "idx_mem_deleted", columnList = "deleted")
        , @Index(name = "idx_mem_enabled", columnList = "enabled")
})
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@SQLDelete(sql = "UPDATE member_center SET deleted = true WHERE id=?")
@FilterDefs({
        @FilterDef(name = "deletedFilter", parameters = @ParamDef(name = "isDeleted", type = Boolean.class)),
        @FilterDef(name = "enabledFilter", parameters = @ParamDef(name = "isEnabled", type = Boolean.class)),
})
@Filters({
        @Filter(name = "deletedFilter", condition = "deleted = :isDeleted"),
        @Filter(name = "enabledFilter", condition = "enabled = :isEnabled")
})
@DynamicUpdate
@EntityListeners(DefaultEntityListener.class)
@Schema(title = "會員中心")
public class MemberCenter extends BaseEntity {
    @Schema(name = "電子信箱")
    @Column(name = "email", unique = true)
    @NotNull
    private String email;

    @Schema(name = "密碼")
    @Column(name = "password")
    @NotNull
    private String password;

    @Schema(name = "使用者公開名稱")
    @Column(name = "name")
    @NotNull
    @Size(message = "name.size", min = CustomRegexp.name_min, max = CustomRegexp.name_max)
    private String name;

    @Schema(name = "使用者公開暱稱")
    @Column(name = "nickname")
    @NotNull
    private String nickname;

    @Schema(name = "是否刪除")
    @Column(name = "deleted")
    private Boolean deleted;

    @Schema(name = "是否啟用")
    @Column(name = "enabled")
    private Boolean enabled;

    @Schema(name = "更新密碼驗證碼")
    @Column(name = "change_password")
    private String changePassword;

    @Schema(name = "會員權限")
    @ManyToMany
    @JoinTable(
            name = "member_roles",
            joinColumns = {@JoinColumn(name = "member_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    @ToString.Exclude
    private List<MemberRole> memberRoles;

    public String getRoles() {
        return String.join(",", memberRoles
                .stream()
                .map(MemberRole::getName)
                .toList());
    }

    @Schema(name = "TokenList")
    @OneToMany(mappedBy = "memberCenter")
    @ToString.Exclude
    private List<AuthCenter> authCenters;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MemberCenter that = (MemberCenter) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
