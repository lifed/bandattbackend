package com.cloud.bandott.Core.Member;

import com.cloud.bandott.Presentation.Component.Member.MemberInfoDto;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface MemberCenterMapper {

    MemberCenter memberInfoDtoToMemberCenter(MemberInfoDto memberInfoDto);

    @InheritInverseConfiguration(name = "memberInfoDtoToMemberCenter")
    MemberInfoDto memberCenterToMemberInfoDto(MemberCenter memberCenter);

    @InheritConfiguration(name = "memberInfoDtoToMemberCenter")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    MemberCenter updateMemberInfoDtoFromToMemberCenter(MemberInfoDto memberInfoDto, @MappingTarget MemberCenter memberCenter);

}
