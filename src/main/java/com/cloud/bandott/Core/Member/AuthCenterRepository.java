package com.cloud.bandott.Core.Member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.util.Streamable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Repository
@RestResource(exported = false)
public interface AuthCenterRepository extends JpaRepository<AuthCenter, Long> {
    // 註銷該用戶的可用Token
    @Transactional
    @Modifying
    @Query("update AuthCenter a set a.usingToken = false where a.memberCenter = ?1 and a.usingToken = true")
    int updateUsingTokenByMemberCenterAndUsingTokenTrue(MemberCenter memberCenter);

    Optional<AuthCenter> findFirstByUsingTokenTrueAndToken(String token);

    // 取得用戶已啟用Token
    Optional<AuthCenter> findFirstByMemberCenter_IdAndUsingTokenTrueOrderByIdDesc(Long id);

    // 取得最後十筆登入紀錄
    @Async
    CompletableFuture<Streamable<AuthCenter>> findTop10ByMemberCenter_IdOrderByIdDesc(Long id);


}
