package com.cloud.bandott.Core.SharedKernel.Contact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactBookInterface extends JpaRepository<ContactBook, Long> {
}
