package com.cloud.bandott.Core.SharedKernel.StoragePath;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public abstract class StoragePath {
    private String rootPath;
    private final String externalPath = "file:./config/properties/file_path.properties";
    private final String internalPath = "classpath:config/properties/file_path.properties";

    protected StoragePath(String suffixPath) {
        Properties properties = new Properties();
        if (!loadProperties(properties, externalPath)) {
            loadProperties(properties, internalPath);
        }
        if (!properties.isEmpty()) {
            // Fixme: 要讓File.separator變成跳脫字元所以放兩個?
            rootPath = properties.getProperty("storage.root").replaceAll("\\.",
                    File.separator + File.separator) + suffixPath;
        }

    }

    private boolean loadProperties(Properties properties, String path) {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            Resource resource = resolver.getResource(path);
            if (resource.exists()) {
                properties.load(resource.getInputStream());
                return true;
            } else {
                log.info("無法讀取設定檔：{}", path);
            }
        } catch (IOException e) {
            log.error("讀取設定檔時出錯：{}", path, e);
        }
        return false;
    }

    public String getRootPath() {
        return rootPath;
    }
}
