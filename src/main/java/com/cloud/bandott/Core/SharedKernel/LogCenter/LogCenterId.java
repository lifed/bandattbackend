package com.cloud.bandott.Core.SharedKernel.LogCenter;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Embeddable
public class LogCenterId implements Serializable {

    @Schema(name = "流水號")
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    private Long id;

    @Schema(name = "UUID")
    @Column(name = "uuid", nullable = false, updatable = false)
    private UUID uuid;
}
