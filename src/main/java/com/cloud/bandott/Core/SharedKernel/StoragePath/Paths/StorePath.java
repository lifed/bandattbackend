package com.cloud.bandott.Core.SharedKernel.StoragePath.Paths;

import com.cloud.bandott.Core.SharedKernel.StoragePath.StoragePath;

public class StorePath extends StoragePath {
    public StorePath(Long id) {
        super("store/" + id + "/");

    }
}
