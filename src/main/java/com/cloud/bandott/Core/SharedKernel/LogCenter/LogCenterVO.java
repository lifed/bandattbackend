package com.cloud.bandott.Core.SharedKernel.LogCenter;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class LogCenterVO {
    // what
    private String moduleName;
    private Long moduleId;

    // who
    private Long userId;
    private String fromIP;

    // where
    private String actionLocation;

    // when
    // private LocalDateTime createdAt;

    // why
    private String actionReason;

    // how
    private ModuleAction moduleAction;

    public void setFromIPByHeaders(HttpServletRequest request) {
        try {
            fromIP = request.getRemoteAddr();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LogCenterVO(Object o, ModuleAction moduleAction) {
        this.moduleName = o.getClass().getSimpleName();
        this.moduleId = ((BaseEntity) o).getId();
        this.fromIP = ""; //((BaseEntity) o).getTempFromIP();
        this.moduleAction = moduleAction;
    }
}
