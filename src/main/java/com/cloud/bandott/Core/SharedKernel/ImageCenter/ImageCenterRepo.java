package com.cloud.bandott.Core.SharedKernel.ImageCenter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
@RestResource(exported = false)
public interface ImageCenterRepo<T extends ImageCenter> extends JpaRepository<T, Long> {
    Optional<T> findFirstByImageHashNameOrderByIdDesc(String imageHashName);

    Optional<T> findFirstByModuleIdOrderByIdDesc(Long id);

    List<T> findByModuleId(Long moduleId);

    @Transactional
    @Modifying
    void deleteByModuleIdAndId(Long moduleId, Long id);
}
