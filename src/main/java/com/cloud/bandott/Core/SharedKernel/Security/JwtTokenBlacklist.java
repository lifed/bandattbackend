package com.cloud.bandott.Core.SharedKernel.Security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtTokenBlacklist {
    private final RedisTemplate<String, String> redisTemplate;
    private final JwtUtil jwtUtil;

    public void add(String token) {
        Date expiration = jwtUtil.extractExpirationDate(token);
        long ttl = expiration.getTime() - System.currentTimeMillis();
        redisTemplate.opsForValue().set(token, "", Duration.ofMillis(ttl));
        redisTemplate.expire(token, Duration.ofMillis(ttl));
    }

    public boolean contains(String token) {
        return Boolean.TRUE.equals(redisTemplate.hasKey(token));
    }

    public void printAllKey() {
        System.out.println("print jwt keys ");
        Set<String> keys = redisTemplate.keys("*");
        assert keys != null;
        for (String key : keys) {
            log.info("ttl: {}, key: {}", redisTemplate.getExpire(key, TimeUnit.SECONDS), key);
        }
    }
}
