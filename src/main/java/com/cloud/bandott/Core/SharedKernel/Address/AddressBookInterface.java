package com.cloud.bandott.Core.SharedKernel.Address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface AddressBookInterface extends JpaRepository<AddressBook, Long> {

}
