package com.cloud.bandott.Core.SharedKernel.LogCenter;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
@Transactional
@AllArgsConstructor
public class LogCenterService {
    private LogCenterRepo logCenterRepo;

    public void addLog(LogCenterVO logCenterVO) {


        LogCenter logCenter = new LogCenter();
        // what
        logCenter.setModuleName(logCenterVO.getModuleName());
        logCenter.setModuleId(logCenterVO.getModuleId());
        // who
        logCenter.setFromIP(logCenterVO.getFromIP());
        // TODO:存入目前操作者MemberCenterId
        logCenter.setUserId(0L);
        // where
        logCenter.setActionLocation(logCenterVO.getActionLocation());
        // when, created when new object
        // why
        logCenter.setActionReason(logCenterVO.getActionReason());
        // how
        logCenter.setModuleAction(logCenterVO.getModuleAction().value());
//        System.out.println("logCenter = " + logCenter);
        // logCenterRepo.save(logCenter);
    }

    public CompletableFuture<Optional<LogCenter>> findUserByModuleActionAsync(Long userId, ModuleAction moduleAction) throws ExecutionException, InterruptedException {
        CompletableFuture<Optional<LogCenter>> logCenterOptional = logCenterRepo.findFirstByUserIdAndModuleActionOrderByCreateAtDesc(userId, moduleAction.value());
        logCenterOptional.whenComplete((logCenter, throwable) -> System.out.println("已接收:" + Instant.now()));
        logCenterOptional.exceptionally(throwable -> {
            System.out.println("已失敗:" + Instant.now());
            return null;
        });
        return logCenterOptional;
    }

    public Optional<LogCenter> findUserByModuleAction(Long userId, ModuleAction moduleAction) {
        Optional<LogCenter> logCenterOptional = logCenterRepo.findTop1ByUserIdAndModuleActionOrderByCreateAtDesc(userId, moduleAction.value());
        System.out.println("已接收:" + Instant.now());
        return logCenterOptional;
    }
}
