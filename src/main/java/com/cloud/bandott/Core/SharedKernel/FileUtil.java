package com.cloud.bandott.Core.SharedKernel;

import com.google.common.io.Files;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
public class FileUtil {
    public void createFile(MultipartFile mFile, String Path) {
        File file = new File(Path);
        try {
            Files.createParentDirs(file);
            Files.write(mFile.getResource().getInputStream().readAllBytes(), file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
