package com.cloud.bandott.Core.SharedKernel.LogCenter;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Immutable;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Objects;

@Entity
@Table(name = "log_center", indexes = {
        @Index(name = "idx_moduleName", columnList = "module_name"),
        @Index(name = "idx_modelId", columnList = "module_id"),
        @Index(name = "idx_userId", columnList = "user_id"),
        @Index(name = "idx_moduleAction", columnList = "module_action"),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@DynamicUpdate
// @ExcludeDefaultListeners
@ToString
@Immutable
@Schema(title = "紀錄中心")
@RestResource(exported = false)
public class LogCenter extends BaseEntity {
    // @EmbeddedId
    // @Schema(name = "複主鍵")
    // private LogCenterId logCenterId;

    // what
    @Schema(name = "模組名稱")
    @Column(name = "module_name")
    private String moduleName;

    @Schema(name = "模組ID")
    @Column(name = "module_id")
    private Long moduleId;

    // who
    @Schema(name = "使用者ID")
    @Column(name = "user_id")
    private Long userId;

    @Schema(name = "來源IP")
    @Column(name = "from_ip")
    private String fromIP;

    // where
    @Schema(name = "來源網址")
    @Column(name = "action_location")
    private String actionLocation;

    // when
    // from baseEntity
    // @Schema(name = "操作時間")
    // @Column(name = "created_at")
    // private Instant createdAt;

    // why
    @Schema(name = "操作理由")
    @Column(name = "action_reason")
    private String actionReason;

    // how
    @Schema(name = "操作動作")
    @Column(name = "module_action")
    private String moduleAction;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        LogCenter logCenter = (LogCenter) o;
        return id != null && Objects.equals(id, logCenter.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
