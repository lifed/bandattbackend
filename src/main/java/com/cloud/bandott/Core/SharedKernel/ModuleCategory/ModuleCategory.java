package com.cloud.bandott.Core.SharedKernel.ModuleCategory;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.SharedKernel.DefaultEntityListener;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;
import java.util.Objects;

@Entity
@Table(
        name = "module_category",
        indexes = {
                @Index(name = "idx_category_module", columnList = "module_name, module_id"),
                @Index(name = "idx_category_parent_id", columnList = "parent_id"),
                @Index(name = "idx_category_name", columnList = "name"),
                @Index(name = "idx_category_sort", columnList = "category_sort"),
        }
)
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@DynamicUpdate
@EntityListeners(DefaultEntityListener.class)
@DiscriminatorColumn(name = "module_name")
@Schema(title = "模組類別")
// FIXME: 也許不是加在這裡？
@SQLDelete(sql = "UPDATE module_category SET deleted = true WHERE id=?")
@Where(clause = "deleted is null or deleted = false")
public abstract class ModuleCategory<T extends ModuleCategory<T>> extends BaseEntity {
    @Schema(name = "類別關聯id")
    @Column(name = "module_id")
    private Long moduleId;

    // 在Repo時可比較直觀的撰寫jpql。
    @Schema(name = "父類別Id")
    @Column(name = "parent_id", insertable = false, updatable = false)
    private Long parentId;

    @Schema(name = "名稱")
    @Column(
            name = "name",
            nullable = false,
            length = 300
    )
    private String name;

    @Schema(name = "排序")
    @Column(name = "category_sort")
    private Long categorySort;

    // FIXME: IDE提示錯誤並不代表真的錯誤，有可能是IDE目前無法解析該寫法。
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "moduleCategory")
    @OrderBy("category_sort")
    @ToString.Exclude
    private List<T> moduleCategories;

    @Schema(name = "父類別Id")
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private T moduleCategory;

    @Column(name = "deleted")
    private Boolean deleted;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ModuleCategory that = (ModuleCategory) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
