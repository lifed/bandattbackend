package com.cloud.bandott.Core.SharedKernel.StoragePath.Paths;

import com.cloud.bandott.Core.SharedKernel.StoragePath.StoragePath;

public class MemberPath extends StoragePath {
    public MemberPath(Long id) {
        super("member/" + id + "/");
    }
}
