package com.cloud.bandott.Core.SharedKernel.Address;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Objects;

@Entity
@Table(name = "address_book")
@ToString
@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
@RestResource(exported = false)
public class AddressBook extends BaseEntity {
    @Schema(name = "地址欄1")
    @Column(name = "address_line1")
    private String addressLine1;
    @Schema(name = "地址欄2")
    @Column(name = "address_line2")
    private String addressLine2;

    // 以下輔助
    @Schema(name = "縣/市")
    @Column(name = "address_city")
    private String addressCity;
    @Schema(name = "鎮/區/鄉")
    @Column(name = "address_city_area")
    private String addressCityArea;
    @Schema(name = "郵遞區號")
    @Column(name = "zip_code")
    private Long zipCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AddressBook that = (AddressBook) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
