package com.cloud.bandott.Core.SharedKernel;

import com.cloud.bandott.Core.SharedKernel.LogCenter.LogCenterService;
import com.cloud.bandott.Core.SharedKernel.LogCenter.LogCenterVO;
import com.cloud.bandott.Core.SharedKernel.LogCenter.ModuleAction;
import jakarta.persistence.PostPersist;
import jakarta.persistence.PostRemove;
import jakarta.persistence.PostUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DefaultEntityListener {

    private static LogCenterService logCenterService;

    @Autowired
    public void setLogCenterService(LogCenterService logCenterService) {
        DefaultEntityListener.logCenterService = logCenterService;
    }

    @PostPersist
    public void addAuditLog(Object o) {
        logCenterService.addLog(new LogCenterVO(o, ModuleAction.SQLCreate));
    }

    @PostUpdate
    public void updateAuditLog(Object o) {
        logCenterService.addLog(new LogCenterVO(o, ModuleAction.SQLUpdate));
    }

    @PostRemove
    public void removeAuditLog(Object o) {
        logCenterService.addLog(new LogCenterVO(o, ModuleAction.SQLDelete));
    }
}
