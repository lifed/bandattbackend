package com.cloud.bandott.Core.SharedKernel.ImageCenter;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Entity
@Table(name = "image_center", indexes = {
        @Index(name = "idx_image_module", columnList = "module_name, module_id"),
        @Index(name = "idx_image_sort", columnList = "image_sort"),
})
@DynamicUpdate
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
// @EntityListeners(DefaultEntityListener.class)
@DiscriminatorColumn(name = "module_name")
@Schema(title = "圖片庫")
@RestResource(exported = false)
public abstract class ImageCenter extends BaseEntity {
    @Schema(name = "來源模組id")
    @Column(name = "module_id")
    private Long moduleId;

    @Schema(name = "圖片名稱")
    @Column(name = "image_name")
    private String imageName;

    @Schema(name = "圖片副檔名")
    @Column(name = "image_ext")
    private String imageExtension;

    @Schema(name = "圖片雜湊名稱")
    @Column(name = "image_hash_name")
    private String imageHashName;

    @Schema(name = "圖片實體位置")
    @Column(name = "image_path")
    private String imagePath;

    @Schema(name = "圖片排序")
    @Column(name = "image_sort")
    private Long imageSort;

    public ImageCenter(MultipartFile mFile) {
        setVars(mFile);
    }

    public void setVars(MultipartFile mFile) {
        this.imageName = Files.getNameWithoutExtension(mFile.getOriginalFilename());
        this.imageExtension = Files.getFileExtension(mFile.getOriginalFilename());
        this.imageHashName = Hashing.sha256()
                .hashString(mFile.getOriginalFilename() + mFile.getSize(), StandardCharsets.UTF_8)
                .toString();
    }

    public String makeFilePath() {
        return this.imagePath + this.imageHashName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ImageCenter that = (ImageCenter) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
