package com.cloud.bandott.Core.SharedKernel.ImageCenter;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.SharedKernel.FileUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ImageService {
    private final FileUtil fileUtil;

    public <T extends ImageCenter> void addImage(Long id, String imagePath, MultipartFile mFile, ImageCenterRepo<T> repo, Class<T> imageClass) {
        if (mFile != null && !mFile.isEmpty()) {
            try {
                List<T> imageList = repo.findByModuleId(id);
                imageList.stream().map(image -> new File(image.makeFilePath()).toPath()).forEach(path -> {
                    try {
                        Files.delete(path);
                    } catch (IOException e) {
                        log.error("刪除圖片錯誤：{}", e.toString());
                    }
                });
                repo.deleteAll(imageList);

                T image = imageClass.getDeclaredConstructor(MultipartFile.class).newInstance(mFile);
                image.setModuleId(id);
                image.setImagePath(imagePath);
                fileUtil.createFile(mFile, image.makeFilePath());
                repo.save(image);
                image.setImageSort(image.getId());
                repo.save(image);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                     NoSuchMethodException e) {
                log.error("新增圖片區段處理異常: {}", e.toString());
            }
        }
    }

    public <T extends ImageCenter> File getFile(String hashName, ImageCenterRepo<T> repo) {
        return new File(repo.findFirstByImageHashNameOrderByIdDesc(hashName)
                .orElseThrow(() -> new UserInterfaceMessageException("Not found", "找不到圖片")).makeFilePath());
    }

    public <T extends ImageCenter> String getPicUrl(Long moduleId, String originalUrl, ImageCenterRepo<T> repo) {
        String url = originalUrl;
        try {
            url = url + repo.findFirstByModuleIdOrderByIdDesc(moduleId)
                    .orElseThrow(() -> new UserInterfaceMessageException("Not found", "找不到圖片")).getImageHashName();
        } catch (Exception e) {
            // log.info("使用者沒有圖片, moduleId: {}", moduleId);
            url = "";
        }
        return url;
    }

    public <T extends ImageCenter> void deleteStoreImage(long moduleId, long store_image_id, ImageCenterRepo<T> repo) {
        repo.deleteByModuleIdAndId(moduleId, store_image_id);
    }
}
