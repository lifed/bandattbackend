package com.cloud.bandott.Core.SharedKernel.Contact;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Objects;

@Entity
@Table(name = "contact_book", indexes = {
        @Index(name = "idx_contact_module", columnList = "module_name, module_id"),
        @Index(name = "idx_contact_sort", columnList = "contact_sort"),
})
@ToString
@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
@RestResource(exported = false)
public class ContactBook extends BaseEntity {
    @Schema(name = "模組名稱")
    @Column(name = "module_name")
    private String moduleName;

    @Schema(name = "模組ID")
    @Column(name = "module_id")
    private Long moduleId;

    // 聯絡類別 電話、手機、Line ID，fb message etc...
    @Schema(name = "類別Id")
    @Column(name = "module_category_id")
    private Long moduleCategoryId;

    // may website, phone number, line id or something else...
    @Schema(name = "聯絡方式")
    @Column(name = "contact_info")
    private String contactInfo;

    @Schema(name = "排序 equal id")
    @Column(name = "contact_sort")
    private Long contactSort;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ContactBook that = (ContactBook) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
