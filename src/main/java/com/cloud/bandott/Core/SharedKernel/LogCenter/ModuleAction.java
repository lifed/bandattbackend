package com.cloud.bandott.Core.SharedKernel.LogCenter;

public enum ModuleAction {
    SQLCreate("建立"),
    SQLUpdate("更新"),
    SQLDelete("刪除"),
    SQLQuery("查詢");

    private String moduleAction = "";

    ModuleAction(String moduleAction) {
        this.moduleAction = moduleAction;
    }

    public String value() {
        return moduleAction;
    }

    @Override
    public String toString() {
        return moduleAction.toString();
    }
}
