package com.cloud.bandott.Core.SharedKernel.StoragePath.Paths;

import com.cloud.bandott.Core.SharedKernel.StoragePath.StoragePath;

public class StoreMenuPath extends StoragePath {
    public StoreMenuPath(Long id) {
        super("store/" + id + "/menu/");
    }
}
