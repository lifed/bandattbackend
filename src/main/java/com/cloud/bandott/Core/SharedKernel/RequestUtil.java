package com.cloud.bandott.Core.SharedKernel;

import com.cloud.bandott.Configuration.DomainProperties;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class RequestUtil {
    private final DomainProperties domainProperties;

    public String getRemotePath(HttpServletRequest request) {
        return getUrlPath(true, request, "");
    }

    public String getRemotePath(HttpServletRequest request, String contextPath) {
        return getUrlPath(true, request, contextPath);
    }

    public String getLocalPath(HttpServletRequest request) {
        return getUrlPath(false, request, "");
    }

    public String getLocalPath(HttpServletRequest request, String contextPath) {
        return getUrlPath(false, request, contextPath);
    }

    private String getUrlPath(boolean isRemote, HttpServletRequest request, String contextPath) {
        String scheme = request.getScheme();
        String serverName = isRemote ? request.getServerName() : request.getLocalName();
        int serverPort = isRemote ? request.getServerPort() : request.getLocalPort();

        StringBuilder urlPath = new StringBuilder(String.format("%s://%s", scheme, serverName));
        if (!isDefaultPort(scheme, serverPort)) {
            urlPath.append(String.format(":%d/", serverPort));
        }
        if (!contextPath.isEmpty()) {
            urlPath.append(contextPath);
        }
        return urlPath.toString();
    }

    boolean isDefaultPort(String scheme, int port) {
        return ("https".equalsIgnoreCase(scheme) && port == 443) || ("http".equalsIgnoreCase(scheme) && port == 80);
    }

    /**
     * 清除多餘的斜線或新增斜線
     *
     * @param path {"thePath", "////thePath"}
     * @return /thePath
     */
    public String normalizePath(String path) {
        if ("".equals(path)) {
            return path;
        } else {
            return path.replaceFirst("^/*", "/")
                    .replaceAll("/{2,}", "/")
                    .strip();
        }
    }

    public String getPathFromConfig(boolean isFrontend, String uriPath, HttpServletRequest request) {
        String urlPath = request.getScheme() + "://" + (isFrontend ? domainProperties.getFrontendURL() : domainProperties.getBackendURL());
        String contextPath = isFrontend ? domainProperties.getFrontendContextPath() : domainProperties.getBackendContextPath();
        if (isFrontend && request.getHeader("Origin").contains("localhost")) {
            urlPath = request.getHeader("Origin");
            contextPath = "";
        }
        return urlPath + normalizePath(contextPath + uriPath);
    }
}
