package com.cloud.bandott.Core.SharedKernel.LogCenter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Repository
@RestResource(exported = false)
public interface LogCenterRepo extends JpaRepository<LogCenter, Long> {


    // 使用者建檔、最後修改日期
    Optional<LogCenter> findTop1ByUserIdAndModuleActionOrderByCreateAtDesc(long userId, String moduleAction);

    @Async
    CompletableFuture<Optional<LogCenter>> findFirstByUserIdAndModuleActionOrderByCreateAtDesc(long userId, String moduleAction);

    Optional<LogCenter> findFirstByIdOrderByIdDesc(long id);


    @Async
    CompletableFuture<Optional<LogCenter>> findFirstByModuleNameAndModuleIdOrderByIdDesc(String moduleName, long moduleId);

    // 找尋特定模組的所有操作紀錄
    @Async
    CompletableFuture<Page<LogCenter>> findByModuleNameOrderByIdDesc(String moduleName, Pageable pageable);

    // 找尋特定模組中的特定Id的所有操作紀錄
    @Async
    CompletableFuture<Page<LogCenter>> findByModuleNameAndModuleIdOrderByIdDesc(String moduleName, long moduleId, Pageable pageable);


}
