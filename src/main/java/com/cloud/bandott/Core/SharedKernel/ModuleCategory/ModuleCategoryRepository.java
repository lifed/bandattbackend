package com.cloud.bandott.Core.SharedKernel.ModuleCategory;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface ModuleCategoryRepository<T extends ModuleCategory<T>> extends JpaRepository<T, Long> {

    @Query("""
            from #{#entityName} c 
            where (c.moduleId = ?1 or c.moduleId is null) and c.parentId is null""")
    List<T> findRootCategoriesByModuleId(long moduleId);

    @Query("from ModuleCategory c where (c.moduleId = ?1 or c.moduleId is null) and c.id = ?2")
    List<T> findTop1ByModuleIdAndId(long ModuleId, long id, Pageable pageable);

    @Query("from ModuleCategory c where (c.moduleId = ?1 or c.moduleId is null) and c.name = ?2")
    List<T> findTop1ByModuleIdAndName(long ModuleId, String name, Pageable pageable);

    @Query("from #{#entityName} c where c.moduleId = ?1 and c.parentId = ?2")
    List<T> findCategoriesByModuleIdAndParentId(long moduleId, Long parentId);

    @Query("from #{#entityName} c where c.moduleId = ?1 and c.parentId is null")
    List<T> findCategoriesByModuleIdAndParentIdIsNull(long moduleId);

    @Query("from #{#entityName} c where c.moduleId = ?1 and c.parentId = ?2")
    List<T> findTopCategoriesByModuleIdAndParentId(long moduleId, long parentId, Pageable pageable);

    @Query("""
            from #{#entityName} c 
            where (c.moduleId = ?1 or c.moduleId is null) and (c.parentId = ?2 or c.parentId is null) and c.name = ?3""")
    List<T> findCategoriesByModuleIdAndParentIdAndName(long moduleId, long parentId, String name);

    @Query("from #{#entityName} c where (c.moduleId = ?1 or c.moduleId is null) and (c.parentId = ?2 or c.parentId is null) and c.name = ?3")
    List<T> findTopCategoriesByModuleIdAndParentIdAndName(long moduleId, long parentId, String name, Pageable pageable);

    @Query("from #{#entityName} c where (c.moduleId = ?1 or c.moduleId is null) and (c.parentId = ?2 or c.parentId is null) and c.name = ?3")
    Optional<T> findCategoryByModuleIdAndParentIdAndName(long moduleId, long parentId, String name);

    @Query("""
            select (count(c) > 0) from #{#entityName} c
            where (c.moduleId = ?1 or c.moduleId is null) and (c.parentId = ?2 or c.parentId is null) and c.name = ?3""")
    boolean isExistByModuleIdAndParentIdAndName(long moduleId, long parentId, String name);

    @Modifying
    @Transactional
    @Query("update #{#entityName} c set c.deleted = true where c.id = ?2 and (c.moduleId = ?1 or c.moduleId is null)")
    void deleteByModuleIdAndId(long moduleId, long id);
}
