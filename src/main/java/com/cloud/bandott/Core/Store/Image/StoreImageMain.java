package com.cloud.bandott.Core.Store.Image;

import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageCenter;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DiscriminatorOptions;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "商店主圖片")
@DiscriminatorOptions(force = true)
public class StoreImageMain extends ImageCenter {
    public StoreImageMain(MultipartFile mFile) {
        super(mFile);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreImageMain storeMain = (StoreImageMain) o;
        return getId() != null && Objects.equals(getId(), storeMain.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
