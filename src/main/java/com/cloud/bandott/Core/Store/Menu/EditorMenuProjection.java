package com.cloud.bandott.Core.Store.Menu;

public interface EditorMenuProjection {
    String getStoreName();

    Float getPrice();

    String getName();

    String getDescription();

    Long getMenuCategoryId();

    String getMenuPicUrl();

    Long getMenuPicId();
}
