package com.cloud.bandott.Core.Store.Category;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Store.Projection.StoreCategoryMapProjection;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreCategoryRequest;
import com.cloud.bandott.Presentation.Component.Store.StoreCategoryMapVO;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@AllArgsConstructor
public class StoreCategoryService {
    private final StoreCategoryRepository storeCategoryRepository;
    private final StoreCategoryMapper storeCategoryMapper;

    public List<StoreCategoryMapVO> getStoreCategories() {
        List<StoreCategoryMapProjection> storeCategoryMapProjections = storeCategoryRepository.findAllOrderByCategorySortAsc();
        return storeCategoryMapProjections
                .stream()
                .map(StoreCategoryMapVO::new)
                .collect(Collectors.toList());
    }

    public void addStoreCategory(StoreCategoryRequest storeCategoryRequest) {
        if (storeCategoryRepository.isExistByModuleIdAndParentIdAndName(0, storeCategoryRequest.getId() == null ? 0 : storeCategoryRequest.getId(), storeCategoryRequest.getName())) {
            throw new UserInterfaceMessageException("Category is exists: StoreCategory", "類別名稱已存在");
        }
        StoreCategory storeCategory = storeCategoryMapper.storeCategoryRequestToStoreCategory(storeCategoryRequest);
        storeCategory.setModuleCategory(
                storeCategoryRequest.getId() != null
                        ? storeCategoryRepository.findTop1ByModuleIdAndId(0, storeCategoryRequest.getId(), PageRequest.of(0, 1)).get(0)
                        : null
        );
        storeCategoryRepository.save(storeCategory);
        storeCategory.setCategorySort(storeCategory.getId());
        storeCategoryRepository.save(storeCategory);
    }

    public void modifyStoreCategories(List<StoreCategoryRequest> storeCategoryRequests) {
        List<StoreCategoryRequest> nonEmptyStoreCategoryRequests = storeCategoryRequests.stream()
                .filter(req -> !req.getName().equals(""))
                .toList();
        List<StoreCategory> fromFront = nonEmptyStoreCategoryRequests.stream()
                .map(storeCategoryMapper::storeCategoryRequestToStoreCategory)
                .toList();
        List<StoreCategory> fromDB = storeCategoryRepository.findByParentIdNullOrderByCategorySortAsc();
        // 被刪除
        List<StoreCategory> toBeDeleted = fromDB.stream()
                .filter(dbCategory -> fromFront.stream()
                        .noneMatch(frontCategory -> dbCategory.getId().equals(frontCategory.getId())))
                .toList();
        storeCategoryRepository.deleteAll(toBeDeleted);
        // 新加入
        List<StoreCategory> toBeAdded = fromFront.stream()
                .filter(frontCategory -> fromDB.stream()
                        .noneMatch(dbCategory -> dbCategory.getId().equals(frontCategory.getId())))
                .peek(toBeAddedCategory -> toBeAddedCategory.setModuleCategory(null))
                .toList();
        storeCategoryRepository.saveAll(toBeAdded);
        IntStream.range(0, toBeAdded.size())
                .forEach(i -> toBeAdded.get(i).setCategorySort(toBeAdded.get(i).getId()));
        storeCategoryRepository.saveAll(toBeAdded);
        List<StoreCategory> latestFromDB = storeCategoryRepository.findByParentIdNullOrderByCategorySortAsc();
        // 修改
        Map<Long, StoreCategory> idToStoreCategoryMap = fromFront.stream()
                .collect(Collectors.toMap(StoreCategory::getId, Function.identity()));
        // 調整name欄位
        latestFromDB.stream()
                .filter(dbCategory -> idToStoreCategoryMap.containsKey(dbCategory.getId()))
                .forEach(dbCategory -> dbCategory.setName(idToStoreCategoryMap.get(dbCategory.getId()).getName()));
        // 調整CategorySort欄位
        // 建立map
        Map<String, StoreCategory> dbCategoryMap = latestFromDB.stream()
                .collect(Collectors.toMap(StoreCategory::getName, Function.identity()));
        // 透過map查找相對應的物件
        List<StoreCategory> list = nonEmptyStoreCategoryRequests.stream()
                .map(scFromDb -> dbCategoryMap.get(scFromDb.getName()))
                .filter(Objects::nonNull)
                .toList();
        // 複製一份db目前categorySort的排序
        List<Long> storeCategoriesSort = latestFromDB.stream()
                .map(StoreCategory::getCategorySort)
                .toList();
        // 再把sort的大小依序填上
        IntStream.range(0, list.size())
                .forEach(i -> list.get(i).setCategorySort(storeCategoriesSort.get(i)));
        storeCategoryRepository.saveAll(list);
    }

    public void modifyStoreCategories(long store_category_id, StoreCategoryRequest storeCategoryRequest) {
        StoreCategory storeCategory = storeCategoryRepository.getReferenceById(store_category_id);
        storeCategoryRepository.save(storeCategoryMapper.updateStoreCategoryFromStoreCategoryRequest(storeCategoryRequest, storeCategory));
    }

    public void adjustStoreCategorySort(long source_id, long target_id) {
        StoreCategory sourceEntity = storeCategoryRepository.getReferenceById(source_id);
        StoreCategory targetEntity = storeCategoryRepository.getReferenceById(target_id);
        long tmpSort = sourceEntity.getCategorySort();
        sourceEntity.setCategorySort(targetEntity.getCategorySort());
        targetEntity.setCategorySort(tmpSort);
        storeCategoryRepository.save(sourceEntity);
        storeCategoryRepository.save(targetEntity);
    }

    public void deleteStoreCategory(long store_category_id) {
        storeCategoryRepository.deleteById(store_category_id);
    }

}
