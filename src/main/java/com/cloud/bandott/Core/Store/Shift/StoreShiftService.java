package com.cloud.bandott.Core.Store.Shift;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Store.StoreInfo;
import com.cloud.bandott.Core.Store.StoreInfoService;
import com.cloud.bandott.Core.Store.StoreMapper;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreShiftRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StoreShiftService {
    private final StoreShiftRepository storeShiftRepository;
    private final StoreInfoService storeInfoService;
    private final StoreMapper storeMapper;

    public void addStoreShift(StoreShiftRequest shiftRequest) {
        storeShiftRepository.save(storeMapper.storeShiftRequestToStoreShift(shiftRequest));
    }

    public void addStoreShifts(long storeId, StoreShiftRequest[] shiftRequests) {
        for (StoreShiftRequest storeShiftRequest : shiftRequests) {
            storeShiftRequest.setStoreInfoId(storeId);
            storeShiftRepository.save(storeMapper.storeShiftRequestToStoreShift(storeShiftRequest));
        }
    }

    public void modifyStoreShift(long storeId, long shift_id, StoreShiftRequest shiftRequest) {
        StoreShift storeShift = storeShiftRepository.findFirstByIdAndStoreInfo_Id(shift_id, storeId)
                .orElseThrow(() -> new UserInterfaceMessageException("Can not find Shift", "找不到該營業時間"));
        storeShiftRepository.save(storeMapper.updateStoreShiftFromStoreShiftRequest(shiftRequest, storeShift));
    }

    public void deleteStoreShift(long store_id, long shift_id) {
        StoreInfo storeInfo = new StoreInfo();
        storeInfo.setId(store_id);
        storeShiftRepository.deleteByIdAndStoreInfo(shift_id, storeInfo);
    }
}
