package com.cloud.bandott.Core.Store.Menu;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategory;
import com.cloud.bandott.Core.Store.StoreInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.Objects;


@Entity
@Table(name = "store_menu")
@DynamicUpdate
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "店家菜單")
@SQLDelete(sql = "UPDATE store_menu SET deleted = true WHERE id=?")
@Where(clause = "deleted is null or deleted = false")
public class StoreMenu extends BaseEntity {
    @Schema(name = "菜餚名稱")
    @Column(name = "name")
    @NotNull
    private String name;

    @Schema(name = "菜餚描述")
    @Column(name = "description")
    private String description;

    @Schema(name = "菜餚價格")
    @Column(name = "price")
    @NotNull
    private Float price;

    @Schema(name = "菜單排序")
    @Column(name = "sort")
    private Long sort;

    @Column(name = "deleted")
    private Boolean deleted;

    @Schema(name = "店家Id")
    @ManyToOne
    @JoinColumn(name = "store_info_id", referencedColumnName = "id")
    @NotNull
    private StoreInfo storeInfo;

    @Schema(name = "菜單類別id")
    @ManyToOne
    @JoinColumn(name = "module_category_id", referencedColumnName = "id")
    @NotNull
    private StoreMenuCategory storeMenuCategory;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreMenu storeMenu = (StoreMenu) o;
        return id != null && Objects.equals(id, storeMenu.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
