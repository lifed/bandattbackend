package com.cloud.bandott.Core.Store.Menu;

import com.cloud.bandott.Core.Store.StoreInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Repository
@RestResource(exported = false)
public interface StoreMenuRepository extends JpaRepository<StoreMenu, Long> {
    Optional<StoreMenu> findByStoreInfo_IdAndId(Long storeId, Long menuId);

    Set<StoreMenu> findByStoreInfo_Id(Long id);

    @Transactional
    @Modifying
    void deleteByIdAndStoreInfo(Long id, StoreInfo storeInfo);

    @Query("""
                select si.name as storeName,
                sm.name as name,
                sm.price as price,
                sm.storeMenuCategory.id as menuCategoryId,
                sm.description as description,
                COALESCE(CONCAT(:url, ic.imageHashName), NULL) as menuPicUrl,
                ic.id as menuPicId
                from StoreMenu sm
                right join fetch StoreInfo si on si.id = sm.storeInfo.id AND sm.id = :menuId and (sm.deleted is null or sm.deleted = false)
                left join FETCH ImageCenter ic on type(ic) = StoreMenuImages and ic.moduleId = :menuId
                where si.id = :storeId
                order by sm.sort asc
            """)
    EditorMenuProjection getEditorMenuByStoreIdAndMenuId(Long storeId, Long menuId, String url);

}
