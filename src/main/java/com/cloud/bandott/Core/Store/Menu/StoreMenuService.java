package com.cloud.bandott.Core.Store.Menu;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.cloud.bandott.Core.SharedKernel.StoragePath.Paths.StoreMenuPath;
import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategoryService;
import com.cloud.bandott.Core.Store.Menu.Image.StoreMenuImages;
import com.cloud.bandott.Core.Store.Menu.Image.StoreMenuImagesRepo;
import com.cloud.bandott.Core.Store.Menu.Image.StoreMenuImagesService;
import com.cloud.bandott.Core.Store.StoreInfo;
import com.cloud.bandott.Presentation.Component.Store.Menu.EditorStoreMenu;
import com.cloud.bandott.Presentation.Component.Store.Menu.MenuCategory;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreMenuRequest;
import com.cloud.bandott.Presentation.Component.Store.StoreImagesController;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class StoreMenuService {
    private final StoreMenuImagesService storeMenuImagesService;
    private final ImageService imageService;
    private final StoreMenuImagesRepo storeMenuImagesRepo;
    private final StoreMenuRepository storeMenuRepository;
    private final StoreMenuMapper storeMenuMapper;
    private final StoreMenuCategoryService menuCategoryService;

    public void addMenu(StoreMenuRequest menuRequest) {
        StoreMenu storeMenu = storeMenuMapper.storeMenuRequestToStoreMenu(menuRequest);
        storeMenuRepository.save(storeMenu);
        storeMenu.setSort(storeMenu.getId());
        imageService.addImage(storeMenu.getId(),
                new StoreMenuPath(storeMenu.getId()).getRootPath(),
                menuRequest.imageFile(),
                storeMenuImagesRepo,
                StoreMenuImages.class);
        storeMenuRepository.save(storeMenu);
    }

    public void addMenus(StoreMenuRequest[] storeMenuRequest) {
        for (StoreMenuRequest menuRequest : storeMenuRequest) {
            addMenu(menuRequest);
        }
    }

    public void modifyMenu(StoreMenuRequest menuRequest) {
        StoreMenu storeMenu = storeMenuRepository.findByStoreInfo_IdAndId(menuRequest.storeId(), menuRequest.menuId())
                .orElseThrow(() -> new UserInterfaceMessageException("Not found menu in the store.", "找不到菜單"));
        storeMenuRepository.save(storeMenuMapper.updateStoreMenuFromStoreMenuRequest(menuRequest, storeMenu));
        storeMenuImagesService.addMenuImage(menuRequest.storeId(), storeMenu, menuRequest.imageFile());
    }

    public void deleteMenu(long storeId, long menuId) {
        storeMenuRepository.deleteByIdAndStoreInfo(menuId, StoreInfo.builder().id(storeId).build());
    }

    public Set<StoreMenu> getMenus(Long storeId) {
        return storeMenuRepository.findByStoreInfo_Id(storeId);
    }

    public EditorStoreMenu getMenuByStoreIdAndMenuId(Long storeId, Long menuId, String url) {
        List<MenuCategory> menuCategoryList = menuCategoryService.getMenuCategoriesByStoreId(storeId);
        EditorMenuProjection editorMenuProjection = storeMenuRepository.getEditorMenuByStoreIdAndMenuId(storeId, menuId, url + "/api/store/p/file/" + StoreImagesController.ImageType.menu + "/");
        return storeMenuMapper.EditorMenuProjectiontoEditorStoreMenu(editorMenuProjection, menuCategoryList);
    }
}
