package com.cloud.bandott.Core.Store.Menu.Category;

import com.cloud.bandott.Core.SharedKernel.ModuleCategory.ModuleCategoryRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RestResource(exported = false)
public interface StoreMenuCategoryRepo extends ModuleCategoryRepository<StoreMenuCategory> {
    List<StoreMenuCategory> findByModuleIdOrderByCategorySortAsc(Long moduleId);
}
