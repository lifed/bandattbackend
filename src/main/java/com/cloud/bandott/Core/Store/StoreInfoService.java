package com.cloud.bandott.Core.Store;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.cloud.bandott.Core.SharedKernel.StoragePath.Paths.StorePath;
import com.cloud.bandott.Core.Store.Category.StoreCategory;
import com.cloud.bandott.Core.Store.Category.StoreCategoryRepository;
import com.cloud.bandott.Core.Store.Enum.StoreStatesEnum;
import com.cloud.bandott.Core.Store.Image.StoreImageBanner;
import com.cloud.bandott.Core.Store.Image.StoreImageBannerRepo;
import com.cloud.bandott.Core.Store.Image.StoreImageMain;
import com.cloud.bandott.Core.Store.Image.StoreImageMainRepo;
import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategoryService;
import com.cloud.bandott.Core.Store.Projection.StoreInfoProjection;
import com.cloud.bandott.Presentation.Component.Store.*;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreInfoRequest;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreBasicInfoVO;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreInfoResponse;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreMenuInfoVO;
import com.cloud.bandott.Presentation.Component.Store.Response.StoresResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class StoreInfoService {
    private final StoreInfoRepository storeInfoRepository;
    private final ImageService imageService;
    private final StoreImageMainRepo imageMainRepo;
    private final StoreImageBannerRepo imageBannerRepo;
    private final StoreMenuCategoryService storeMenuCategoryService;
    private final StoreMapper storeMapper;
    private final StoreCategoryRepository storeCategoryRepository;

    public StoresResponse getShops(StoreReq storeReq, String urlPath) {
        Optional<Integer> pages = storeReq.pages();
        int page = pages.isPresent() && pages.get() > 1 ? pages.get() : 1;
        PageRequest pageRequest = PageRequest.of(page - 1, 9, Sort.by("id").ascending());
        Optional<Integer> unFilterPrice = storeReq.price();
        Integer filterPrice = unFilterPrice.isPresent() && unFilterPrice.get() >= 1 && unFilterPrice.get() <= 5 ? unFilterPrice.get() : null;
        Page<StoreInfoProjection> projectionPage = storeInfoRepository
                .findShopsByFilter(pageRequest,
                        urlPath + "/api/store/p/file/" + StoreImagesController.ImageType.main + "/",
                        urlPath + "/api/store/p/file/" + StoreImagesController.ImageType.banner + "/",
                        StoreStatesEnum.Public,
                        storeReq.name(),
                        filterPrice,
                        storeReq.storeCategoryId());
        List<StoreBasicInfoVO> stores = projectionPage.stream()
                .map(StoreBasicInfoVO::fromProjection).toList();
        StoresResponse storesResponse = new StoresResponse();
        storesResponse.setSize(projectionPage.getSize());
        storesResponse.setTotalElements(projectionPage.getTotalElements());
        storesResponse.setTotalPages(projectionPage.getTotalPages());
        storesResponse.setPage(page);
        storesResponse.setStores(stores);
        return storesResponse;
    }

    public StoreMenuInfoVO getShopInfoById(Long storeId, String urlPath) {
        StoreInfo storeInfo = storeInfoRepository.findById(storeId)
                .orElseThrow(() -> new UserInterfaceMessageException("Not Found", "找不到該店家資訊"));
        return storeMapper.storeInfoToStoreMenuInfoVO(storeInfo);
    }

    public StoreMenuInfoVO getShopDetailById(Long storeId, String urlPath) {
        StoreInfo storeInfo = storeInfoRepository.findById(storeId)
                .orElseThrow(() -> new UserInterfaceMessageException("Not Found", "找不到該店家資訊"));
        StoreMenuInfoVO storeMenuInfoVO = storeMapper.storeInfoToStoreMenuInfoVO(storeInfo);
        storeMenuInfoVO.setMenuType(storeMenuCategoryService.getStoreMenuCategories(storeId, urlPath));
        return storeMenuInfoVO;
    }

    public StoreInfoResponse addStore(StoreInfoRequest storeInfoRequest) {
        StoreInfo storeInfo = storeInfoRepository.save(storeMapper.storeInfoRequestToStoreInfo(storeInfoRequest));
        imageService.addImage(storeInfo.getId(), new StorePath(storeInfo.getId()).getRootPath(), storeInfoRequest.getMainImage(), imageMainRepo, StoreImageMain.class);
        imageService.addImage(storeInfo.getId(), new StorePath(storeInfo.getId()).getRootPath(), storeInfoRequest.getBannerImage(), imageBannerRepo, StoreImageBanner.class);
        return new StoreInfoResponse();
    }

    public StoreInfoResponse renewStore(long storeId, StoreInfoRequest storeInfoRequest) {
        StoreInfo storeInfo = storeMapper.storeInfoRequestToStoreInfo(storeInfoRequest);
        storeInfo.setId(storeId);
        storeInfoRepository.save(storeInfo);
        return new StoreInfoResponse();
    }

    public StoreInfoResponse modifyStore(long storeId, StoreInfoRequest storeInfoRequest) {
        StoreInfo storeInfo = storeInfoRepository.findById(storeId)
                .orElseThrow(() -> new UserInterfaceMessageException("Not found store", "找不到店家"));
        storeMapper.updateStoreInfoFromStoreInfoRequest(storeInfoRequest, storeInfo);
        storeInfo.setStoreCategory(StoreCategory.builder().id(storeInfoRequest.getStoreCategoryId()).build());
        imageService.addImage(storeInfo.getId(), new StorePath(storeInfo.getId()).getRootPath(), storeInfoRequest.getMainImage(), imageMainRepo, StoreImageMain.class);
        imageService.addImage(storeInfo.getId(), new StorePath(storeInfo.getId()).getRootPath(), storeInfoRequest.getBannerImage(), imageBannerRepo, StoreImageBanner.class);
        storeInfoRepository.save(storeInfo);
        return new StoreInfoResponse();
    }

    public StoreInfoResponse deleteStore(long storeId) {
        storeInfoRepository.deleteById(storeId);
        return new StoreInfoResponse();
    }

    public EditorStoreInfoVO getStoreInfoEditorById(Long storeId, String url) {
        EditorStoreInfoVO editorStoreInfoVO;
        if (storeId == null) {
            editorStoreInfoVO = new EditorStoreInfoVO();
        } else {
            editorStoreInfoVO = new EditorStoreInfoVO(storeInfoRepository.findStoreInfoProjectionFirstById(storeId,
                    url + "/api/store/p/file/" + StoreImagesController.ImageType.main + "/",
                    url + "/api/store/p/file/" + StoreImagesController.ImageType.banner + "/"));
        }
        // 店家類別
        List<StoreCategoryMapVO> collect = storeCategoryRepository.findAllOrderByCategorySortAsc()
                .stream()
                .map(StoreCategoryMapVO::new)
                .collect(Collectors.toList());
        editorStoreInfoVO.setStoreCategoryMaps(collect);
        // 店家狀態
        editorStoreInfoVO.setStoreStateJson(getEnumToJson());
        return editorStoreInfoVO;
    }

    private String getEnumToJson() {
        Map<String, String> enumMap = new LinkedHashMap<>();
        Arrays.stream(StoreStatesEnum.values())
                .forEach(value -> enumMap.put(value.name().toLowerCase(), value.value()));
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(enumMap);
    }

    public StoreSuggestion getStoreSuggestionByName(String keyword) {
        List<String> list = storeInfoRepository.findTop5ByNameStartsWithIgnoreCaseOrderByIdAsc(keyword).stream()
                .map(StoreInfo::getName)
                .toList();
        return new StoreSuggestion(list);
    }
}
