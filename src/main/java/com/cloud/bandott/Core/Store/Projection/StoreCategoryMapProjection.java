package com.cloud.bandott.Core.Store.Projection;

public interface StoreCategoryMapProjection {
    Long getId();

    String getName();
}
