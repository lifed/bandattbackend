package com.cloud.bandott.Core.Store;

import com.cloud.bandott.Core.Store.Shift.StoreShift;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreInfoRequest;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreShiftRequest;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreBasicInfoVO;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreMenuInfoVO;
import org.mapstruct.*;

// https://stackoverflow.com/questions/42367081/map-a-dto-to-an-entity-retrieved-from-database-if-dto-has-id-using-mapstruct/42375045#42375045
@Mapper(componentModel = "spring")
public interface StoreMapper {
    @Mapping(source = "storeName", target = "name")
    @Mapping(target = "storeCategory", ignore = true)
    StoreInfo storeInfoRequestToStoreInfo(StoreInfoRequest storeInfoRequest);

    @InheritInverseConfiguration(name = "storeInfoRequestToStoreInfo")
    StoreInfoRequest storeInfoToStoreInfoRequest(StoreInfo storeInfo);

    @InheritConfiguration(name = "storeInfoRequestToStoreInfo")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    StoreInfo updateStoreInfoFromStoreInfoRequest(StoreInfoRequest storeInfoRequest, @MappingTarget StoreInfo storeInfo);

    @Mapping(source = "storeInfoId", target = "storeInfo.id")
    StoreShift storeShiftRequestToStoreShift(StoreShiftRequest storeShiftRequest);

    @InheritInverseConfiguration(name = "storeShiftRequestToStoreShift")
    StoreShiftRequest storeShiftToStoreShiftRequest(StoreShift storeShift);

    @InheritConfiguration(name = "storeShiftRequestToStoreShift")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    StoreShift updateStoreShiftFromStoreShiftRequest(StoreShiftRequest storeShiftRequest, @MappingTarget StoreShift storeShift);

    @AfterMapping
    default void linkStoreShifts(@MappingTarget StoreInfo storeInfo) {
        storeInfo.getStoreShifts().forEach(storeShift -> storeShift.setStoreInfo(storeInfo));
    }

    @AfterMapping
    default void linkStoreMenus(@MappingTarget StoreInfo storeInfo) {
        storeInfo.getStoreMenus().forEach(storeMenu -> storeMenu.setStoreInfo(storeInfo));
    }

    @Mapping(target = "storeCategoryName", source = "storeInfo.storeCategory.name")
    StoreMenuInfoVO storeInfoToStoreMenuInfoVO(StoreInfo storeInfo);

    @Mapping(target = "storeCategoryId", source = "storeInfo.storeCategory.id")
    StoreBasicInfoVO storeInfoToStoreBasicInfoVO(StoreInfo storeInfo);
}
