package com.cloud.bandott.Core.Store.Enum;

public enum StoreStatesEnum {
    Public("已公開"), Hidden("未公開"), TemporarilyClosed("暫停停業"), PermanentlyClosed("永久停業");
    private String status;

    StoreStatesEnum(String status) {
        this.status = status;
    }

    public String value() {
        return this.status;
    }
}
