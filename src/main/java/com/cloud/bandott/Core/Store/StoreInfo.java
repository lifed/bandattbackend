package com.cloud.bandott.Core.Store;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.Store.Category.StoreCategory;
import com.cloud.bandott.Core.Store.Enum.StoreStatesEnum;
import com.cloud.bandott.Core.Store.Menu.StoreMenu;
import com.cloud.bandott.Core.Store.Shift.StoreShift;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "store_info", indexes = {
        @Index(name = "idx_store_info_name", columnList = "name"),
})
@DynamicUpdate
@SelectBeforeUpdate
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "店家清冊")
public class StoreInfo extends BaseEntity {
    @Schema(name = "店家名稱")
    @Column(name = "name")
    @NotNull
    private String name;

    @Schema(name = "店家介紹")
    @Column(
            name = "description"
            , length = 4000
    )
    private String description;

    @Schema(name = "店家是否營業(enum)")
    @Column(name = "store_states")
    private StoreStatesEnum storeStatesEnum;

    @Schema(name = "店家價位")
    @Column(name = "price")
    private Integer price;

    // not used.
    @Schema(name = "營業時段")
    @OneToMany(mappedBy = "storeInfo", cascade = CascadeType.REMOVE)
    @ToString.Exclude
    private List<StoreShift> storeShifts;

    @Schema(name = "菜單內容")
    @OneToMany(mappedBy = "storeInfo", cascade = CascadeType.REMOVE)
    @ToString.Exclude
    private List<StoreMenu> storeMenus;

    // @Schema(name = "店家圖片")
    // @OneToMany(mappedBy = "moduleId", cascade = CascadeType.REMOVE)
    // @ToString.Exclude
    // private List<StoreImageMain> storeImageMains;
    //
    // @Schema(name = "店家橫幅圖片")
    // @OneToMany(mappedBy = "moduleId", cascade = CascadeType.REMOVE)
    // @ToString.Exclude
    // private List<StoreImageBanner> storeImageBanners;

    // 共用的Entity無法透過關聯取存
    // @Schema(name = "店家菜單分類")
    // @OneToMany(mappedBy = "storeInfo", cascade = CascadeType.ALL)
    // @ToString.Exclude
    // private List<StoreMenuCategory> storeMenuCategories;

    @Schema(name = "店家分類")
    @ManyToOne
    @JoinColumn(name = "store_category_id", referencedColumnName = "id")
    private StoreCategory storeCategory;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreInfo storeInfo = (StoreInfo) o;
        return id != null && Objects.equals(id, storeInfo.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
