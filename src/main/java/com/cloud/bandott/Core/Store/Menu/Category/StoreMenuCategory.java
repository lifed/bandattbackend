package com.cloud.bandott.Core.Store.Menu.Category;

import com.cloud.bandott.Core.SharedKernel.ModuleCategory.ModuleCategory;
import com.cloud.bandott.Core.Store.Menu.StoreMenu;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;
import java.util.Objects;

@Entity
@DynamicUpdate
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "菜單類別")
@DiscriminatorOptions(force = true)
@SQLDelete(sql = "UPDATE module_category SET deleted = true WHERE id=?")
@Where(clause = "deleted is null or deleted = false")
public class StoreMenuCategory extends ModuleCategory<StoreMenuCategory> {

    // @Schema(name = "店家storeId")
    // @ManyToOne
    // @JoinColumn(name = "module_id", referencedColumnName = "id")
    // @NotNull
    // private StoreInfo storeInfo;

    @Schema(name = "菜單分類")
    @OneToMany(mappedBy = "storeMenuCategory")
    @ToString.Exclude
    private List<StoreMenu> storeMenus;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreMenuCategory that = (StoreMenuCategory) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
