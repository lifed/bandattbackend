package com.cloud.bandott.Core.Store;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;

import java.util.Objects;

@Entity
@Table(name = "Store_web_link")
@ToString
@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
public class StoreWebLink extends BaseEntity {
    @Schema(name = "店家Id")
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "store_info_id", nullable = false)
    @NotNull
    private StoreInfo storeInfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreWebLink that = (StoreWebLink) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
