package com.cloud.bandott.Core.Store.Category;

import com.cloud.bandott.Presentation.Component.Store.Request.StoreCategoryRequest;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreCategoryResponse;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface StoreCategoryMapper {

    StoreCategory storeCategoryResponseToStoreCategory(StoreCategoryResponse storeCategoryResponse);

    StoreCategoryResponse storeCategoryToStoreCategoryResponse(StoreCategory storeCategory);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    StoreCategory updateStoreCategoryFromStoreCategoryResponse(StoreCategoryResponse storeCategoryResponse, @MappingTarget StoreCategory storeCategory);

    @AfterMapping
    default void linkModuleCategories(@MappingTarget StoreCategory storeCategory) {
        if (storeCategory.getModuleCategories() != null) {
            storeCategory.getModuleCategories().forEach(moduleCategory -> moduleCategory.setModuleCategory(storeCategory));
        }
    }

    @Mapping(source = "id", target = "moduleCategory.id")
    StoreCategory storeCategoryRequestToStoreCategory(StoreCategoryRequest storeCategoryRequest);

    @Mapping(source = "moduleCategory.id", target = "id")
    StoreCategoryRequest storeCategoryToStoreCategoryRequest(StoreCategory storeCategory);

    @Mapping(source = "id", target = "moduleCategory.id")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    StoreCategory updateStoreCategoryFromStoreCategoryRequest(StoreCategoryRequest storeCategoryRequest, @MappingTarget StoreCategory storeCategory);
}
