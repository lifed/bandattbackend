package com.cloud.bandott.Core.Store.Menu.Image;

import com.cloud.bandott.Core.SharedKernel.FileUtil;
import com.cloud.bandott.Core.SharedKernel.StoragePath.Paths.StoreMenuPath;
import com.cloud.bandott.Core.Store.Menu.StoreMenu;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@AllArgsConstructor
public class StoreMenuImagesService {
    private final StoreMenuImagesRepo storeMenuImagesRepo;
    private final FileUtil fileUtil;

    public StoreMenuImages addMenuImage(long storeId, StoreMenu storeMenu, MultipartFile mFile) {
        // TODO: 刪除原有圖片才新增圖片
        StoreMenuImages storeMenuImages = null;
        if (mFile != null && !mFile.isEmpty()) {
            storeMenuImages = new StoreMenuImages(mFile);
            storeMenuImages.setModuleId(storeMenu.getId());
            storeMenuImages.setImagePath(new StoreMenuPath(storeId).getRootPath() + storeMenuImages.getImageHashName());
            fileUtil.createFile(mFile, storeMenuImages.makeFilePath());
            storeMenuImagesRepo.save(storeMenuImages);
            storeMenuImages.setImageSort(storeMenuImages.getId());
            storeMenuImagesRepo.save(storeMenuImages);
        }
        return storeMenuImages;
    }
}
