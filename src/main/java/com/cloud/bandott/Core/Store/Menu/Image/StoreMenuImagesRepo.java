package com.cloud.bandott.Core.Store.Menu.Image;

import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageCenterRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreMenuImagesRepo extends ImageCenterRepo<StoreMenuImages> {
}
