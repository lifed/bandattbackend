package com.cloud.bandott.Core.Store.Category;

import com.cloud.bandott.Core.SharedKernel.ModuleCategory.ModuleCategoryRepository;
import com.cloud.bandott.Core.Store.Projection.StoreCategoryMapProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RestResource(exported = false)
public interface StoreCategoryRepository extends ModuleCategoryRepository<StoreCategory> {
    List<StoreCategory> findByParentIdNullOrderByCategorySortAsc();

    @Query("""
            select sc
            from StoreCategory sc
            where sc.parentId is null
            order by sc.categorySort
            """)
    List<StoreCategoryMapProjection> findAllOrderByCategorySortAsc();
}
