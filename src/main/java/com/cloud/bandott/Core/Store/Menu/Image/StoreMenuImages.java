package com.cloud.bandott.Core.Store.Menu.Image;

import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageCenter;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DiscriminatorOptions;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@Schema(title = "商店菜單圖片庫")
@DiscriminatorOptions(force = true)
public class StoreMenuImages extends ImageCenter {
    public StoreMenuImages(MultipartFile mFile) {
        super(mFile);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreMenuImages that = (StoreMenuImages) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
