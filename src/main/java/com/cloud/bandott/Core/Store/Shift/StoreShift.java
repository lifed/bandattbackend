package com.cloud.bandott.Core.Store.Shift;

import com.cloud.bandott.Core.SharedKernel.BaseEntity;
import com.cloud.bandott.Core.Store.StoreInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DynamicUpdate;

import java.util.Objects;

@Entity
@Table(name = "store_Shift")
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@DynamicUpdate
@Schema(title = "店家營業時間")
public class StoreShift extends BaseEntity {
    @Schema(name = "營業日期一到日")
    @Column(name = "store_daily")
    private int storeDaily;
    @Schema(name = "開始營業時間_小時")
    @Column(name = "start_hour")
    private int startHour;
    @Schema(name = "開始營業時間_分鐘")
    @Column(name = "start_min")
    private int startMin;
    @Schema(name = "結束營業時間_小時")
    @Column(name = "end_hour")
    private int endHour;
    @Schema(name = "結束營業時間_小時")
    @Column(name = "end_min")
    private int endMin;

    @Schema(name = "店家id")
    @ManyToOne
    @JoinColumn(name = "store_info_id")
    private StoreInfo storeInfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreShift that = (StoreShift) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
