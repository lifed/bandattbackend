package com.cloud.bandott.Core.Store.Projection;

public interface StoreInfoProjection {
    Long getId();

    String getName();

    String getDescription();

    String getStoreStatesEnum();

    Long getStoreCategoryId();

    String getPrice();

    String getImageMainUrl();

    String getImageBannerUrl();

    Long getImageMainId();

    Long getImageBannerId();

}
