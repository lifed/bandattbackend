package com.cloud.bandott.Core.Store.Menu;

import com.cloud.bandott.Presentation.Component.Store.Menu.EditorStoreMenu;
import com.cloud.bandott.Presentation.Component.Store.Menu.MenuCategory;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreMenuRequest;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface StoreMenuMapper {
    @Mapping(source = "storeId", target = "storeInfo.id")
    @Mapping(source = "menuCategoryId", target = "storeMenuCategory.id")
    StoreMenu storeMenuRequestToStoreMenu(StoreMenuRequest storeMenuRequest);

    @InheritInverseConfiguration(name = "storeMenuRequestToStoreMenu")
    StoreMenuRequest storeMenuToStoreMenuRequest(StoreMenu storeMenu);

    @InheritConfiguration(name = "storeMenuRequestToStoreMenu")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    StoreMenu updateStoreMenuFromStoreMenuRequest(StoreMenuRequest storeMenuRequest, @MappingTarget StoreMenu storeMenu);

    EditorStoreMenu EditorMenuProjectiontoEditorStoreMenu(EditorMenuProjection projection, List<MenuCategory> menuCategoryList);
}
