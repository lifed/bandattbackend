package com.cloud.bandott.Core.Store.Category;

import com.cloud.bandott.Core.SharedKernel.ModuleCategory.ModuleCategory;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.DynamicUpdate;

import java.util.Objects;

@Entity
@DynamicUpdate
@Getter
@Setter
@SuperBuilder
@ToString(callSuper = true)
@NoArgsConstructor
@Schema(title = "店家類別")
@DiscriminatorOptions(force = true)
public class StoreCategory extends ModuleCategory<StoreCategory> {
//    @Schema(name = "店家storeInfoId")
//    @ManyToOne
//    @JoinColumn(name = "module_id", referencedColumnName = "id")
//    private StoreInfo storeInfo;

//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
//    @OrderBy("categorySort")
//    @ToString.Exclude
//    private List<StoreCategory> categories;
//
//    @Schema(name = "父類別Id")
//    @ManyToOne
//    @JoinColumn(name = "parent_id")
//    private StoreCategory category;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StoreCategory that = (StoreCategory) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
