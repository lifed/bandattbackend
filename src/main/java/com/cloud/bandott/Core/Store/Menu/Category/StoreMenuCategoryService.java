package com.cloud.bandott.Core.Store.Menu.Category;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.cloud.bandott.Core.Store.Menu.Image.StoreMenuImagesRepo;
import com.cloud.bandott.Presentation.Component.Store.Menu.MenuCategory;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreMenuCategoryRequest;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreMenuCategoryDto;
import com.cloud.bandott.Presentation.Component.Store.StoreImagesController;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@AllArgsConstructor
public class StoreMenuCategoryService {
    private final StoreMenuCategoryRepo storeMenuCategoryRepo;
    private final StoreMenuCategoryMapper storeMenuCategoryMapper;
    private final ImageService imageService;
    private final StoreMenuImagesRepo storeMenuImagesRepo;

    public void addStoreMenuCategory(long moduleId, StoreMenuCategoryRequest storeMenuCategoryRequest) {
        if (storeMenuCategoryRepo.isExistByModuleIdAndParentIdAndName(moduleId, 0, storeMenuCategoryRequest.getName())) {
            throw new UserInterfaceMessageException("Category is exist: StoreMenuCategory", "已存在該類別");
        }
        StoreMenuCategory storeMenuCategory = storeMenuCategoryMapper.storeMenuCategoryRequestToStoreMenuCategory(storeMenuCategoryRequest);
        storeMenuCategory.setModuleId(moduleId);
        storeMenuCategoryRepo.save(storeMenuCategory);
        storeMenuCategory.setCategorySort(storeMenuCategory.getId());
        storeMenuCategoryRepo.save(storeMenuCategory);
    }

    public void modifyStoreMenuCategory(long moduleId, long menu_category_id, StoreMenuCategoryRequest storeMenuCategoryRequest) {
        StoreMenuCategory storeMenuCategory = null;
        try {
            storeMenuCategory = storeMenuCategoryRepo.findTop1ByModuleIdAndId(moduleId, menu_category_id, PageRequest.ofSize(1)).get(0);
        } catch (Exception e) {
            throw new UserInterfaceMessageException("Category is not exist: StoreMenuCategory", "找不到該店家的此類別");
        }
        storeMenuCategoryRepo.save(storeMenuCategoryMapper.updateStoreMenuCategoryFromStoreMenuCategoryRequest(storeMenuCategoryRequest, storeMenuCategory));
    }

    public void deleteStoreMenuCategory(long moduleId, long storeCategoryId) {
        storeMenuCategoryRepo.deleteByModuleIdAndId(moduleId, storeCategoryId);
    }

    public StoreMenuCategory getStoreMenuCategory(long moduleCategoryId) {
        return storeMenuCategoryRepo.getReferenceById(moduleCategoryId);
    }


    public List<StoreMenuCategoryDto> getStoreMenuCategories(Long moduleId, String urlPath) {
        return storeMenuCategoryRepo
                .findByModuleIdOrderByCategorySortAsc(moduleId).stream()
                .map(obj -> {
                    StoreMenuCategoryDto storeMenuCategoryDto = storeMenuCategoryMapper.storeMenuCategoryToStoreMenuCategoryDto(obj);
                    storeMenuCategoryDto.setStoreMenus(storeMenuCategoryDto.getStoreMenus()
                            .stream()
                            .peek(storeMenu -> storeMenu.setMenuUrl(imageService.getPicUrl(storeMenu.getId(), urlPath + "/api/store/p/file/" + StoreImagesController.ImageType.menu + "/", storeMenuImagesRepo)))
                            .collect(Collectors.toList()));
                    return storeMenuCategoryDto;

                }).collect(Collectors.toList());

    }

    public List<MenuCategory> getMenuCategoriesByStoreId(Long storeId) {
        return storeMenuCategoryRepo.findByModuleIdOrderByCategorySortAsc(storeId).stream()
                .map(storeMenuCategoryMapper::storeMenuCategoryToMenuCategory)
                .toList();
    }

    public void modifyMenuCategoriesByStoreId(Long storeId, List<MenuCategory> menuCategories) {
        List<MenuCategory> nonEmptyMenuCategories = menuCategories.stream()
                .filter(req -> !req.name().equals(""))
                .toList();
        List<StoreMenuCategory> fromFront = nonEmptyMenuCategories.stream()
                .map(storeMenuCategoryMapper::menuCategoryToStoreMenuCategory)
                .toList();
        List<StoreMenuCategory> fromDB = storeMenuCategoryRepo.findByModuleIdOrderByCategorySortAsc(storeId);
        // 被刪除
        List<StoreMenuCategory> toBeDeleted = fromDB.stream()
                .filter(dbCategory -> fromFront.stream()
                        .noneMatch(frontCategory -> dbCategory.getId().equals(frontCategory.getId())))
                .toList();
        storeMenuCategoryRepo.deleteAll(toBeDeleted);
        // 新加入
        List<StoreMenuCategory> toBeAdded = fromFront.stream()
                .filter(frontCategory -> fromDB.stream()
                        .noneMatch(dbCategory -> dbCategory.getId().equals(frontCategory.getId())))
                .peek(toBeAddedCategory -> toBeAddedCategory.setModuleCategory(null))
                .peek(toBeAddedCategory -> toBeAddedCategory.setModuleId(storeId))
                .toList();
        storeMenuCategoryRepo.saveAll(toBeAdded);
        IntStream.range(0, toBeAdded.size())
                .forEach(i -> toBeAdded.get(i).setCategorySort(toBeAdded.get(i).getId()));
        storeMenuCategoryRepo.saveAll(toBeAdded);
        List<StoreMenuCategory> latestFromDB = storeMenuCategoryRepo.findByModuleIdOrderByCategorySortAsc(storeId);
        // 修改
        Map<Long, StoreMenuCategory> idToStoreCategoryMap = fromFront.stream()
                .collect(Collectors.toMap(StoreMenuCategory::getId, Function.identity()));
        // 調整name欄位
        latestFromDB.stream()
                .filter(dbCategory -> idToStoreCategoryMap.containsKey(dbCategory.getId()))
                .forEach(dbCategory -> dbCategory.setName(idToStoreCategoryMap.get(dbCategory.getId()).getName()));
        // 調整CategorySort欄位
        // 建立map
        Map<String, StoreMenuCategory> dbCategoryMap = latestFromDB.stream()
                .collect(Collectors.toMap(StoreMenuCategory::getName, Function.identity()));
        // 透過map查找相對應的物件
        List<StoreMenuCategory> list = nonEmptyMenuCategories.stream()
                .map(scFromDb -> dbCategoryMap.get(scFromDb.name()))
                .filter(Objects::nonNull)
                .toList();
        // 複製一份db目前categorySort的排序
        List<Long> storeCategoriesSort = latestFromDB.stream()
                .map(StoreMenuCategory::getCategorySort)
                .toList();
        // 再把sort的大小依序填上
        IntStream.range(0, list.size())
                .forEach(i -> list.get(i).setCategorySort(storeCategoriesSort.get(i)));
        storeMenuCategoryRepo.saveAll(list);
    }
}
