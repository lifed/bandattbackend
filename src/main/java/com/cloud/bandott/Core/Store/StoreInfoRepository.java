package com.cloud.bandott.Core.Store;

import com.cloud.bandott.Core.Store.Enum.StoreStatesEnum;
import com.cloud.bandott.Core.Store.Projection.StoreInfoProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
@RestResource(exported = false)
public interface StoreInfoRepository extends JpaRepository<StoreInfo, Long> {
    Set<StoreInfo> findTop5ByNameStartsWithIgnoreCaseOrderByIdAsc(String name);

    Page<StoreInfo> findByStoreStatesEnum(StoreStatesEnum storeStatesEnum, Pageable pageable);

    @Query(value = """
                select s.id as id,
                s.name as name,
                s.description as description,
                s.price as price,
                s.storeStatesEnum as storeStatesEnum,
                s.storeCategory.id as storeCategoryId,
                COALESCE(CONCAT(:imgMainUrl, ic_main.imageHashName), NULL) as imageMainUrl,
                COALESCE(CONCAT(:imgBannerUrl, ic_banner.imageHashName), NULL) as imageBannerUrl,
                ic_main.id as imageMainId,
                ic_banner.id as imageBannerId
                from StoreInfo s
                left join FETCH ImageCenter ic_main on s.id = ic_main.moduleId and TYPE(ic_main) = StoreImageMain
                left join FETCH ImageCenter ic_banner on s.id = ic_banner.moduleId and TYPE(ic_banner) = StoreImageBanner
                left join FETCH ModuleCategory mc on s.storeCategory.id = mc.id and TYPE(mc) = StoreCategory
                where s.storeStatesEnum = :storeStatesEnum and
                    (:name is null or s.name like CONCAT(:name, '%')) and
                    (:price is null or s.price = :price) and
                    (:storeCategoryId is null or s.storeCategory.id = :storeCategoryId) and
                    (true or :imgBannerUrl=:imgMainUrl)
            """)
        //     INFO: (true or :imgBannerUrl=:imgMainUrl) 該區段為了規避countQuery找不到的問題。
    Page<StoreInfoProjection> findShopsByFilter(Pageable pageable,
                                                String imgMainUrl,
                                                String imgBannerUrl,
                                                StoreStatesEnum storeStatesEnum,
                                                String name,
                                                Integer price,
                                                Long storeCategoryId);

    @Query("""
                select s.id as id,
                s.name as name,
                s.description as description,
                s.price as price,
                s.storeStatesEnum as storeStatesEnum,
                s.storeCategory.id as storeCategoryId,
                COALESCE(CONCAT(:imgMainUrl, ic_main.imageHashName), NULL) as imageMainUrl,
                COALESCE(CONCAT(:imgBannerUrl, ic_banner.imageHashName), NULL) as imageBannerUrl,
                ic_main.id as imageMainId,
                ic_banner.id as imageBannerId
                from StoreInfo s
                left join FETCH ImageCenter ic_main on s.id = ic_main.moduleId and TYPE(ic_main) = StoreImageMain
                left join FETCH ImageCenter ic_banner on s.id = ic_banner.moduleId and TYPE(ic_banner) = StoreImageBanner
                left join FETCH ModuleCategory mc on s.storeCategory.id = mc.id and TYPE(mc) = StoreCategory
                where s.id = :storeId
            """)
    StoreInfoProjection findStoreInfoProjectionFirstById(Long storeId, String imgMainUrl, String imgBannerUrl);
}
