package com.cloud.bandott.Core.Store.Image;

import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageCenterRepo;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RestResource(exported = false)
public interface StoreImageMainRepo extends ImageCenterRepo<StoreImageMain> {
}
