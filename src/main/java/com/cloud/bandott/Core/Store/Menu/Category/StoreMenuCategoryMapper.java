package com.cloud.bandott.Core.Store.Menu.Category;

import com.cloud.bandott.Presentation.Component.Store.Menu.MenuCategory;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreMenuCategoryRequest;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreMenuCategoryDto;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface StoreMenuCategoryMapper {
    @Mapping(source = "storeId", target = "moduleId")
    StoreMenuCategory storeMenuCategoryRequestToStoreMenuCategory(StoreMenuCategoryRequest storeMenuCategoryRequest);

    @Mapping(source = "moduleId", target = "storeId")
    StoreMenuCategoryRequest storeMenuCategoryToStoreMenuCategoryRequest(StoreMenuCategory storeMenuCategory);

    @Mapping(source = "storeId", target = "moduleId")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    StoreMenuCategory updateStoreMenuCategoryFromStoreMenuCategoryRequest(StoreMenuCategoryRequest storeMenuCategoryRequest, @MappingTarget StoreMenuCategory storeMenuCategory);

    @Mapping(target = "typeName", source = "name")
    StoreMenuCategoryDto storeMenuCategoryToStoreMenuCategoryDto(StoreMenuCategory storeMenuCategory);

    MenuCategory storeMenuCategoryToMenuCategory(StoreMenuCategory storeMenuCategory);

    @InheritConfiguration(name = "storeMenuCategoryToMenuCategory")
    StoreMenuCategory menuCategoryToStoreMenuCategory(MenuCategory menuCategory);

}
