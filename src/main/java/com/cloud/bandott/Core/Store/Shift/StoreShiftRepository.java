package com.cloud.bandott.Core.Store.Shift;

import com.cloud.bandott.Core.Store.StoreInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RestResource(exported = false)
public interface StoreShiftRepository extends JpaRepository<StoreShift, Long> {
    long deleteByIdAndStoreInfo(Long id, StoreInfo storeInfo);

    Optional<StoreShift> findFirstByIdAndStoreInfo_Id(Long shiftId, Long storeId);

}
