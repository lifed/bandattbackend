package com.cloud.bandott.Core.Store.Enum;

import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;

public class StringToStoreStatesEnumConverter implements Converter<String, StoreStatesEnum> {
    @Override
    public StoreStatesEnum convert(String source) {
        return Arrays.stream(StoreStatesEnum.values())
                .filter(e -> e.name().equalsIgnoreCase(source))
                .findFirst()
                .orElse(null);
    }
}
