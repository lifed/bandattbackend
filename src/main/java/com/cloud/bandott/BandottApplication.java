package com.cloud.bandott;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@Slf4j
public class BandottApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(BandottApplication.class);
        Map<String, String> pathMap = new HashMap<>();
        String configPath = "config";
        // 全域設定檔
        pathMap.put("application", System.getProperty("user.dir") + File.separator + configPath + File.separator + "application.properties");
        pathMap.forEach((nameKey, pathValue) -> {
            File property = new File(pathValue);
            if (property.exists()) {
                app.setAdditionalProfiles(property.getAbsolutePath());
                log.info("已載入設定檔名稱: {},檔案路徑: {}", nameKey, pathValue);
            }
        });
        app.run(args);
    }

    public final static boolean isDebug = true;

}
