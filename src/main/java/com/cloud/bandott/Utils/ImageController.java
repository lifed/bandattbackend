package com.cloud.bandott.Utils;

import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/images")

public class ImageController {
    @Resource
    private ImageResourceHttpRequestHandler imageResourceHttpRequestHandler;

    @GetMapping("/{modulename}/{filename}")
    public void download(
            @PathVariable String filename,
            @PathVariable String modulename,
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse
    ) {
        // 注意！可能有資安問題
        try {
            File file = new File(getClass().getResource("/images/" + modulename + "/" + filename).getFile());
            httpServletRequest.setAttribute(ImageResourceHttpRequestHandler.ATTRIBUTE_FILE, file);
            imageResourceHttpRequestHandler.handleRequest(httpServletRequest, httpServletResponse);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
