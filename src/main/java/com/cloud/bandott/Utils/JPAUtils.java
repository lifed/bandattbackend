package com.cloud.bandott.Utils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class JPAUtils {
    private static EntityManagerFactory factory;
    static {
        factory = Persistence.createEntityManagerFactory("postgresql");
    }

    public static EntityManager getEntityManager() {
        return factory.createEntityManager();
    }
}
