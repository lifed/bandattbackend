package com.cloud.bandott.Utils;

import com.cloud.bandott.Core.Member.MemberCenter;
import org.springframework.stereotype.Component;

@Component
public class PermissionUtils {
    public boolean isAdmin(MemberCenter memberCenter) {
        return memberCenter.getMemberRoles()
                .stream()
                .anyMatch(role -> "BACKEND_USER".equals(role.getName()) || "WEB_ADMIN".equals(role.getName()) || "ROOT".equals(role.getName()));
    }
}
