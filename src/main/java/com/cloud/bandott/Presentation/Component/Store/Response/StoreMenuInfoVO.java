package com.cloud.bandott.Presentation.Component.Store.Response;

import com.cloud.bandott.Core.Store.Enum.StoreStatesEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreMenuInfoVO {
    private Long id;
    private String name;
    private String description;
    private StoreStatesEnum storeStatesEnum;
    private List<StoreShiftDto> storeShifts;
    private String storeCategoryName;
    private List<StoreMenuCategoryDto> menuType;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StoreShiftDto {
        private int storeDaily;
        private int startHour;
        private int startMin;
        private int endHour;
        private int endMin;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StoreCategoryDto {
        private String name;
    }
}
