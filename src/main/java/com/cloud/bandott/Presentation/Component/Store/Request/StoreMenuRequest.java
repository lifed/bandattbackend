package com.cloud.bandott.Presentation.Component.Store.Request;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

public record StoreMenuRequest(
        @PathVariable
        Long storeId,
        @PathVariable(required = false)
        Long menuId,
        String name,
        String description,
        Float price,
        Long menuCategoryId,
        MultipartFile imageFile
) {
}
