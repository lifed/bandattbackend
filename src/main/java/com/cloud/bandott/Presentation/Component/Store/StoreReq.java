package com.cloud.bandott.Presentation.Component.Store;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

public record StoreReq(
        @PathVariable(required = false) Optional<Integer> pages,
        @RequestParam(required = false) String name,
        @RequestParam(required = false) Optional<Integer> price,
        @RequestParam(required = false) Long storeCategoryId
) {
}
