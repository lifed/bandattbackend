package com.cloud.bandott.Presentation.Component.Member;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.MemberCenterRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

// https://stackoverflow.com/questions/29681675/how-to-write-an-aspect-pointcut-based-on-an-annotated-parameter/29714549#29714549
@Aspect
@Component
@AllArgsConstructor
@Slf4j
public class EmailHashCodeChecker {
    private final MemberCenterRepository memberCenterRepository;

    @Before("execution(* *(.., @ResetPasswordHashCode (*), ..))")
    public void verifyInvestigationId(JoinPoint joinPoint) {
        String[] argsName = ((MethodSignature) joinPoint.getSignature()).getParameterNames();
        Object[] argsValue = joinPoint.getArgs();
        Map<String, String> requestArgs = new HashMap<>();
        for (int i = 0; i < argsName.length; i++) {
            requestArgs.put(argsName[i], argsValue[i].toString());
        }
        String HashCode = requestArgs.get("HashCode");
        log.info("重新設定密碼的認證碼: {}", HashCode);
        memberCenterRepository.findFirstByChangePasswordOrderByIdAsc(HashCode)
                .orElseThrow(() -> new UserInterfaceMessageException("Bad code.", "驗證碼錯誤"));
    }
}
