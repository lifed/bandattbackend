package com.cloud.bandott.Presentation.Component.Store;

import com.cloud.bandott.Core.Store.Category.StoreCategoryService;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreCategoryRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/store/")
@AllArgsConstructor
@PreAuthorize("hasAnyRole('BACKEND_USER', 'WEB_ADMIN', 'ROOT')")
public class StoreCategoryManagementController {
    private final StoreCategoryService storeCategoryService;

    @GetMapping("/storeCategories")
    public ResponseEntity<List<StoreCategoryMapVO>> getStoreCategories() {
        return new ResponseEntity<>(
                storeCategoryService.getStoreCategories(),
                HttpStatus.OK
        );
    }

    @PostMapping("/storeCategories")
    public ResponseEntity modifyStoreCategories(@RequestBody List<StoreCategoryRequest> storeCategories) {
        storeCategoryService.modifyStoreCategories(storeCategories);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @PostMapping("/category")
    public ResponseEntity addStoreCategory(StoreCategoryRequest storeCategoryRequest) {
        storeCategoryService.addStoreCategory(storeCategoryRequest);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @PostMapping("/category/adjustsort")
    public ResponseEntity adjustSort(@RequestParam long source_id, @RequestParam long target_id) {
        storeCategoryService.adjustStoreCategorySort(source_id, target_id);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @PatchMapping("/category/{id}")
    public ResponseEntity modifyStoreCategory(@PathVariable long id, StoreCategoryRequest storeCategoryRequest) {
        storeCategoryService.modifyStoreCategories(id, storeCategoryRequest);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity modifyStoreCategory(@PathVariable long id) {
        storeCategoryService.deleteStoreCategory(id);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }
}
