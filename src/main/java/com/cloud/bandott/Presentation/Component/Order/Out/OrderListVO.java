package com.cloud.bandott.Presentation.Component.Order.Out;

import com.cloud.bandott.Presentation.Component.ListPageVO;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@SuperBuilder
public class OrderListVO extends ListPageVO {
    List<OrderVO> orderList;
}
