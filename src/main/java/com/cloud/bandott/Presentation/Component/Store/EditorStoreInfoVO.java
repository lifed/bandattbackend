package com.cloud.bandott.Presentation.Component.Store;

import com.cloud.bandott.Core.Store.Projection.StoreInfoProjection;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class EditorStoreInfoVO {
    Long id;
    String name;
    String description;
    String storeStatesEnum;
    String price;
    String imageMainUrl;
    String imageBannerUrl;
    Long imageMainId;
    Long imageBannerId;
    Long storeCategoryId;
    List<StoreCategoryMapVO> storeCategoryMaps;
    String storeStateJson;

    public EditorStoreInfoVO() {
    }

    public EditorStoreInfoVO(StoreInfoProjection projection) {
        this.id = projection.getId();
        this.storeCategoryId = projection.getStoreCategoryId();
        this.name = projection.getName();
        this.description = projection.getDescription();
        this.storeStatesEnum = projection.getStoreStatesEnum();
        this.price = projection.getPrice();
        this.imageMainUrl = projection.getImageMainUrl();
        this.imageBannerUrl = projection.getImageBannerUrl();
        this.imageMainId = projection.getImageMainId();
        this.imageBannerId = projection.getImageBannerId();
    }

}
