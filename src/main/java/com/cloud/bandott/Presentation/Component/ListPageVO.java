package com.cloud.bandott.Presentation.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ListPageVO {
    int Page;
    int Size;
    long totalElements;
    int totalPages;
}
