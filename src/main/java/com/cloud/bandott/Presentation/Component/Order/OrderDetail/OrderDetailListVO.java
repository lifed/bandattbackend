package com.cloud.bandott.Presentation.Component.Order.OrderDetail;

import com.cloud.bandott.Presentation.Component.ListPageVO;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@SuperBuilder
public class OrderDetailListVO extends ListPageVO {
    List<OrderDetailVO> detailVOList;
}
