package com.cloud.bandott.Presentation.Component.Store;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.cloud.bandott.Core.SharedKernel.StoragePath.Paths.StoreMenuPath;
import com.cloud.bandott.Core.SharedKernel.StoragePath.Paths.StorePath;
import com.cloud.bandott.Core.Store.Image.StoreImageBanner;
import com.cloud.bandott.Core.Store.Image.StoreImageBannerRepo;
import com.cloud.bandott.Core.Store.Image.StoreImageMain;
import com.cloud.bandott.Core.Store.Image.StoreImageMainRepo;
import com.cloud.bandott.Core.Store.Menu.Image.StoreMenuImages;
import com.cloud.bandott.Core.Store.Menu.Image.StoreMenuImagesRepo;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreInfoResponse;
import com.cloud.bandott.Utils.ImageResourceHttpRequestHandler;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@RequestMapping("/api/store")
@AllArgsConstructor
@Slf4j
public class StoreImagesController {
    private final ImageService imageService;
    private final StoreImageMainRepo imageMainRepo;
    private final StoreImageBannerRepo imageBannerRepo;
    private final StoreMenuImagesRepo menuImagesRepo;

    @GetMapping("/p/file/{imageType}/{sha256}")
    public void download(@PathVariable String sha256,
                         @PathVariable String imageType,
                         HttpServletRequest request,
                         HttpServletResponse response) {
        File file;
        switch (ImageType.valueOf(imageType)) {
            case main:
                file = imageService.getFile(sha256, imageMainRepo);
                break;
            case banner:
                file = imageService.getFile(sha256, imageBannerRepo);
                break;
            case menu:
                file = imageService.getFile(sha256, menuImagesRepo);
                break;
            default:
                file = null;
        }

        try {
            request.setAttribute(ImageResourceHttpRequestHandler.ATTRIBUTE_FILE, file);
            imageResourceHttpRequestHandler.handleRequest(request, response);
        } catch (Exception e) {
            log.error("無法讀取圖片，參數：sha256:{}, imageType:{}, e: {} ", sha256, imageType, e.toString());
            throw new UserInterfaceMessageException("Something wrong!", "現在無法讀取圖片");
        }
    }
    @Resource
    private ImageResourceHttpRequestHandler imageResourceHttpRequestHandler;

    @PreAuthorize("hasAnyRole('BACKEND_USER', 'WEB_ADMIN', 'ROOT')")
    @PostMapping(value = {"/file/{storeId}/{imageType}"
            , "/p/file/{storeId}/{imageType}/{menuId}"})
    public ResponseEntity<Void> addStoreImage(@PathVariable Long storeId,
                                              @PathVariable String imageType,
                                              @PathVariable(required = false) Long menuId,
                                              @RequestPart MultipartFile imageFile) {
        if (imageFile == null || imageFile.isEmpty()) {
            throw new UserInterfaceMessageException("FileNotFound", "找不到檔案，請重新操作");
        }
        HttpStatus httpStatus = HttpStatus.CREATED;
        switch (ImageType.valueOf(imageType)) {
            case main ->
                    imageService.addImage(storeId, new StorePath(storeId).getRootPath(), imageFile, imageMainRepo, StoreImageMain.class);
            case banner ->
                    imageService.addImage(storeId, new StorePath(storeId).getRootPath(), imageFile, imageBannerRepo, StoreImageBanner.class);
            case menu ->
                    imageService.addImage(menuId, new StoreMenuPath(storeId).getRootPath(), imageFile, menuImagesRepo, StoreMenuImages.class);
            default -> httpStatus = HttpStatus.OK;
        }

        return new ResponseEntity<>(httpStatus);
    }

    @PreAuthorize("hasAnyRole('BACKEND_USER', 'WEB_ADMIN', 'ROOT')")
    @DeleteMapping("/file/{moduleId}/{imageId}/{imageType}")
    public ResponseEntity<StoreInfoResponse> deleteStoreImage(@PathVariable Long moduleId,
                                                              @PathVariable Long imageId,
                                                              @PathVariable String imageType) {
        HttpStatus httpStatus = HttpStatus.OK;
        switch (ImageType.valueOf(imageType)) {
            case main -> imageService.deleteStoreImage(moduleId, imageId, imageMainRepo);
            case banner -> imageService.deleteStoreImage(moduleId, imageId, imageBannerRepo);
            case menu -> imageService.deleteStoreImage(moduleId, imageId, menuImagesRepo);
            default -> httpStatus = HttpStatus.ACCEPTED;
        }
        return new ResponseEntity<>(httpStatus);
    }

    public enum ImageType {
        main,
        banner,
        menu,
    }

}
