package com.cloud.bandott.Presentation.Component.Order;

import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.cloud.bandott.Core.OrderMenu.OrderCenterService;
import com.cloud.bandott.Presentation.Component.Order.In.OrderDto;
import com.cloud.bandott.Presentation.Component.Order.Out.OrderListVO;
import com.cloud.bandott.Utils.PermissionUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/api/order")
@AllArgsConstructor
@Slf4j
@PreAuthorize("hasAnyRole('FRONTEND_USER')")
public class OrderManagementController {
    private final OrderCenterService orderCenterService;
    private final MemberCenterService memberCenterService;
    private final PermissionUtils permissionUtils;

    @GetMapping("/published/{pages}")
    public ResponseEntity<OrderListVO> getMyOrder(
            @PathVariable(required = false) Optional<Integer> pages,
            Principal principal
    ) {
        int page = pages.isPresent() && pages.get() > 1 ? pages.get() : 1;
        OrderListVO orderListVO = orderCenterService.getPublishedOrders(principal.getName(), page);
        return new ResponseEntity<>(orderListVO, HttpStatus.OK);
    }

    @GetMapping("/joined/{pages}")
    public ResponseEntity<OrderListVO> getJoinedOrder(
            @PathVariable(required = false) Optional<Integer> pages,
            Principal principal) {
        int page = pages.isPresent() && pages.get() > 1 ? pages.get() : 1;
        return new ResponseEntity<>(orderCenterService.getJoinedOrders(principal.getName(), page), HttpStatus.OK);
    }

    @GetMapping("/all/{pages}")
    public ResponseEntity<OrderListVO> getAllOrder(@PathVariable(required = false) Optional<Integer> pages) {
        int page = pages.isPresent() && pages.get() > 1 ? pages.get() : 1;
        return new ResponseEntity<>(orderCenterService.getOrdersOut(page), HttpStatus.OK);
    }

    @GetMapping("/getOrder/{orderId}")
    public ResponseEntity<OrderDto> getOrderInfo(@PathVariable long orderId) {
        return new ResponseEntity<>(orderCenterService.getOrderInfo(orderId), HttpStatus.OK);
    }

    @PostMapping("/editorOrder")
    public ResponseEntity addOrder(Principal principal,
                                   @RequestBody OrderDto orderDto) {
        orderDto.setMemberCenterId(memberCenterService.getMemberInfoByEmail(principal.getName()).getId());
        orderCenterService.addOrder(orderDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PatchMapping("/editorOrder/{orderId}")
    public ResponseEntity editOrder(Principal principal,
                                    @PathVariable long orderId,
                                    @RequestBody OrderDto orderDto) {
        orderDto.setOrderId(orderId);
        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());
        orderDto.setMemberCenterId(memberCenter.getId());
        orderCenterService.editOrder(permissionUtils.isAdmin(memberCenter), orderDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/published/{orderId}")
    public ResponseEntity deleteOrder(Principal principal, @PathVariable long orderId) {
        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());
        orderCenterService.deleteOrder(permissionUtils.isAdmin(memberCenter), orderId, memberCenter);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
