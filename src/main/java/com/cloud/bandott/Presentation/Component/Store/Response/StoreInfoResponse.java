package com.cloud.bandott.Presentation.Component.Store.Response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StoreInfoResponse {
    String message = "完成請求";
}
