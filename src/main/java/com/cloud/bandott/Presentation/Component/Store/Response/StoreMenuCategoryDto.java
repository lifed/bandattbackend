package com.cloud.bandott.Presentation.Component.Store.Response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreMenuCategoryDto {
    private String typeName;
    private List<StoreMenuDto> storeMenus;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StoreMenuDto {
        private Long id;
        private String name;
        private String price;
        private String menuUrl;
        private String description;
        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDateTime createAt;
    }
}
