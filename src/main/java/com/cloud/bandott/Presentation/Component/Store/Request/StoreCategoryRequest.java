package com.cloud.bandott.Presentation.Component.Store.Request;

import com.cloud.bandott.Core.Store.Category.StoreCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the {@link StoreCategory} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreCategoryRequest {
    private Long id;
    private String name;
}
