package com.cloud.bandott.Presentation.Component.Member.Response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LoginMemberResponse {
    private Long Id;
    private String Name;
    private String PicURL;
    private String Token;
    private String Auth;
}
