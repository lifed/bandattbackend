package com.cloud.bandott.Presentation.Component.Store.Response;

import com.cloud.bandott.Core.Store.Category.StoreCategory;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * A DTO for the {@link StoreCategory} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StoreCategoryResponse {
    private Long id;
    private String name;
    private List<ModuleCategoryDto> moduleCategories;

    /**
     * A DTO for the {@link com.cloud.bandott.Core.SharedKernel.ModuleCategory.ModuleCategory} entity
     */
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class ModuleCategoryDto {
        private Long id;
        private String name;
        private List<ModuleCategoryDto> moduleCategories;
    }
}
