package com.cloud.bandott.Presentation.Component;

import com.cloud.bandott.Configuration.Security.CorsYml;
import com.cloud.bandott.Core.SharedKernel.RequestUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.cloud.bandott.BandottApplication.isDebug;

@RestController
@RequestMapping("/api/server/p")
@Slf4j
@AllArgsConstructor
public class SecurityController {
    private final PasswordEncoder passwordEncoder;
    private final RequestUtil requestUtil;
    private CorsYml corsYml;

    // ChatGPT建議
    // CsrfToken必須綁定到用戶的Session上，防止攻擊者偷取用戶的CsrfToken。
    // CsrfToken必須在每次使用後重新產生，防止攻擊者利用已知CsrfToken攻擊。
    // CsrfToken必須在每個表單中都存在，防止攻擊者利用不完整的表單進行攻擊。
    // 所有敏感操作都需要檢查CsrfToken，確保所有操作都有CsrfToken保護。
    @GetMapping("/csrf")
    public ResponseEntity<String> getCSRF(HttpServletRequest request, HttpServletResponse response, CsrfToken fakeCsrfToken) {
        String resultStr = "";
        boolean isOrigin = false;
        String remoteOrigin = requestUtil.getRemotePath(request);
        if (isDebug) {
            System.out.println("getCSRF Session: " + request.getSession().getId());
            log.info("remoteOrigin = {}", remoteOrigin);
        }
        for (String s : corsYml.getAllowedOrigin()) {
            if (remoteOrigin.contains(s)) {
                isOrigin = true;
                break;
            }
        }
        CsrfToken csrfToken = new HttpSessionCsrfTokenRepository().loadDeferredToken(request, response).get();
        if (isOrigin) {
            resultStr = "{\"parameterName\":\"" + csrfToken.getParameterName() + "\"" +
                    ",\"token\":\"" + csrfToken.getToken() + "\"" +
                    ",\"headerName\":\"" + csrfToken.getHeaderName() + "\"}";
        } else {
            resultStr = "{\"parameterName\":\"" + fakeCsrfToken.getParameterName() + "\"" +
                    ",\"token\":\"" + fakeCsrfToken.getToken() + "\"" +
                    ",\"headerName\":\"" + fakeCsrfToken.getHeaderName() + "\"}";
        }
        return new ResponseEntity<>(resultStr, HttpStatus.OK);
    }

    @GetMapping("/isAlive")
    public ResponseEntity<String> isServerOn() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/hash/{password}")
    public String hashPassword(@PathVariable String password) {
        System.out.println("password = " + password);
        return passwordEncoder.encode(password);
    }
}

