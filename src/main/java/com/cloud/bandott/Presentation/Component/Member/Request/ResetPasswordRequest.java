package com.cloud.bandott.Presentation.Component.Member.Request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

import static com.cloud.bandott.Presentation.Component.Member.Request.CustomRegexp.RegexpPasswordLowerAndDigit;

@Setter
@Getter
public class ResetPasswordRequest {
    @Email(message = "{mail.regexp}")
    private String email;
    @Pattern(message = "{password.regexp}", regexp = RegexpPasswordLowerAndDigit)
    private String password;
    @Pattern(message = "{password.regexp}", regexp = RegexpPasswordLowerAndDigit)
    private String repassword;
}
