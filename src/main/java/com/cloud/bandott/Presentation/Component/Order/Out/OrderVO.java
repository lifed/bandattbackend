package com.cloud.bandott.Presentation.Component.Order.Out;

import com.cloud.bandott.Core.OrderMenu.OrderStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * A DTO for the {@link com.cloud.bandott.Core.OrderMenu.OrderCenter} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderVO {
    private Long id;
    private String title;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private LocalDateTime createAt;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private LocalDateTime endRequestAt;
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss")
    private LocalDateTime takeMealAt;
    private OrderStatusEnum orderStatusEnum;
    private String orderStatus;
    private String storeName;
    private Long storeId;
    private String organizerName;
    private String organizerNickname;
}
