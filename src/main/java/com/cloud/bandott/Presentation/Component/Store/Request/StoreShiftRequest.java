package com.cloud.bandott.Presentation.Component.Store.Request;

import com.cloud.bandott.Core.Store.Shift.StoreShift;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the {@link StoreShift} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreShiftRequest {
    private Long storeInfoId;
    private int storeDaily;
    private int startHour;
    private int startMin;
    private int endHour;
    private int endMin;
}
