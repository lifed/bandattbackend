package com.cloud.bandott.Presentation.Component.Member;

import com.cloud.bandott.Presentation.Component.Member.Request.CustomRegexp;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class MemberInfoDto {
    @Email
    private String email;
    @Pattern(message = "{password.regexp}", regexp = CustomRegexp.RegexpPasswordLowerAndDigit)
    private String password;
    @Pattern(message = "{password.regexp}", regexp = CustomRegexp.RegexpPasswordLowerAndDigit)
    private String repassword;
    @Size(message = "{name.size}", min = CustomRegexp.name_min, max = CustomRegexp.name_max)
    private String name;
    @Size(message = "{name.size}", min = 0, max = CustomRegexp.name_max)
    private String nickname;
    // In
    private MultipartFile uploadImg;
    // Out
    private String userPictureURL;
}
