package com.cloud.bandott.Presentation.Component.Member.Request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class LoginMemberRequest {
    @NotEmpty(message = "{mail.notEmpty}")
    @Email(message = "{mail.regexp}")
    private String email;

    @NotEmpty(message = "{password.notEmpty}")
    @Pattern(message = "{password.regexp}", regexp = CustomRegexp.RegexpPasswordLowerAndDigit)
    private String password;
    private String captcha;
}
