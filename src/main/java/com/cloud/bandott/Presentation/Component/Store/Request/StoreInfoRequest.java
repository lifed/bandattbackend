package com.cloud.bandott.Presentation.Component.Store.Request;

import com.cloud.bandott.Core.Store.Enum.StoreStatesEnum;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

/**
 * A DTO for the {@link com.cloud.bandott.Core.Store.StoreInfo} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreInfoRequest {
    @NotNull
    private String storeName;
    private String description;
    private String price;
    private StoreStatesEnum storeStatesEnum;
    private MultipartFile mainImage;
    private MultipartFile bannerImage;
    private Long storeCategoryId;
}
