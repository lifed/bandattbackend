package com.cloud.bandott.Presentation.Component.Store.Response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoresResponse {
    List<StoreBasicInfoVO> stores = new ArrayList<>();
    int Page;
    int Size;
    long totalElements;
    int totalPages;
}
