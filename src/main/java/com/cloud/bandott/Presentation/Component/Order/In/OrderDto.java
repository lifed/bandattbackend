package com.cloud.bandott.Presentation.Component.Order.In;

import com.cloud.bandott.Core.OrderMenu.OrderCenter;
import com.cloud.bandott.Core.OrderMenu.OrderStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * A DTO for the {@link OrderCenter} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    @NotNull
    private String title;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy[-/]MM[-/]dd HH:mm:ss")
    private LocalDateTime endRequestAt;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy[-/]MM[-/]dd HH:mm:ss")
    private LocalDateTime takeMealAt;
    private OrderStatusEnum orderStatus;
    private String abandonReason;
    private Long storeId;
    private Long orderId;
    private Long memberCenterId;
}
