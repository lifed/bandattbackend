package com.cloud.bandott.Presentation.Component.Member.Request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Deprecated
public class RegisterMemberRequest {
    @Email(message = "{mail.regexp}")
    private String email;
    //    @Pattern(message = "{password.regexp}", regexp = CustomRegexp.RegexpPasswordLowerAndDigit)
    private String password;
    //    @Pattern(message = "{password.regexp}", regexp = CustomRegexp.RegexpPasswordLowerAndDigit)
    private String repassword;
    @Size(message = "{name.size}", min = CustomRegexp.name_min, max = CustomRegexp.name_max)
    private String name;
    @Size(message = "{name.size}", min = CustomRegexp.name_min, max = CustomRegexp.name_max)
    private String nickname;
    private MultipartFile userPicture;
}
