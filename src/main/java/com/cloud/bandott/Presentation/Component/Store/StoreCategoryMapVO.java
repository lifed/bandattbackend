package com.cloud.bandott.Presentation.Component.Store;

import com.cloud.bandott.Core.Store.Projection.StoreCategoryMapProjection;
import lombok.Data;

@Data
public class StoreCategoryMapVO {
    Long id;
    String name;

    public StoreCategoryMapVO(StoreCategoryMapProjection projection) {
        this.id = projection.getId();
        this.name = projection.getName();
    }
}
