package com.cloud.bandott.Presentation.Component.Store.Request;

import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A DTO for the {@link StoreMenuCategory} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreMenuCategoryRequest {
    private Long storeId;
    private String name;
}
