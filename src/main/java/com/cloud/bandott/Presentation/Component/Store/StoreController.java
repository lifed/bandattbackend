package com.cloud.bandott.Presentation.Component.Store;

import com.cloud.bandott.Core.SharedKernel.RequestUtil;
import com.cloud.bandott.Core.Store.Category.StoreCategoryService;
import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategoryService;
import com.cloud.bandott.Core.Store.Shift.StoreShiftService;
import com.cloud.bandott.Core.Store.StoreInfoService;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreMenuInfoVO;
import com.cloud.bandott.Presentation.Component.Store.Response.StoresResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/store/p")
@AllArgsConstructor
public class StoreController {
    private final StoreInfoService storeInfoService;
    private final RequestUtil requestUtil;
    private final StoreCategoryService storeCategoryService;
    private final StoreMenuCategoryService storeMenuCategoryService;
    private final StoreShiftService storeShiftService;

    @GetMapping({"/stores/{pages}"})
    @ResponseBody
    public ResponseEntity<StoresResponse> getShop(StoreReq storeReq,
                                                  HttpServletRequest request) {
        return new ResponseEntity<>(
                storeInfoService.getShops(storeReq, requestUtil.getPathFromConfig(false, "", request)),
                HttpStatus.OK
        );
    }

    @GetMapping("/storeInfo/{storeID}")
    public ResponseEntity<StoreMenuInfoVO> getShopInfoById(@PathVariable Long storeID,
                                                           HttpServletRequest request) {
        return new ResponseEntity<>(
                storeInfoService.getShopInfoById(storeID, requestUtil.getPathFromConfig(false, "", request)),
                HttpStatus.OK
        );
    }

    @GetMapping("/storeDetail/{storeID}")
    public ResponseEntity<StoreMenuInfoVO> getShopDetailById(@PathVariable Long storeID,
                                                             HttpServletRequest request) {
        return new ResponseEntity<>(
                storeInfoService.getShopDetailById(storeID, requestUtil.getPathFromConfig(false, "", request)),
                HttpStatus.OK
        );
    }

    @GetMapping("/suggestionList/{keyword}")
    public ResponseEntity<StoreSuggestion> getStoreSuggestion(@PathVariable String keyword) {
        return new ResponseEntity<>(
                storeInfoService.getStoreSuggestionByName(keyword),
                HttpStatus.OK
        );
    }

    @GetMapping("/storeCategories")
    public ResponseEntity<List<StoreCategoryMapVO>> getStoreCategories() {
        return new ResponseEntity<>(
                storeCategoryService.getStoreCategories(),
                HttpStatus.OK
        );
    }
}
