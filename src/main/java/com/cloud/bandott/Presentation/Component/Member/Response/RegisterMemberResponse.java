package com.cloud.bandott.Presentation.Component.Member.Response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegisterMemberResponse {
    // HttpStatus will be 201
    private boolean isCreatedUser = false;
    private boolean isCreatedUserPic = false;
}
