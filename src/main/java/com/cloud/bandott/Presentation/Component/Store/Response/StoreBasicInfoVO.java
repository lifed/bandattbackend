package com.cloud.bandott.Presentation.Component.Store.Response;

import com.cloud.bandott.Core.Store.Projection.StoreInfoProjection;

public record StoreBasicInfoVO(Long id,
                               String name,
                               String description,
                               Long storeCategoryId,
                               String storeStatesEnum,
                               String price,
                               String bannerUrl,
                               String mainUrl) {

    public static StoreBasicInfoVO fromProjection(StoreInfoProjection projection) {
        return new StoreBasicInfoVO(
                projection.getId(),
                projection.getName(),
                projection.getDescription(),
                projection.getStoreCategoryId(),
                projection.getStoreStatesEnum(),
                projection.getPrice(),
                projection.getImageBannerUrl(),
                projection.getImageMainUrl()
        );
    }
}
