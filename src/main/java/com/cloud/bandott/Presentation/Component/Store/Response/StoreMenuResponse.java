package com.cloud.bandott.Presentation.Component.Store.Response;

import com.cloud.bandott.Core.Store.Enum.StoreStatesEnum;
import com.cloud.bandott.Core.Store.Menu.StoreMenu;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * A DTO for the {@link com.cloud.bandott.Core.Store.StoreInfo} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreMenuResponse {
    @NotNull
    private String name;
    private String description;
    private StoreStatesEnum storeStatesEnum;
    private Set<StoreMenuDto> storeMenus;

    /**
     * A DTO for the {@link StoreMenu} entity
     */
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StoreMenuDto {
        private String name;
        private int price;
    }
}
