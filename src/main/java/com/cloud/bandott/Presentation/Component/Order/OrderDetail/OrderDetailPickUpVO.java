package com.cloud.bandott.Presentation.Component.Order.OrderDetail;

import com.cloud.bandott.Core.OrderMenu.Detail.Projections.OrderDetailPickUpProjection;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderDetailPickUpVO {
    private String name;
    private double price;
    private double quantity;
    private double total;
    private String typeName;

    public OrderDetailPickUpVO(OrderDetailPickUpProjection projection) {
        this.name = projection.getName();
        this.price = projection.getPrice();
        this.quantity = projection.getQuantity();
        this.total = projection.getTotal();
        this.typeName = projection.getTypeName();
    }
}