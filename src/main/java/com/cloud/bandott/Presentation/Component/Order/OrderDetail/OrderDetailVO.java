package com.cloud.bandott.Presentation.Component.Order.OrderDetail;

import com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailVO {
    private Long id;
    private String dishName;
    private Long dishPrice;
    private Integer dishQuantity;
    private String mealStatus;
    private MealStatusEnum mealStatusEnum;
    private String mealStatusReason;
    private String userName;
    private Long userId;

}
