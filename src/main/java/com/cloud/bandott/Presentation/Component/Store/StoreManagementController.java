package com.cloud.bandott.Presentation.Component.Store;

import com.cloud.bandott.Core.SharedKernel.RequestUtil;
import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategoryService;
import com.cloud.bandott.Core.Store.Shift.StoreShiftService;
import com.cloud.bandott.Core.Store.StoreInfoService;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreInfoRequest;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreMenuCategoryRequest;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreShiftRequest;
import com.cloud.bandott.Presentation.Component.Store.Response.StoreInfoResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/store")
@AllArgsConstructor
@PreAuthorize("hasAnyRole('BACKEND_USER', 'WEB_ADMIN', 'ROOT')")
public class StoreManagementController {
    private final StoreInfoService storeInfoService;
    private final StoreMenuCategoryService storeMenuCategoryService;
    private final StoreShiftService storeShiftService;
    private final RequestUtil requestUtil;

    @PostMapping
    public ResponseEntity<StoreInfoResponse> addStore(StoreInfoRequest storeInfoRequest) {
        return new ResponseEntity<>(
                storeInfoService.addStore(storeInfoRequest),
                HttpStatus.CREATED
        );
    }

    // @PutMapping("/{storeId}")
    // public ResponseEntity<StoreInfoResponse> renewStore(@PathVariable long storeId, StoreInfoRequest storeInfoRequest) {
    //     return new ResponseEntity<>(
    //             storeInfoService.renewStore(storeId, storeInfoRequest),
    //             HttpStatus.CREATED
    //     );
    // }

    @PatchMapping("/{storeId}")
    public ResponseEntity<StoreInfoResponse> modifyStoreMenu(@PathVariable long storeId, StoreInfoRequest storeInfoRequest) {
        return new ResponseEntity<>(
                storeInfoService.modifyStore(storeId, storeInfoRequest),
                HttpStatus.CREATED
        );
    }

    @DeleteMapping("/{storeId}")
    public ResponseEntity<StoreInfoResponse> deleteStore(@PathVariable long storeId) {
        return new ResponseEntity<>(
                storeInfoService.deleteStore(storeId),
                HttpStatus.OK
        );
    }

    @PostMapping("/{storeId}/shift")
    public ResponseEntity<Void> addStoreShift(@PathVariable long storeId, StoreShiftRequest shiftRequest) {
        shiftRequest.setStoreInfoId(storeId);
        storeShiftService.addStoreShift(shiftRequest);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @PostMapping("/{storeId}/shifts")
    public ResponseEntity<Void> addStoreShifts(@PathVariable long storeId, StoreShiftRequest[] shiftRequest) {
        storeShiftService.addStoreShifts(storeId, shiftRequest);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @PatchMapping("/{storeId}/shift/{shift_id}")
    public ResponseEntity<Void> modifyStoreShift(@PathVariable long storeId, @PathVariable long shift_id, StoreShiftRequest shiftRequest) {
        storeShiftService.modifyStoreShift(storeId, shift_id, shiftRequest);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @DeleteMapping("/{storeId}/shift/{shift_id}")
    public ResponseEntity<Void> deleteStoreShift(@PathVariable long storeId, @PathVariable long shift_id) {
        storeShiftService.deleteStoreShift(storeId, shift_id);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @PostMapping("/{storeId}/menucategory")
    public ResponseEntity<Void> addStoreCategory(@PathVariable long storeId, StoreMenuCategoryRequest storeMenuCategoryRequest) {
        storeMenuCategoryService.addStoreMenuCategory(storeId, storeMenuCategoryRequest);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

    @PatchMapping("/{storeId}/menucategory/{menu_category_id}")
    public ResponseEntity<Void> modifyStoreCategory(@PathVariable long storeId, @PathVariable long menu_category_id, StoreMenuCategoryRequest storeMenuCategoryRequest) {
        storeMenuCategoryService.modifyStoreMenuCategory(storeId, menu_category_id, storeMenuCategoryRequest);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

    @DeleteMapping("/{storeId}/menucategory/{menu_category_id}")
    public ResponseEntity<Void> deleteStoreCategory(@PathVariable long storeId, @PathVariable long menu_category_id) {
        storeMenuCategoryService.deleteStoreMenuCategory(storeId, menu_category_id);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @GetMapping({"/editor/{storeId}", "/editor"})
    public ResponseEntity<EditorStoreInfoVO> getStoreInfo(@PathVariable(required = false) Long storeId,
                                                          HttpServletRequest request) {
        return new ResponseEntity<>(
                storeInfoService.getStoreInfoEditorById(storeId, requestUtil.getPathFromConfig(false, "", request)),
                HttpStatus.OK
        );
    }
}
