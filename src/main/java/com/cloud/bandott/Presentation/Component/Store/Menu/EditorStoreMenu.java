package com.cloud.bandott.Presentation.Component.Store.Menu;

import java.util.List;

public record EditorStoreMenu(
        String storeName,
        String name,
        String description,
        Float price,
        String menuPicUrl,
        Long menuPicId,
        Long menuCategoryId,
        List<MenuCategory> menuCategoryList
) {
}
