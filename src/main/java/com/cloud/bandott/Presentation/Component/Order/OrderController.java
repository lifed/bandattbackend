package com.cloud.bandott.Presentation.Component.Order;

import com.cloud.bandott.Core.OrderMenu.OrderCenterService;
import com.cloud.bandott.Presentation.Component.Order.Out.OrderListVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/order/p")
@AllArgsConstructor
@Slf4j
public class OrderController {
    private final OrderCenterService orderCenterService;

    @GetMapping("/tables/{page}")
    public ResponseEntity<OrderListVO> getTodayTables(
            @PathVariable(required = false) Optional<Integer> pages
    ) {
        int page = pages.isPresent() && pages.get() > 1 ? pages.get() : 1;
        return new ResponseEntity<>(orderCenterService.getTodayTables(page), HttpStatus.OK);
    }
}
