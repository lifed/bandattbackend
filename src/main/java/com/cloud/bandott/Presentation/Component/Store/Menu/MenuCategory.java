package com.cloud.bandott.Presentation.Component.Store.Menu;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record MenuCategory(
        Long id,
        @NotNull
        @NotEmpty
        String name
) {
}
