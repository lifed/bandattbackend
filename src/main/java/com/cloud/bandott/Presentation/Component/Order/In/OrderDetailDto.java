package com.cloud.bandott.Presentation.Component.Order.In;

import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * A DTO for the {@link com.cloud.bandott.Core.OrderMenu.Detail.OrderDetail} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailDto {
    @Size(min = 1, message = "購物車沒有資料")
    private List<OrderDetailContentDto> items;
    // 以下Controller後設定
    private Long orderId;
    private MemberCenter memberCenter;
    private MealStatusEnum mealStatus;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderDetailContentDto {
        @NotNull
        private Long id;
        @NotNull
        private Integer quantity;

    }
}
