package com.cloud.bandott.Presentation.Component.Member.Request;

import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FindPasswordRequest {
    @Email(message = "{mail.regexp}")
    private String email;
}
