package com.cloud.bandott.Presentation.Component.Member;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageEnum;
import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.Images.MemberImagesRepository;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.cloud.bandott.Core.SharedKernel.RequestUtil;
import com.cloud.bandott.Core.SharedKernel.Security.JwtTokenBlacklist;
import com.cloud.bandott.Presentation.Component.Member.Request.CustomRegexp;
import com.cloud.bandott.Presentation.Component.Member.Response.LoginMemberResponse;
import com.cloud.bandott.Utils.ImageResourceHttpRequestHandler;
import io.jsonwebtoken.JwtException;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static com.cloud.bandott.BandottApplication.isDebug;

@RestController
@RequestMapping("/api/member/p")
@RequiredArgsConstructor
@Slf4j
public class MemberController {
    private final MemberCenterService memberCenterService;
    private final ImageService imageService;
    private final MemberImagesRepository memberImagesRepository;
    private final RequestUtil requestUtil;
    private final JwtTokenBlacklist jwtTokenBlacklist;

    @Value("${security.mode}")
    private String securityMode;

    @Resource
    private ImageResourceHttpRequestHandler imageResourceHttpRequestHandler;

    public CompletableFuture<ResponseEntity<Void>> signUpUser(MemberInfoDto memberInfoDto) {
        if (isDebug) {
            System.out.println("memberInfoDto = " + memberInfoDto);
        }
        return memberCenterService.register(memberInfoDto).thenApply(result -> new ResponseEntity<>(HttpStatus.CREATED));
    }

    @PostMapping(value = "/signUp", consumes = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<Void>> signUpJson(
            @Valid @RequestBody MemberInfoDto memberInfoDto
    ) {
        return signUpUser(memberInfoDto);

    }

    @PostMapping(value = "/signUp", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public CompletableFuture<ResponseEntity<Void>> signUpFormData(
            @Valid MemberInfoDto memberInfoDto
    ) {
        return signUpUser(memberInfoDto);
    }

    @GetMapping("/signIn")
    @PostMapping("/signIn")
    @ResponseBody
    public ResponseEntity<Map<String, String>> signIn(HttpServletRequest req, Authentication authentication) {
        Map<String, String> jsonMap = new HashMap<>();
        Map<String, String[]> reqMap = req.getParameterMap();
        jsonMap.put("狀態", "請用正常方式登入");
        HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
        if (reqMap.get("error") != null) {
            throw new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberAccountAndPasswordNotMatchException);
        }
        if (reqMap.get("logout") != null) {
            httpStatus = HttpStatus.OK;
            jsonMap.put("狀態", "已登出");
        }
        if (reqMap.get("login") != null && authentication.isAuthenticated()) {
            httpStatus = HttpStatus.OK;
            jsonMap.put("狀態", "已登入");
        }
        return new ResponseEntity<>(
                jsonMap
                , httpStatus
        );
    }

    @GetMapping("/file/{sha256}")
    public void download(
            @PathVariable String sha256,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        File file = imageService.getFile(sha256, memberImagesRepository);
        try {
            request.setAttribute(ImageResourceHttpRequestHandler.ATTRIBUTE_FILE, file);
            imageResourceHttpRequestHandler.handleRequest(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/sendPasswordResetEmail")
    public ResponseEntity<String> sendPasswordResetEmail(@RequestParam @Email String email
            , HttpServletRequest request) {
        if (isDebug) {
            log.info("email: {}", email);
        }
        memberCenterService.setEmailCode(email, requestUtil.getPathFromConfig(true, "/confirmedResetPassword", request));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/verifyPasswordResetCode")
    public ResponseEntity<String> verifyPasswordResetCode(@RequestParam @ResetPasswordHashCode String HashCode) {
        if (isDebug) {
            log.info("HashCode:{}", HashCode);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/updatePasswordWithCode")
    public ResponseEntity<String> updatePasswordWithCode(
            @RequestParam @ResetPasswordHashCode String HashCode
            , @RequestParam String Password
            , @RequestParam String rePassword
    ) {
        if (isDebug) {
            // System.out.println("Password = " + Password);
            // System.out.println("rePassword = " + rePassword);
            System.out.println("HashCode = " + HashCode);
        }
        if (!Password.trim().equals(rePassword.trim())) {
            throw new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberPasswordAndRepasswordNotMatchException);
        }
        memberCenterService.resetPassword(HashCode, Password);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/jwtSignIn")
    @ResponseBody
    public ResponseEntity<LoginMemberResponse> jwtSignIn(
            HttpServletRequest request,
            @Email String email,
            @Pattern(message = "{password.regexp}", regexp = CustomRegexp.RegexpPasswordLowerAndDigit) String password) {
        if ("jwt".equals(securityMode.toLowerCase())) {
            return new ResponseEntity<>(memberCenterService.jwtSignIn(
                    email,
                    password,
                    requestUtil.getPathFromConfig(false, "", request))
                    , HttpStatus.OK);
        } else {
            return new ResponseEntity<>(LoginMemberResponse.builder().build(),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/jwtSignOut")
    @ResponseBody
    public ResponseEntity<Void> jwtSignOut(HttpServletRequest request) {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (!(header == null || !header.startsWith("Bearer "))) {
            String token = header.substring(7);
            try {
                jwtTokenBlacklist.add(token);
            } catch (JwtException e) {
                // token出現問題不處理
                log.error("jwtSignOut error: {}", e.toString());
            }
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
