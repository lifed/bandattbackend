package com.cloud.bandott.Presentation.Component.Store;

import java.util.List;

public record StoreSuggestion(
        List<String> suggestionList
) {
}
