package com.cloud.bandott.Presentation.Component.Member.Request;

public class CustomRegexp {
    public static final String RegexpEmail = "";
    // https://openhome.cc/zh-tw/regex/syntax/group/
    // 至少有一個數字
    // 至少有一個小寫英文字母
    // 字串長度在 8 ~ 30 個字母之間
    public static final String RegexpPasswordLowerAndDigit = "^(?=.*?[a-z])(?=.*?[0-9]).{8,30}$";
    // 至少有一個大寫
    public static final String RegexpPasswordUpperAndLowerAndDigit = "^(?=.*?[a-z])(?=.*?[0-9])(?<![A-Z]).{8,30}$";
    // 至少有一個特殊符號
    public static final String RegexpPasswordUpperAndLowerAndDigitAndSpecialCharacters = "^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[A-Z])(?=.*?[#?!@$ %^&*-]).{8,30}$";

    public static final int name_min = 2;
    public static final int name_max = 30;

}
