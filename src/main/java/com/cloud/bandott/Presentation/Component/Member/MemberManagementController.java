package com.cloud.bandott.Presentation.Component.Member;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageEnum;
import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.Member.MemberCenterMapper;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageCenterRepo;
import com.cloud.bandott.Core.SharedKernel.ImageCenter.ImageService;
import com.cloud.bandott.Core.SharedKernel.RequestUtil;
import com.cloud.bandott.Presentation.Component.Member.Response.LoginMemberResponse;
import com.cloud.bandott.Presentation.Component.Member.Response.RegisterMemberResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;

import static com.cloud.bandott.BandottApplication.isDebug;

@RestController
@RequestMapping("/api/member")
@AllArgsConstructor
@PreAuthorize("hasAnyRole('FRONTEND_USER')")
@Slf4j
public class MemberManagementController {
    private final ImageCenterRepo memberImagesRepository;
    private final MemberCenterService memberCenterService;
    private final MemberCenterMapper memberCenterMapper;
    private final ImageService imageService;
    private final RequestUtil requestUtil;

    @GetMapping(value = "/getUserInfo")
    @ResponseBody
    public ResponseEntity<LoginMemberResponse> getUserInfo(Principal principal
            , Authentication authentication
            , HttpServletRequest request
    ) {
        if (isDebug) {
            log.info("principal.getName = {}", principal.getName());
            log.info("authentication.getAuthorities = {}", authentication.getAuthorities());
        }
        LoginMemberResponse loginMemberResponse = memberCenterService.loginWithPrincipal(principal.getName(),
                requestUtil.getPathFromConfig(false, "", request));
        return new ResponseEntity<>(
                loginMemberResponse
                , HttpStatus.OK
        );
    }

    @GetMapping(value = "/getUserEditableInfo")
    @ResponseBody
    public ResponseEntity<MemberInfoDto> getUserEditableInfo(Principal principal
            , Authentication authentication
            , HttpServletRequest request) {
        if (isDebug) {
            log.info("authentication.isAuthenticated() => {}", authentication.isAuthenticated());
        }
        MemberCenter memberInfo = memberCenterService.getMemberInfoByEmail(principal.getName());
        MemberInfoDto memberInfoDto = memberCenterMapper.memberCenterToMemberInfoDto(memberInfo);
        memberInfoDto.setUserPictureURL(imageService.getPicUrl(memberInfo.getId(),
                requestUtil.getPathFromConfig(false, "", request) + "/api/member/p/file/",
                memberImagesRepository));
        memberInfoDto.setPassword(null);
        if (isDebug) {
            log.info("dto content => {}", memberInfoDto);
        }
        return new ResponseEntity<>(
                memberInfoDto
                , HttpStatus.OK
        );
    }

    @PostMapping(value = "/updateUserEditableInfo")
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<RegisterMemberResponse> updateUserEditableInfo(
            MemberInfoDto memberInfoDto,
            Principal principal
    ) {
        HttpStatus httpStatus = HttpStatus.CREATED;
        if (!memberInfoDto.getPassword().equals(memberInfoDto.getRepassword())) {
            throw new UserInterfaceMessageException(UserInterfaceMessageEnum.MemberPasswordAndRepasswordNotMatchException);
        }
        memberInfoDto.setEmail(principal.getName());
        try {
            memberCenterService.updateMemberInfo(memberInfoDto);
        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
            if (isDebug) {
                log.error("error: {}, time:{}", e, LocalDateTime.now());
            }
        }
        return new ResponseEntity<>(httpStatus);
    }
}
