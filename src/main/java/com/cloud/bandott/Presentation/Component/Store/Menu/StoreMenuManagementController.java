package com.cloud.bandott.Presentation.Component.Store.Menu;

import com.cloud.bandott.Core.SharedKernel.RequestUtil;
import com.cloud.bandott.Core.Store.Menu.Category.StoreMenuCategoryService;
import com.cloud.bandott.Core.Store.Menu.StoreMenuService;
import com.cloud.bandott.Presentation.Component.Store.Request.StoreMenuRequest;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/store")
@AllArgsConstructor
@PreAuthorize("hasAnyRole('BACKEND_USER', 'WEB_ADMIN', 'ROOT')")
public class StoreMenuManagementController {
    private final StoreMenuService storeMenuService;
    private final StoreMenuCategoryService storeMenuCategoryService;
    private final RequestUtil requestUtil;

    @PostMapping("/{storeId}/menu")
    public ResponseEntity<Void> addStoreMenu(StoreMenuRequest storeMenuRequest) {
        storeMenuService.addMenu(storeMenuRequest);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

    @PatchMapping("/{storeId}/menu/{menuId}")
    public ResponseEntity<Void> modifyStoreMenu(StoreMenuRequest MenuRequest) {
        storeMenuService.modifyMenu(MenuRequest);
        return new ResponseEntity<>(
                HttpStatus.CREATED
        );
    }

    @DeleteMapping("/{storeId}/menu/{storeMenuId}")
    public ResponseEntity<Void> deleteStoreMenu(@PathVariable long storeId, @PathVariable long storeMenuId) {
        storeMenuService.deleteMenu(storeId, storeMenuId);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @GetMapping("/{storeId}/menuCategories")
    public ResponseEntity<List<MenuCategory>> getStoreCategories(@PathVariable Long storeId) {
        return new ResponseEntity<>(
                storeMenuCategoryService.getMenuCategoriesByStoreId(storeId),
                HttpStatus.OK
        );
    }

    @PostMapping("/{storeId}/menuCategories")
    public ResponseEntity<Void> modifyMenuCategories(@PathVariable Long storeId,
                                                     @RequestBody List<MenuCategory> menuCategories) {
        storeMenuCategoryService.modifyMenuCategoriesByStoreId(storeId, menuCategories);
        return new ResponseEntity<>(
                HttpStatus.OK
        );
    }

    @GetMapping({"/{storeId}/editor", "/{storeId}/editor/{menuId}"})
    public ResponseEntity<EditorStoreMenu> getMenu(@PathVariable Long storeId,
                                                   @PathVariable(required = false) Long menuId,
                                                   HttpServletRequest request) {
        return new ResponseEntity<>(
                storeMenuService.getMenuByStoreIdAndMenuId(storeId, menuId, requestUtil.getPathFromConfig(false, "", request)),
                HttpStatus.OK
        );
    }
}
