package com.cloud.bandott.Presentation.Component.Order.OrderDetail;

import com.cloud.bandott.Core.Filters.ControllerAdvice.UserInterfaceMessageException;
import com.cloud.bandott.Core.Member.MemberCenter;
import com.cloud.bandott.Core.Member.MemberCenterService;
import com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum;
import com.cloud.bandott.Core.OrderMenu.Detail.OrderDetailService;
import com.cloud.bandott.Core.OrderMenu.OrderCenterService;
import com.cloud.bandott.Presentation.Component.Order.In.OrderDetailDto;
import com.cloud.bandott.Utils.PermissionUtils;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/api/order")
@AllArgsConstructor
@PreAuthorize("hasAnyRole('FRONTEND_USER')")
public class OrderDetailController {
    private final OrderDetailService orderDetailService;
    private final OrderCenterService orderCenterService;
    private final MemberCenterService memberCenterService;
    private final PermissionUtils permissionUtils;

    public boolean isOrderOwner(Principal principal, Long orderId) {
        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());
        return Objects.equals(orderCenterService.getOrderCenter(orderId).getMemberCenter().getId(), memberCenter.getId());
    }

    @GetMapping("/{orderId}/PickupDetail")
    public ResponseEntity<List<OrderDetailPickUpVO>> getOrderPickupDetail(Principal principal, @PathVariable Long orderId) {
        if (!isOrderOwner(principal, orderId)) {
            throw new UserInterfaceMessageException("bad request", "權限不足");
        }
        return new ResponseEntity<>(
                orderDetailService.getOrderDetailPickup(orderId)
                , HttpStatus.OK);
    }

    @GetMapping("/{orderId}/PublishedDetail")
    public ResponseEntity<OrderDetailListVO> getOrderPublishedDetail(Principal principal, @PathVariable Long orderId) {
        if (!isOrderOwner(principal, orderId)) {
            throw new UserInterfaceMessageException("bad request", "權限不足");
        }
        return new ResponseEntity<>(
                orderDetailService.getOrderPublished(orderId)
                , HttpStatus.OK);
    }

    @GetMapping("/{orderId}/JoinedDetail")
    public ResponseEntity<OrderDetailListVO> getOrderJoinedDetail(Principal principal, @PathVariable Long orderId) {
        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());

        if (!(isOrderOwner(principal, orderId)
                || orderDetailService.getOrderDetailByMemberId(orderId, memberCenter.getId()).isPresent())) {
            throw new UserInterfaceMessageException("bad request", "權限不足");
        }
        return new ResponseEntity<>(
                orderDetailService.getOrderJoined(orderId, memberCenterService.getMemberInfoByEmail(principal.getName()).getId())
                , HttpStatus.OK);
    }

    @PostMapping("/{orderId}/detail")
    public ResponseEntity addOrder(Principal principal, @PathVariable Long orderId, @Valid @RequestBody OrderDetailDto orderDetailDto) {
        orderDetailDto.setMemberCenter(memberCenterService.getMemberInfoByEmail(principal.getName()));
        orderDetailDto.setOrderId(orderId);
        orderDetailService.addOrderDetail(orderDetailDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/{orderId}/detail/{orderDetailId}")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity deleteOrder(Principal principal, @PathVariable long orderId, @PathVariable long orderDetailId) {
        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());
        orderDetailService.deleteOrderDetail(orderId, orderDetailId, memberCenter, permissionUtils.isAdmin(memberCenter));
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("mealStatus", MealStatusEnum.CancelledBySelf.value());
        return new ResponseEntity(responseMap, HttpStatus.OK);
    }

    @PatchMapping("/{orderId}/detail/{orderDetailId}")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity modifyDetail(Principal principal,
                                       @PathVariable Long orderId,
                                       @PathVariable Long orderDetailId,
                                       @RequestParam Integer dishQuantity,
                                       @RequestParam(required = false) MealStatusEnum mealStatusEnum) {
        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());
        orderDetailService.modifyDetail(orderId, orderDetailId, dishQuantity, mealStatusEnum, memberCenter, permissionUtils.isAdmin(memberCenter));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/{orderId}/detail/batch/Paid/{userId}")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity modifyDetailMealStatusByUser(Principal principal,
                                                       @PathVariable Long orderId,
                                                       @PathVariable Long userId) {

        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());
        orderDetailService.modifyDetailMealStatusByUser(orderId, userId, memberCenter, permissionUtils.isAdmin(memberCenter));

        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/{orderId}/detail/batch/Reserved")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity modifyDetailMealStatus(Principal principal,
                                                 @PathVariable Long orderId) {
        MemberCenter memberCenter = memberCenterService.getMemberInfoByEmail(principal.getName());
        orderDetailService.modifyDetailMealStatus(orderId, memberCenter, permissionUtils.isAdmin(memberCenter));
        return new ResponseEntity(HttpStatus.OK);
    }
}
