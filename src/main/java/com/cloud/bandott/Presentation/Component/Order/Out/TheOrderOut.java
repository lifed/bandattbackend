package com.cloud.bandott.Presentation.Component.Order.Out;

import com.cloud.bandott.Core.OrderMenu.Detail.MealStatusEnum;
import com.cloud.bandott.Core.OrderMenu.OrderStatusEnum;
import com.cloud.bandott.Core.Store.Enum.StoreStatesEnum;
import com.cloud.bandott.Presentation.Component.Member.Request.CustomRegexp;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * A DTO for the {@link com.cloud.bandott.Core.OrderMenu.OrderCenter} entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TheOrderOut {
    private LocalDateTime createAt;
    private LocalDateTime updateAt;
    @NotNull
    private String title;
    @NotNull
    private LocalDateTime EndRequestAt;
    private LocalDateTime TakeMealAt;
    private OrderStatusEnum orderStatusEnum;
    private String AbandonReason;
    @NotNull
    @Size(message = "name.size", min = CustomRegexp.name_min, max = CustomRegexp.name_max)
    private String memberCenterNickname;
    private Long storeInfoId;
    @NotNull
    private String storeInfoName;
    private StoreStatesEnum storeInfoStoreStatesEnum;
    private List<OrderDetailDto> orderDetail;

    /**
     * A DTO for the {@link com.cloud.bandott.Core.OrderMenu.Detail.OrderDetail} entity
     */
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderDetailDto {
        private Long id;
        private LocalDateTime createAt;
        private LocalDateTime updateAt;
        @NotNull
        private String DishName;
        @NotNull
        private Float DishPrice;
        @NotNull
        private int DishCount;
        @NotNull
        private MealStatusEnum mealStatusEnum;
        @NotNull
        @Size(message = "name.size", min = CustomRegexp.name_min, max = CustomRegexp.name_max)
        private String memberCenterNickname;
    }
}
