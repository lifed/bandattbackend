package com.cloud.bandott.Configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class DomainProperties {
    @Value("${domain.frontend.contextPath}")
    String frontendContextPath;
    @Value("${domain.frontend.url}")
    String frontendURL;
    @Value("${domain.backend.contextPath}")
    String backendContextPath;
    @Value("${domain.backend.url}")
    String backendURL;

    @Value("${domain.localhost.url}")
    String localhostURL;
}
