package com.cloud.bandott.Configuration.Security;

import com.cloud.bandott.Configuration.ConfigurationUtils;
import com.cloud.bandott.Configuration.YamlPropertySourceFactory;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app.session")
@PropertySource(value = {"file:./config/yml/authentication.yml", "classpath:config/yml/authentication.yml"},
        factory = YamlPropertySourceFactory.class,
        ignoreResourceNotFound = true)
@Setter
@Getter
public class SessionAuthenticationConfig {
    boolean defaultCSRF;

    @PostConstruct
    public void checkConfigurationLoaded() {
        ConfigurationUtils.checkConfigSource("classpath:config/yml/authentication.yml", "file:./config/yml/authentication.yml");
    }
}
