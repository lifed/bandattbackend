package com.cloud.bandott.Configuration.Security;

import com.cloud.bandott.Configuration.ConfigurationUtils;
import com.cloud.bandott.Configuration.YamlPropertySourceFactory;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "cors")
@PropertySource(value = {"file:./config/yml/cors.yml", "classpath:config/yml/cors.yml"},
        factory = YamlPropertySourceFactory.class,
        ignoreResourceNotFound = true)
@Setter
@Getter
public class CorsYml {
    private List<String> AllowedOrigin;
    private List<String> AllowedMethod;
    private List<String> PathPattern;
    private boolean AllowCredentials;
    private List<String> AllowedHeaders;
    private List<String> ExposedHeaders;
    private Long MaxAge;

    @PostConstruct
    public void checkConfigurationLoaded() {
        ConfigurationUtils.checkConfigSource("classpath:config/yml/cors.yml", "file:./config/yml/cors.yml");
    }

}
