package com.cloud.bandott.Configuration.Security;

import com.cloud.bandott.Core.Member.EnumRole;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class WebSecurityConfig {
    private final SecurityConfigStrategy securityConfigStrategy;
    private final CorsYml corsYml;

    // https://docs.spring.io/spring-security/reference/features/authentication/password-storage.html
    final String[] WHITE_LIST_CONTROLLER = {
            "/api/order/p/**",
            "/api/member/p/**",
            "/api/store/p/**",
            "/api/server/p/**",
            "/rest/**"
    };
    final String[] WHITE_LIST_RESOURCES = {
            "/css/**"
            , "/js/**"
            , "/img/**"
            , "/lib/**"
            , "/favicon.ico"
    };
    final String[] WHITE_LIST_SWAGGER = {
            "/swagger-ui/**"
            , "/v3/api-docs/**"
    };

    // 預設編碼為BCrypt
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CorsConfigurationSource corsConfiguration() {
        CorsConfiguration corsConfig = new CorsConfiguration();
        corsConfig.applyPermitDefaultValues();
        corsConfig.setAllowedHeaders(corsYml.getAllowedHeaders().size() == 0 ? List.of(CorsConfiguration.ALL) : corsYml.getAllowedHeaders());
        corsConfig.setAllowedOrigins(corsYml.getAllowedOrigin());
        corsConfig.setAllowedMethods(corsYml.getAllowedMethod());
        corsConfig.setAllowCredentials(corsYml.isAllowCredentials());
        corsConfig.setMaxAge(corsYml.getMaxAge());

        corsConfig.setExposedHeaders(corsYml.getExposedHeaders());

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        corsYml.getPathPattern().forEach(pattern -> source.registerCorsConfiguration(pattern, corsConfig));
        return source;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .cors((cors) -> {
                    cors.configurationSource(corsConfiguration());
                })
                .authorizeHttpRequests((authorize) -> authorize
                        .requestMatchers(WHITE_LIST_CONTROLLER).permitAll()
                        .requestMatchers(WHITE_LIST_RESOURCES).permitAll()
                        .requestMatchers(WHITE_LIST_SWAGGER).hasRole(EnumRole.BACKEND_USER.name())
                        .anyRequest().authenticated()
                )
                .headers((header) -> {
                    header.frameOptions().disable();
                })
                .httpBasic().disable()
        ;
        // default is session
        securityConfigStrategy.configure(http);

        return http.build();
    }

}
