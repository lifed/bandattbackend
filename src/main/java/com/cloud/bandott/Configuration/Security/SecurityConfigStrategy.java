package com.cloud.bandott.Configuration.Security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

public interface SecurityConfigStrategy {
    void configure(HttpSecurity http) throws Exception;
}
