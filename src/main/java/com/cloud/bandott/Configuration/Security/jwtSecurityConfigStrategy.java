package com.cloud.bandott.Configuration.Security;

import com.cloud.bandott.Core.Filters.Security.JwtAuthorizationFilter;
import com.cloud.bandott.Core.SharedKernel.Security.JwtTokenBlacklist;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.cloud.bandott.BandottApplication.isDebug;

@Configuration
@ConditionalOnProperty(prefix = "security", name = "mode", havingValue = "jwt")
@AllArgsConstructor
public class jwtSecurityConfigStrategy implements SecurityConfigStrategy {
    private final JwtTokenBlacklist jwtTokenBlacklist;
    private final JwtAuthorizationFilter jwtAuthorizationFilter;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        if (isDebug) {
            System.out.println("AuthenticationMode = jwt");
        }
        http.formLogin(AbstractHttpConfigurer::disable)
                .logout(AbstractHttpConfigurer::disable)
                .csrf(AbstractHttpConfigurer::disable)
                .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Scheduled(fixedRate = 15000)
    public void printExistJwtToken() {
        jwtTokenBlacklist.printAllKey();
    }
}
