package com.cloud.bandott.Configuration.Security;

import com.cloud.bandott.Core.Filters.Security.CSRFTokenInHeaderFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

import static com.cloud.bandott.BandottApplication.isDebug;

@Configuration
@ConditionalOnProperty(prefix = "security", name = "mode", havingValue = "session", matchIfMissing = true)
@EnableJdbcHttpSession
@RequiredArgsConstructor
public class SessionSecurityConfigStrategy implements SecurityConfigStrategy {
    private final SessionAuthenticationConfig sessionAuthentication;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        if (isDebug) {
            System.out.println("AuthenticationMode = session");
        }
        if (sessionAuthentication.isDefaultCSRF()) {
            // 適用場景, 同網域
            http.csrf((csrf) -> {
                csrf.csrfTokenRepository(new HttpSessionCsrfTokenRepository());
            });
        } else {
            // 適用場景, 不同網域
            // 將面臨CSRF風險，透過額外補強降低一定風險，如：CSRF Token in Header, 限制請求Token網域。
            // 無法防止人為攻擊
            // 會被瀏覽器阻擋，如：Chrome, 設定-> 隱私權和安全性 -> 封鎖第三方Cookie
            http.csrf(AbstractHttpConfigurer::disable)
                    .addFilterBefore(new CSRFTokenInHeaderFilter(new HttpSessionCsrfTokenRepository()), CsrfFilter.class);
            // http.csrf().disable();
        }
        http.sessionManagement((session) -> session.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED));

        // TODO: 與Cookie機制共用
        http.formLogin(formLoginConfig -> formLoginConfig
                        .loginPage("/api/member/p/signIn")
                        .loginProcessingUrl("/api/member/p/signIn/processing")
                        .defaultSuccessUrl("/api/member/getUserInfo")
                        // .failureUrl("/api/member/p/signIn?error")
                        // .failureForwardUrl("/api/member/p/signIn?error")
                        .usernameParameter("email"))
                .logout(logout -> logout
                        .logoutUrl("/api/member/p/signOut")
                        // 如果未設定路徑預設是 Method:GET, path: loginPage + '?logout'
                        // .logoutSuccessUrl("/api/signIn?logout")
                        .deleteCookies("JSESSIONID")
                        .clearAuthentication(true)
                        .invalidateHttpSession(true));
    }

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();
        // cookieSerializer.setCookieMaxAge(600);
        cookieSerializer.setSameSite("None");
        // cookieSerializer.setDomainName("localhost");
        cookieSerializer.setUseSecureCookie(true);
        cookieSerializer.setUseHttpOnlyCookie(true);
        return cookieSerializer;
    }
}
