package com.cloud.bandott.Configuration.Jpa;

import com.cloud.bandott.Configuration.ConfigurationUtils;
import jakarta.annotation.PostConstruct;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Profile("postgresql")
@Configuration
@PropertySource(value = {"classpath:config/properties/datasource_postgresql.properties",
        "file:./config/properties/datasource_postgresql.properties"},
        ignoreResourceNotFound = true)

@EnableJpaRepositories(
        basePackages = "com.cloud.bandott.Core"
        , entityManagerFactoryRef = "postgresqlEntityManager"
        , transactionManagerRef = "postgresqlTransactionManager"
        , excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.cloud.bandott.Core.SharedKernel.LogCenter.*")
)

public class PostgreSQLConfig {

    @PostConstruct
    public void checkConfigurationLoaded() {
        ConfigurationUtils.checkConfigSource("classpath:config/properties/datasource_postgresql.properties",
                "file:./config/properties/datasource_postgresql.properties");
    }


    @Bean
    @ConfigurationProperties("spring.datasource.postgresql")
    @Primary
    public DataSourceProperties postgresqlDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    public DataSource postgresqlDataSource() {
        return postgresqlDataSourceProperties().initializeDataSourceBuilder().build();
    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean postgresqlEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(postgresqlDataSource());
        // FIXME: 會創建所有table，應該排除特定package
        //  "com.cloud.bandott.Core.SharedKernel.LogCenter.*"
        em.setPackagesToScan("com.cloud.bandott.Core");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("logging.level.org.hibernate.SQL", "TRACE");
        properties.put("logging.level.org.hibernate.type", "TRACE");
        properties.put("logging.level.org.hibernate.type.descriptor.sql", "trace");
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    @Primary
    public PlatformTransactionManager postgresqlTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(postgresqlEntityManager().getObject());
        return transactionManager;
    }
}
