package com.cloud.bandott.Configuration.Jpa;

import com.cloud.bandott.Configuration.ConfigurationUtils;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Objects;

@Profile("mysql")
@Configuration
@PropertySource(value = {"classpath:config/properties/datasource_mysql.properties",
        "file:./config/properties/datasource_mysql.properties"},
        ignoreResourceNotFound = true)

@EnableJpaRepositories(
        basePackages = "com.cloud.bandott.Core.SharedKernel.LogCenter"
        , entityManagerFactoryRef = "mysqlEntityManager"
        , transactionManagerRef = "mysqlTransactionManager"
)

@AllArgsConstructor
public class MysqlConfig {
    private final Environment env;

    @PostConstruct
    public void checkConfigurationLoaded() {
        ConfigurationUtils.checkConfigSource("classpath:config/properties/datasource_mysql.properties", "file:./config/properties/datasource_mysql.properties");
    }

    @Bean
    public DataSource mysqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(Objects.requireNonNull(env.getProperty("spring.datasource.mysql.driverClassName")));
        dataSource.setUrl(env.getProperty("spring.datasource.mysql.url"));
        dataSource.setUsername(env.getProperty("spring.datasource.mysql.username"));
        dataSource.setPassword(env.getProperty("spring.datasource.mysql.password"));
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean mysqlEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(mysqlDataSource());
        em.setPackagesToScan("com.cloud.bandott.Core.SharedKernel.LogCenter");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.ddl-auto"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public PlatformTransactionManager mysqlTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(mysqlEntityManager().getObject());
        return transactionManager;
    }
}
