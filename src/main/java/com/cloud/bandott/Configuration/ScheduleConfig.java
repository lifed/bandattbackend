package com.cloud.bandott.Configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

@Configuration
@EnableScheduling
public class ScheduleConfig {
    @Scheduled(fixedRate = 60000)
    public void fixedRate() {
        System.out.println("LocalDateTime.now() = " + LocalDateTime.now());
    }

}
