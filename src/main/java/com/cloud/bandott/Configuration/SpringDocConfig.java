package com.cloud.bandott.Configuration;

import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = {"file:./config/properties/springdoc.properties", "classpath:config/properties/springdoc.properties"},
        ignoreResourceNotFound = true)
public class SpringDocConfig {
    @Bean
    public GroupedOpenApi userOpenApi() {
        String[] packagesToscan = {"com.cloud.bandott.Presentation.Component.Member"};
        return GroupedOpenApi.builder()
                .group("users")
                .packagesToScan(packagesToscan)
                .build();
    }

    @Bean
    public GroupedOpenApi storeOpenApi() {
        String[] packagesToscan = {"com.cloud.bandott.Presentation.Component.Store"};
        return GroupedOpenApi.builder()
                .group("stores")
                .packagesToScan(packagesToscan)
                .build();
    }

    @Bean
    public GroupedOpenApi orderOpenApi() {
        String[] packagesToscan = {"com.cloud.bandott.Presentation.Component.Order"};
        return GroupedOpenApi.builder()
                .group("orders")
                .packagesToScan(packagesToscan)
                .build();
    }

    // below spring-boot-starter-data-jpa
    @Bean
    public GroupedOpenApi coreUserOpenApi() {
        String[] packagesToscan = {"com.cloud.bandott.Core.Member"};
        return GroupedOpenApi.builder()
                .group("rest_user")
                .packagesToScan(packagesToscan)
                .build();
    }

    @Bean
    public GroupedOpenApi coreStoreOpenApi() {
        String[] packagesToscan = {"com.cloud.bandott.Core.Store"};
        return GroupedOpenApi.builder()
                .group("rest_Store")
                .packagesToScan(packagesToscan)
                .build();
    }

    @Bean
    public GroupedOpenApi coreOrderOpenApi() {
        String[] packagesToscan = {"com.cloud.bandott.Core.OrderMenu"};
        return GroupedOpenApi.builder()
                .group("rest_order")
                .packagesToScan(packagesToscan)
                .build();
    }
}
