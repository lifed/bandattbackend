package com.cloud.bandott.Configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;

import java.util.Objects;
import java.util.Properties;

@Slf4j
public class YamlPropertySourceFactory implements PropertySourceFactory {
    @Override
    public PropertySource<?> createPropertySource(String name, EncodedResource encodedResource) {
        if (encodedResource.getResource().exists()) {
            YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
            factory.setResources(encodedResource.getResource());
            Properties properties = factory.getObject();
            assert properties != null;
            return new PropertiesPropertySource(Objects.requireNonNull(encodedResource.getResource().getFilename()), properties);
        } else {
            log.info("設定檔無法讀取：{}", encodedResource.getResource().getDescription());
            return new EmptyPropertySource(encodedResource.getResource().getFilename());
        }
    }

    public static class EmptyPropertySource extends PropertySource<Object> {
        public EmptyPropertySource(String name) {
            super(name);
        }

        @Override
        public Object getProperty(String name) {
            return null;
        }
    }
}
