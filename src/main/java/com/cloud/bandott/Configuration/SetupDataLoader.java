package com.cloud.bandott.Configuration;

import com.cloud.bandott.Core.Member.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;
    private final MemberCenterRepository memberCenterRepository;
    private final MemberRoleRepository memberRoleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        Arrays.stream(EnumRole.values()).forEach(s ->
                createRoleIfNotFound(s.name())
        );


        if (memberCenterRepository.findFirstByEmail("admin@bandott.com.tw").isEmpty()) {
            List<MemberRole> adminRole = new ArrayList<>();
            adminRole.add(memberRoleRepository.findByName(EnumRole.WEB_ADMIN.name()));
            adminRole.add(memberRoleRepository.findByName(EnumRole.FRONTEND_USER.name()));
            MemberCenter user = MemberCenter.builder()
                    .enabled(true)
                    .email("admin@bandott.com.tw")
                    .password(passwordEncoder.encode("admin1234"))
                    .name("管理者")
                    .nickname("蛇摸都不想管")
                    .memberRoles(adminRole)
                    .build();
            memberCenterRepository.save(user);
        }
        alreadySetup = true;
    }

    @Transactional
    public void createRoleIfNotFound(String name) {
        MemberRole role = memberRoleRepository.findByName(name);
        if (role == null) {
            role = MemberRole.builder()
                    .name(name)
                    .build();
            memberRoleRepository.save(role);
        }
    }

}
