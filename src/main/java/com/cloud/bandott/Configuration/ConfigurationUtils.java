package com.cloud.bandott.Configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@Slf4j
public class ConfigurationUtils {
    public static void checkConfigSource(String internalPath, String externalPath) {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource internalConfig = resolver.getResource(internalPath);
        Resource externalConfig = resolver.getResource(externalPath);

        if (externalConfig.exists()) {
            log.info("目前使用的是外部設定檔: {}", externalPath);
        } else if (internalConfig.exists()) {
            log.info("目前使用的是內部設定檔: {}", internalPath);
        } else {
            log.info("找不到設定檔: {}, {}", internalPath, externalPath);
        }
    }
}
