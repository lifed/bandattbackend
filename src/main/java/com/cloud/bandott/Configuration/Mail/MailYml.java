package com.cloud.bandott.Configuration.Mail;

import com.cloud.bandott.Configuration.YamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mail")
@PropertySource(value = {"file:./config/yml/mail.yml", "classpath:config/yml/mail.yml"},
        factory = YamlPropertySourceFactory.class,
        ignoreResourceNotFound = true
)
@Data
public class MailYml {
    private String host;
    private int port;
    private Transport transport;
    private Smtp smtp;
    private String debug;
    private String username;
    private String password;

    @Data
    public static class Transport {
        private String protocol;
    }

    @Data
    public static class Smtp {
        private boolean auth;
        private StartTls starttls;

        @Data
        public static class StartTls {
            private boolean enable;
        }
    }
}
