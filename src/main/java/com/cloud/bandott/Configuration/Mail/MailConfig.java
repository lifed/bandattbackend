package com.cloud.bandott.Configuration.Mail;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@AllArgsConstructor
public class MailConfig {
    private final MailYml mailYml;

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailYml.getHost());
        mailSender.setPort(mailYml.getPort());
        mailSender.setUsername(mailYml.getUsername());
        mailSender.setPassword(mailYml.getPassword());
        Properties props = mailSender.getJavaMailProperties();
        // https://javadoc.io/doc/com.sun.mail/jakarta.mail/latest/jakarta.mail/com/sun/mail/smtp/package-summary.html
        props.put("mail.transport.protocol", mailYml.getTransport().getProtocol());
        props.put("mail.smtp.auth", mailYml.getSmtp().isAuth());
        props.put("mail.smtp.starttls.enable", mailYml.getSmtp().getStarttls().isEnable());
        props.put("mail.debug", mailYml.getDebug());
        return mailSender;
    }
}
