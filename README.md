## 便當屋(後端)

### 開發版本

1. Java: 17
2. Spring Boot: 3.0.X

### 資料庫

兩組資料庫一定都要安裝且設定才能正確啟動伺服器
請依照設定修改datasource_{dbName}.properties

1. PostgreSQL(主)
2. MariaDB(輔)

### Mapstruct

若修改Mapper Class後有問題請maven clean && maven compile。

### Redis

若使用jwt模式登出機制使用Redis黑名單方式註銷。

若無使用Redis則等token時間到期。

### 設定檔

注意！application.properties的內外部設定檔都會被讀取，外部設定檔若無內部設定檔的參數則會拿內部設定檔的參數為主。

#### 內部設定檔位置

```bash
classpath:application.properties
classpath:config/**
```

#### 外部設定檔

請複製一份內部設定檔config，放到外部設定檔的位置，檔案名稱、參數也是。

```bash
APP[Folder]
├─Bandott.jar[File]
└─config[Folder]
    ├─application.properties[File]
    ├─properties[Folder]
    ├─schema[Folder]
    └─yml[Folder]
```
